///COMPONENT SEARCH
var search_bar = Vue.component("search_bar", {
    template:
        '<div class="header-search-form">\
            <input type="hidden" name="post_type" ref="post_type" value="product" />\
            <input\
                class="header-search-input"\
                name="s"\
                type="text"\
                placeholder="Search products..."\
                ref="query"\
            />\
            <select ref="product_cat" class="header-search-select" name="product_cat">\
                <option value="">All Categories</option>\
                <option value="clothing">Clothing (9)</option>\
                <option value="mens-clothing">Men&#039;s Clothing (3)</option>\
                <option value="watches">Watches (3)</option>\
                <option value="womens-clothing">Women&#039;s Clothing (6)</option>\
            </select>\
            <button class="header-search-button" @click="search()">\
                <i class="fa fa-search" aria-hidden="true"></i>\
            </button>\
    </div>',
    props: [],
    data: function () {
        return {
            query: "",
            product_cat: "",
            post_type: "product",
            msg: "",
        };
    },
    methods: {
        search() {
            this.TestShow(this.$refs.query.value);
            //this.product_cat = product_cat;
        },
        TestShow(query) {
            this.$emit("TestShow", query);
        },
    },
});

//
var SearchBar1 = new Vue({
    el: "#Testdata",
    data() {
        return {
            searchText: "",
            product_cat: "",
            post_type: "",
            message: "Old message",
        };
    },
    methods: {
        getData(query, product_type) {
            this.searchText = query;
            this.product_cat = product_type;
        },
    },
    mounted() {},
    updated() {},
    template:
        '<div>\
        <search_bar @TestShow="getData(...arguments)" />\
        <h1>Hi{{searchText}}</h1>\
        </div>',
});
