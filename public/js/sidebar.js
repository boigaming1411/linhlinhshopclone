/// COMPONENT SHOW SIDEBAR
var SideBar = new Vue({
    el: "#side_bar",
    data: {
        side_bar_product: "",
    },
    beforeCreate() {
        $.get(
            "/api/widget-product-list",
            function (data) {
                if (data) {
                    //data = JSON.parse(data);
                    this.side_bar_product = data;
                    //console.log(this.product);
                }
            }.bind(this),
            "JSON"
        );
    },
    methods: {
        formatDate: (value) => {
            if (value) {
                return moment(String(value)).format("DD/MM/YYYY");
            }
        },
        getProductDetailUrl: (item) => {
            return "/product-detail/" + item.id;
        },
    },
    mounted() {},
    updated() {
        this.formatDate();
    },
    template:
        '<ul class="product_list_widget">\
                <li v-for="item in side_bar_product">\
                    <a :href="getProductDetailUrl(item)">\
                        <img\
                            width="300"\
                            height="300"\
                            :src="item.images[0].src"\
                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"\
                            alt=""\
                            decoding="async"\
                            loading="lazy"\
                            sizes="(max-width: 300px) 100vw, 300px"\
                        />\
                        <span class="product-title">{{item.name}}</span>\
                    </a>\
                    <div v-html="item.price_html"></div>\
                </li>\
            </ul>',
});
