var Cart = Vue.component("Cart", {
    template:
        '<div\
                id="woocommerce_widget_cart-1"\
                class="widget woocommerce widget_shopping_cart"\
            >\
                <div class="widget-title">\
                    <h3>Cart</h3>\
                </div>\
                <div class="widget_shopping_cart_content">\
                    <ul class="woocommerce-mini-cart cart_list product_list_widget">\
                        <li class="woocommerce-mini-cart-item mini_cart_item" v-for="item in >\
                            <a\
                                href="/cart/?remove_item=b6d767d2f8ed5d21a44b0e5886680cb9&amp;_wpnonce=f831c2c8b1"\
                                class="remove remove_from_cart_button"\
                                aria-label="Remove this item"\
                                data-product_id="22"\
                                data-cart_item_key="b6d767d2f8ed5d21a44b0e5886680cb9"\
                                data-product_sku=""\
                            >\
                                ×\
                            </a>\
                            <a href="/product/bikini/">\
                                <img\
                                    width="300"\
                                    height="300"\
                                    src="https://staging.dndcleaners.ca/wp-content/uploads/2019/40/StockSnap_MO7PZ7AYIC-300x300.jpg"\
                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"\
                                    alt=""\
                                    decoding="async"\
                                    loading="lazy"\
                                    sizes="(max-width: 300px) 100vw, 300px"\
                                />\
                                Bikini\
                            </a>\
                            <span class="quantity">\
                                2 ×\
                                <span class="woocommerce-Price-amount amount">\
                                    <bdi>\
                                        <span class="woocommerce-Price-currencySymbol">\
                                            $\
                                        </span>\
                                        20\
                                    </bdi>\
                                </span>\
                            </span>\
                        </li>\
                    </ul>\
                    <p class="woocommerce-mini-cart__total total">\
                        <strong>Subtotal:</strong>\
                        <span class="woocommerce-Price-amount amount">\
                            <bdi>\
                                <span class="woocommerce-Price-currencySymbol">$</span>{{subtotal}}\
                            </bdi>\
                        </span>\
                    </p>\
                    <p class="woocommerce-mini-cart__buttons buttons">\
                        <a\
                            href="/cart/"\
                            class="button wc-forward wp-element-button"\
                        >\
                            View cart\
                        </a>\
                        <a\
                            href="/checkout/"\
                            class="button checkout wc-forward wp-element-button"\
                        >\
                            Checkout\
                        </a>\
                    </p>\
                </div>\
            </div>;',
    props: [],
    data: function () {
        return {
            product: "",
            subtotal: "",
        };
    },
    methods: {},
});
