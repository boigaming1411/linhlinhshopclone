///COMPONENT WOMEN CLOTHING

var WomenProduct = new Vue({
    el: "#women_product",
    data: {
        product: "",
    },
    beforeCreate() {
        $.get(
            "/api/product_category?id_category=228",
            function (data) {
                if (data) {
                    //data = JSON.parse(data);
                    this.product = data;
                    //console.log(this.product);
                }
            }.bind(this),
            "JSON"
        );
    },
    methods: {
        formatDate: (value) => {
            if (value) {
                return moment(String(value)).format("DD/MM/YYYY");
            }
        },
        getProductDetailUrl: (item) => {
            return "/product-detail/" + item.id;
        },
    },
    mounted() {},
    updated() {
        this.formatDate();
    },
    template:
        '<ul class="products columns-4" style="display: flex;flex-wrap: wrap;">\
        <li v-for="item in product" \
            style="flex: 0 0 21%;"\
            class="product type-product post-22 status-publish first instock product_cat-clothing product_cat-womens-clothing has-post-thumbnail sale shipping-taxable purchasable product-type-simple"\
                                >\
                                    <a\
                                        :href="getProductDetailUrl(item)"\
                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"\
                                    >\
                                        <span v-if="item.on_sale" class="onsale">Sale!</span>\
                                        <img\
                                            width="300"\
                                            height="300"\
                                            :src="item.images[0].src"\
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"\
                                            alt=""\
                                            decoding="async"\
                                            loading="lazy"\
                                            sizes="(max-width: 300px) 100vw, 300px"\
                                        />\
                                        <h2 class="woocommerce-loop-product__title">{{item.name}}</h2>\
                                        <span class="price" \
                                            v-html="item.price_html"></span\
                                        > </a\
                                    ><a\
                                        v-if="!item.has_options"\
                                        href="/shop/?add-to-cart=22"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_simple add_to_cart_button ajax_add_to_cart"\
                                        data-product_id="22"\
                                        data-product_sku=""\
                                        aria-label="Add &ldquo;Bikini&rdquo; to your cart"\
                                        rel="nofollow"\
                                        >Add to cart</a\
                                    >\
                                    <a\
                                        v-if="item.has_options"\
                                        :href="getProductDetailUrl(item)"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_variable"\
                                        data-product_id="item.id"\
                                        data-product_sku=""\
                                        aria-label="Select options for &ldquo;Black pants&rdquo;"\
                                        rel="nofollow"\
                                        >Select options</a\
                                    >\
                                </li>\
                                </ul>',
});

///COMPONENT MEN CLOTHING

var MenProduct = new Vue({
    el: "#men_product",
    data: {
        product: "",
    },
    beforeCreate() {
        $.get(
            "/api/product_category?id_category=225",
            function (data) {
                if (data) {
                    //data = JSON.parse(data);
                    this.product = data;
                    //console.log(this.product);
                }
            }.bind(this),
            "JSON"
        );
    },
    methods: {
        formatDate: (value) => {
            if (value) {
                return moment(String(value)).format("DD/MM/YYYY");
            }
        },
        getProductDetailUrl: (item) => {
            return "/product-detail/" + item.id;
        },
    },
    mounted() {},
    updated() {
        this.formatDate();
    },
    template:
        '<ul class="products columns-4" style="display: flex;flex-wrap: wrap;">\
        <li v-for="item in product" \
            style="flex: 0 0 21%;"\
            class="product type-product post-22 status-publish first instock product_cat-clothing product_cat-womens-clothing has-post-thumbnail sale shipping-taxable purchasable product-type-simple"\
                                >\
                                    <a\
                                        :href="getProductDetailUrl(item)"\
                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"\
                                    >\
                                        <span v-if="item.on_sale" class="onsale">Sale!</span>\
                                        <img\
                                            width="300"\
                                            height="300"\
                                            :src="item.images[0].src"\
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"\
                                            alt=""\
                                            decoding="async"\
                                            loading="lazy"\
                                            sizes="(max-width: 300px) 100vw, 300px"\
                                        />\
                                        <h2 class="woocommerce-loop-product__title">{{item.name}}</h2>\
                                        <span class="price" \
                                            v-html="item.price_html"></span\
                                        > </a\
                                    ><a\
                                        v-if="!item.has_options"\
                                        href="/shop/?add-to-cart=22"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_simple add_to_cart_button ajax_add_to_cart"\
                                        data-product_id="22"\
                                        data-product_sku=""\
                                        aria-label="Add &ldquo;Bikini&rdquo; to your cart"\
                                        rel="nofollow"\
                                        >Add to cart</a\
                                    >\
                                    <a\
                                        v-if="item.has_options"\
                                        :href="getProductDetailUrl(item)"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_variable"\
                                        data-product_id="item.id"\
                                        data-product_sku=""\
                                        aria-label="Select options for &ldquo;Black pants&rdquo;"\
                                        rel="nofollow"\
                                        >Select options</a\
                                    >\
                                </li>\
                                </ul>',
});

///COMPONENT WATCHES
var Watches = new Vue({
    el: "#watch_product",
    data: {
        product: "",
    },
    beforeCreate() {
        $.get(
            "/api/product_category?id_category=227",
            function (data) {
                if (data) {
                    //data = JSON.parse(data);
                    this.product = data;
                    //console.log(this.product);
                }
            }.bind(this),
            "JSON"
        );
    },
    methods: {
        formatDate: (value) => {
            if (value) {
                return moment(String(value)).format("DD/MM/YYYY");
            }
        },
        getProductDetailUrl: (item) => {
            return "/product-detail/" + item.id;
        },
    },
    mounted() {},
    updated() {
        this.formatDate();
    },
    template:
        '<ul class="products columns-4" style="display: flex;flex-wrap: wrap;">\
        <li v-for="item in product" \
            style="flex: 0 0 21%;"\
            class="product type-product post-22 status-publish first instock product_cat-clothing product_cat-womens-clothing has-post-thumbnail sale shipping-taxable purchasable product-type-simple"\
                                >\
                                    <a\
                                        :href="getProductDetailUrl(item)"\
                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"\
                                    >\
                                        <span v-if="item.on_sale" class="onsale">Sale!</span>\
                                        <img\
                                            width="300"\
                                            height="300"\
                                            :src="item.images[0].src"\
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"\
                                            alt=""\
                                            decoding="async"\
                                            loading="lazy"\
                                            sizes="(max-width: 300px) 100vw, 300px"\
                                        />\
                                        <h2 class="woocommerce-loop-product__title">{{item.name}}</h2>\
                                        <span class="price"><div v-html="item.price_html"></div></span\
                                        > </a\
                                    ><a\
                                        v-if="!item.has_options"\
                                        href="/shop/?add-to-cart=22"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_simple add_to_cart_button ajax_add_to_cart"\
                                        data-product_id="22"\
                                        data-product_sku=""\
                                        aria-label="Add &ldquo;Bikini&rdquo; to your cart"\
                                        rel="nofollow"\
                                        >Add to cart</a\
                                    >\
                                    <a\
                                        v-if="item.has_options"\
                                        :href="getProductDetailUrl(item)"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_variable"\
                                        data-product_id="item.id"\
                                        data-product_sku=""\
                                        aria-label="Select options for &ldquo;Black pants&rdquo;"\
                                        rel="nofollow"\
                                        >Select options</a\
                                    >\
                                </li>\
                                </ul>',
});

///COMPONENT FEATURED
var Featured = new Vue({
    el: "#featured",
    data: {
        product: "",
    },
    beforeCreate() {
        $.get(
            "/api/get_featured_product",
            function (data) {
                if (data) {
                    //data = JSON.parse(data);
                    this.product = data;
                    //console.log(this.product);
                }
            }.bind(this),
            "JSON"
        );
    },
    methods: {
        formatDate: (value) => {
            if (value) {
                return moment(String(value)).format("DD/MM/YYYY");
            }
        },
        getProductDetailUrl: (item) => {
            return "/product-detail/" + item.id;
        },
    },
    mounted() {},
    updated() {
        this.formatDate();
    },
    template:
        '<ul class="products columns-4" style="display: flex;flex-wrap: wrap;">\
        <li v-for="item in product" \
            style="flex: 0 0 21%;"\
            class="product type-product post-22 status-publish first instock product_cat-clothing product_cat-womens-clothing has-post-thumbnail sale shipping-taxable purchasable product-type-simple"\
                                >\
                                    <a\
                                        :href="getProductDetailUrl(item)"\
                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"\
                                    >\
                                        <span v-if="item.on_sale" class="onsale">Sale!</span>\
                                        <img\
                                            width="300"\
                                            height="300"\
                                            :src="item.images[0].src"\
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"\
                                            alt=""\
                                            decoding="async"\
                                            loading="lazy"\
                                            sizes="(max-width: 300px) 100vw, 300px"\
                                        />\
                                        <h2 class="woocommerce-loop-product__title">{{item.name}}</h2>\
                                        <span class="price"><div v-html="item.price_html"></div></span\
                                        > </a\
                                    ><a\
                                        v-if="!item.has_options"\
                                        href="/shop/?add-to-cart=22"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_simple add_to_cart_button ajax_add_to_cart"\
                                        data-product_id="22"\
                                        data-product_sku=""\
                                        aria-label="Add &ldquo;Bikini&rdquo; to your cart"\
                                        rel="nofollow"\
                                        >Add to cart</a\
                                    >\
                                    <a\
                                        v-if="item.has_options"\
                                        :href="getProductDetailUrl(item)"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_variable"\
                                        data-product_id="item.id"\
                                        data-product_sku=""\
                                        aria-label="Select options for &ldquo;Black pants&rdquo;"\
                                        rel="nofollow"\
                                        >Select options</a\
                                    >\
                                </li>\
                                </ul>',
});

///COMPONENT BESTSELLING
var BestSelling = new Vue({
    el: "#best_selling",
    data: {
        product: "",
    },
    beforeCreate() {
        $.get(
            "/api/get_bestselling_product",
            function (data) {
                if (data) {
                    //data = JSON.parse(data);
                    this.product = data;
                    //console.log(this.product);
                }
            }.bind(this),
            "JSON"
        );
    },
    methods: {
        formatDate: (value) => {
            if (value) {
                return moment(String(value)).format("DD/MM/YYYY");
            }
        },
        getProductDetailUrl: (item) => {
            return "/product-detail/" + item.id;
        },
    },
    mounted() {},
    updated() {
        this.formatDate();
    },
    template:
        '<ul class="products columns-4" style="display: flex;flex-wrap: wrap;">\
        <li v-for="item in product" \
            style="flex: 0 0 21%;"\
            class="product type-product post-22 status-publish first instock product_cat-clothing product_cat-womens-clothing has-post-thumbnail sale shipping-taxable purchasable product-type-simple"\
                                >\
                                    <a\
                                        :href="getProductDetailUrl(item)"\
                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"\
                                    >\
                                        <span v-if="item.on_sale" class="onsale">Sale!</span>\
                                        <img\
                                            width="300"\
                                            height="300"\
                                            :src="item.images[0].src"\
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"\
                                            alt=""\
                                            decoding="async"\
                                            loading="lazy"\
                                            sizes="(max-width: 300px) 100vw, 300px"\
                                        />\
                                        <h2 class="woocommerce-loop-product__title">{{item.name}}</h2>\
                                        <span class="price"><div v-html="item.price_html"></div></span\
                                        > </a\
                                    ><a\
                                        v-if="!item.has_options"\
                                        href="/shop/?add-to-cart=22"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_simple add_to_cart_button ajax_add_to_cart"\
                                        data-product_id="22"\
                                        data-product_sku=""\
                                        aria-label="Add &ldquo;Bikini&rdquo; to your cart"\
                                        rel="nofollow"\
                                        >Add to cart</a\
                                    >\
                                    <a\
                                        v-if="item.has_options"\
                                        :href="getProductDetailUrl(item)"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_variable"\
                                        data-product_id="item.id"\
                                        data-product_sku=""\
                                        aria-label="Select options for &ldquo;Black pants&rdquo;"\
                                        rel="nofollow"\
                                        >Select options</a\
                                    >\
                                </li>\
                                </ul>',
});

///COMPONENT RECENT
var Recent = new Vue({
    el: "#recent",
    data: {
        product: "",
    },
    beforeCreate() {
        $.get(
            "/api/get_recent_product",
            function (data) {
                if (data) {
                    //data = JSON.parse(data);
                    this.product = data;
                    //console.log(this.product);
                }
            }.bind(this),
            "JSON"
        );
    },
    methods: {
        formatDate: (value) => {
            if (value) {
                return moment(String(value)).format("DD/MM/YYYY");
            }
        },
        getProductDetailUrl: (item) => {
            return "/product-detail/" + item.id;
        },
    },
    mounted() {},
    updated() {
        this.formatDate();
    },
    template:
        '<ul class="products columns-4" style="display: flex;flex-wrap: wrap;">\
        <li v-for="item in product" \
            style="flex: 0 0 21%;"\
            class="product type-product post-22 status-publish first instock product_cat-clothing product_cat-womens-clothing has-post-thumbnail sale shipping-taxable purchasable product-type-simple"\
                                >\
                                    <a\
                                        :href="getProductDetailUrl(item)"\
                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"\
                                    >\
                                        <span v-if="item.on_sale" class="onsale">Sale!</span>\
                                        <img\
                                            width="300"\
                                            height="300"\
                                            :src="item.images[0].src"\
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"\
                                            alt=""\
                                            decoding="async"\
                                            loading="lazy"\
                                            sizes="(max-width: 300px) 100vw, 300px"\
                                        />\
                                        <h2 class="woocommerce-loop-product__title">{{item.name}}</h2>\
                                        <span class="price"><div v-html="item.price_html"></div></span\
                                        > </a\
                                    ><a\
                                        v-if="!item.has_options"\
                                        href="/shop/?add-to-cart=22"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_simple add_to_cart_button ajax_add_to_cart"\
                                        data-product_id="22"\
                                        data-product_sku=""\
                                        aria-label="Add &ldquo;Bikini&rdquo; to your cart"\
                                        rel="nofollow"\
                                        >Add to cart</a\
                                    >\
                                    <a\
                                        v-if="item.has_options"\
                                        :href="getProductDetailUrl(item)"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_variable"\
                                        data-product_id="item.id"\
                                        data-product_sku=""\
                                        aria-label="Select options for &ldquo;Black pants&rdquo;"\
                                        rel="nofollow"\
                                        >Select options</a\
                                    >\
                                </li>\
                                </ul>',
});

///COMPONENT SALE
var Sale = new Vue({
    el: "#sale",
    data: {
        product: "",
    },
    beforeCreate() {
        $.get(
            "/api/get_sale_product",
            function (data) {
                if (data) {
                    //data = JSON.parse(data);
                    this.product = data;
                    //console.log(this.product);
                }
            }.bind(this),
            "JSON"
        );
    },
    methods: {
        formatDate: (value) => {
            if (value) {
                return moment(String(value)).format("DD/MM/YYYY");
            }
        },
        getProductDetailUrl: (item) => {
            return "/product-detail/" + item.id;
        },
    },
    mounted() {},
    updated() {
        this.formatDate();
    },
    template:
        '<ul class="products columns-4" style="display: flex;flex-wrap: wrap;">\
        <li v-for="item in product" \
            style="flex: 0 0 21%;"\
            class="product type-product post-22 status-publish first instock product_cat-clothing product_cat-womens-clothing has-post-thumbnail sale shipping-taxable purchasable product-type-simple"\
                                >\
                                    <a\
                                        :href="getProductDetailUrl(item)"\
                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"\
                                    >\
                                        <span v-if="item.on_sale" class="onsale">Sale!</span>\
                                        <img\
                                            width="300"\
                                            height="300"\
                                            :src="item.images[0].src"\
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"\
                                            alt=""\
                                            decoding="async"\
                                            loading="lazy"\
                                            sizes="(max-width: 300px) 100vw, 300px"\
                                        />\
                                        <h2 class="woocommerce-loop-product__title">{{item.name}}</h2>\
                                        <span class="price"><div v-html="item.price_html"></div></span\
                                        > </a\
                                    ><a\
                                        v-if="!item.has_options"\
                                        href="/shop/?add-to-cart=22"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_simple add_to_cart_button ajax_add_to_cart"\
                                        data-product_id="22"\
                                        data-product_sku=""\
                                        aria-label="Add &ldquo;Bikini&rdquo; to your cart"\
                                        rel="nofollow"\
                                        >Add to cart</a\
                                    >\
                                    <a\
                                        v-if="item.has_options"\
                                        :href="getProductDetailUrl(item)"\
                                        data-quantity="1"\
                                        class="button wp-element-button product_type_variable"\
                                        data-product_id="item.id"\
                                        data-product_sku=""\
                                        aria-label="Select options for &ldquo;Black pants&rdquo;"\
                                        rel="nofollow"\
                                        >Select options</a\
                                    >\
                                </li>\
                                </ul>',
});
