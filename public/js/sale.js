var ProductSale = new Vue({
    el: "#product_sale",
    data: {
        product: "",
    },
    beforeCreate() {
        $.get(
            "/api/list-sale",
            function (data) {
                if (data) {
                    //data = JSON.parse(data);
                    this.product = data;
                    console.log(this.product);
                }
            }.bind(this),
            "JSON"
        );
    },
    methods: {
        formatDate: (value) => {
            if (value) {
                return moment(String(value)).format("DD/MM/YYYY");
            }
        },
        getProductDetailUrl: (item) => {
            return "/product-detail/" + item.id;
        },
    },
    mounted() {},
    updated() {
        this.formatDate();
    },
    template:
        '<ul class="products columns-3" style="display: flex;flex-wrap: wrap;">\
            <li\
                v-for="item in product"\
                style="flex: 0 0 29%;"\
                class="product type-product post-2055 status-publish first instock product_cat-clothing product_cat-womens-clothing has-post-thumbnail sale featured downloadable virtual purchasable product-type-simple"\
            >\
                <a\
                    :href="getProductDetailUrl(item)"\
                    class="woocommerce-LoopProduct-link woocommerce-loop-product__link"\
                >\
                    <span class="onsale">Sale!</span>\
                    <img\
                        width="300"\
                        height="300"\
                        :src="item.images[0].src"\
                        class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"\
                        alt=""\
                        decoding="async"\
                        loading="lazy"\
                        sizes="(max-width: 300px) 100vw, 300px"\
                    />\
                    <h2 class="woocommerce-loop-product__title">\
                        {{item.name}}\
                    </h2>\
                    <span class="price"\
                        ><del aria-hidden="true"\
                            ><span\
                                class="woocommerce-Price-amount amount"\
                                ><bdi\
                                    ><span\
                                        class="woocommerce-Price-currencySymbol"\
                                        >&#36;</span\
                                    >{{item.regular_price}}</bdi\
                                ></span\
                            ></del\
                        >\
                        <ins\
                            ><span\
                                class="woocommerce-Price-amount amount"\
                                ><bdi\
                                    ><span\
                                        class="woocommerce-Price-currencySymbol"\
                                        >&#36;</span\
                                    >{{item.price}}</bdi\
                                ></span\
                            ></ins\
                        ></span\
                    > </a\
                ><a\
                    v-if="!item.has_options"\
                    href="/sale/?add-to-cart=2055"\
                    data-quantity="1"\
                    class="button wp-element-button product_type_simple add_to_cart_button ajax_add_to_cart"\
                    data-product_id="2055"\
                    data-product_sku="woo-single"\
                    aria-label="Add &ldquo;Single Shirt&rdquo; to your cart"\
                    rel="nofollow"\
                    >Add to cart</a\
                >\
                <a\
                    v-if="item.has_options"\
                    href="/product/polo-t-shirt/"\
                    data-quantity="1"\
                    class="button wp-element-button product_type_variable add_to_cart_button"\
                    data-product_id="2054"\
                    data-product_sku=""\
                    aria-label="Select options for &ldquo;Polo T-shirt&rdquo;"\
                    rel="nofollow"\
                    >Select options</a\
                >\
            </li>\
        </ul>',
});
