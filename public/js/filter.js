var Filter = Vue.component("Filter                ", {
    template:
        '<div id="woocommerce_price_filter-1" class="widget woocommerce widget_price_filter">\
            <div class="widget-title"><h3>Filter by price</h3></div>\
            <form method="get" action="/shop/">\
                <div class="price_slider_wrapper">\
                    <div\
                        class="price_slider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"\
                        style=""\
                    >\
                        <div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 100%"></div>\
                        <span\
                            tabindex="0"\
                            class="ui-slider-handle ui-corner-all ui-state-default"\
                            style="left: 0%"\
                        ></span\
                        ><span\
                            tabindex="0"\
                            class="ui-slider-handle ui-corner-all ui-state-default"\
                            style="left: 100%"\
                        ></span>\
                    </div>\
                    <div class="price_slider_amount" data-step="10">\
                        <label class="screen-reader-text" for="min_price">Min price</label>\
                        <input\
                            type="text"\
                            id="min_price"\
                            name="min_price"\
                            value="10"\
                            data-min="10"\
                            placeholder="Min price"\
                            style="display: none"\
                        />\
                        <label class="screen-reader-text" for="max_price">Max price</label>\
                        <input\
                            type="text"\
                            id="max_price"\
                            name="max_price"\
                            value="80"\
                            data-max="80"\
                            placeholder="Max price"\
                            style="display: none"\
                        />\
                        <button type="submit" class="button wp-element-button">Filter</button>\
                        <div class="price_label" style="">\
                            Price: <span class="from">$10</span> — <span class="to">$80</span>\
                        </div>\
                        <div class="clear"></div>\
                    </div>\
                </div>\
            </form>\
        </div>\
         <div id="woocommerce_layered_nav_filters-1" class="widget woocommerce widget_layered_nav_filters">\
            <div class="widget-title"><h3>Active filters</h3></div>\
            <ul>\
                <li class="chosen">\
                    <a\
                        rel="nofollow"\
                        aria-label="Remove filter"\
                        href="/shop/?max_price=50&amp;s=s&amp;post_type=product"\
                        >Min\
                        <span class="woocommerce-Price-amount amount"\
                            ><bdi><span class="woocommerce-Price-currencySymbol">$</span>20</bdi></span\
                        ></a\
                    >\
                </li>\
                <li class="chosen">\
                    <a\
                        rel="nofollow"\
                        aria-label="Remove filter"\
                        href="/shop/?min_price=20&amp;s=s&amp;post_type=product"\
                        >Max\
                        <span class="woocommerce-Price-amount amount"\
                            ><bdi><span class="woocommerce-Price-currencySymbol">$</span>50</bdi></span\
                        ></a\
                    >\
                </li>\
            </ul>\
        </div>',
    props: [],
    data: function () {
        return {
            min_price: "",
            max_price: "",
        };
    },
    methods: {},
});
