var Blog = new Vue({
    el: "#all_blog",
    data: {
        blog: "",
    },
    beforeCreate() {
        $.get(
            "/api/list-blog",
            function (data) {
                if (data) {
                    //data = JSON.parse(data);
                    this.blog = data;
                    //console.log(this.product);
                }
            }.bind(this),
            "JSON"
        );
    },
    methods: {
        formatDate: (value) => {
            if (value) {
                return moment(String(value)).format("DD/MM/YYYY");
            }
        },
        getBlogDetailUrl: (item) => {
            return "/blog-detail/" + item.id;
        },
    },
    mounted() {},
    updated() {
        this.formatDate();
    },
    template:
        '<div>\
        <article class="col-md-6" v-for="item in blog">\
            <div\
                class="post-98 post type-post status-publish format-standard has-post-thumbnail hentry category-category-1 tag-tag-2 tag-tag1"\
            >\
                <div class="news-item has-thumbnail">\
                    <div class="news-thumb">\
                        <a :href="getBlogDetailUrl(item)" :title="item.title.rendered">\
                            <img\
                                width="720"\
                                height="405"\
                                :src="item.link_image"\
                                class="attachment-envo-storefront-med size-envo-storefront-med wp-post-image"\
                                alt=""\
                                decoding="async"\
                                loading="lazy"\
                                sizes="(max-width: 720px) 100vw, 720px"\
                            />\
                        </a>\
                    </div>\
                    <div class="news-text-wrap">\
                        <span class="author-meta">\
                            <span class="author-meta-by">By</span>\
                            <a href=""> hugo </a>\
                        </span>\
                        <div class="content-date-comments">\
                            <span class="posted-date"> {{formatDate(item.date)}} </span>\
                            <span class="comments-meta">\
                                <a\
                                    href=""\
                                    rel="nofollow"\
                                    title="Comment on Lorem ipsum dolor sit amet"\
                                >\
                                    0\
                                </a>\
                                <i class="fa fa-comments-o"></i>\
                            </span>\
                        </div>\
                        <h2 class="entry-title">\
                            <a :href="getBlogDetailUrl(item)" rel="bookmark"\
                                >{{item.title.rendered}}</a\
                            >\
                        </h2>\
                        <div class="post-excerpt" v-html="item.excerpt.rendered">\
                        </div>\
                    </div>\
                </div>\
            </div>\
        </article>\
        </div>',
});
