<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CloneShopController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\WigetController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('product-all',[CloneShopController::class,'product']);
Route::get('list-sale',[CloneShopController::class,'list_sale']);
Route::get('list-blog',[CloneShopController::class,'list_blog']);
Route::get('search-product/{name}',[CloneShopController::class,'search_product']);
Route::get('filter/{query_url}',[CloneShopController::class,'filter_product']);
Route::get('our-story-page',[CloneShopController::class,'our_Story_page']);
Route::get('setting',[CloneShopController::class,'getSetting']);
Route::get('theme-info',[CloneShopController::class,'getThemeInfo']);
Route::get('/cart_user',[CartController::class,'user_cart']);
Route::get('/searchByPrototype',[CloneShopController::class,'searchbyprototype']);
Route::get('/widget-product-list',[WigetController::class,'right_side_bar_widget_api']);
Route::get('/product_detail/{id}',[ProductController::class,'get_Product_Details']);
Route::get('/product_category',[CategoryController::class,'get_product_category']);
Route::get('/list-category',[CategoryController::class,'get_list_category']);
Route::get('/related-product/{id}',[ProductController::class,'get_Related_Product']);
Route::get('/add_cart',[CartController::class,'add_cart']);
Route::get('/get_featured_product',[ProductController::class,'get_featured_product']);
Route::get('/get_recent_product',[ProductController::class,'get_recent_product']);
Route::get('/get_bestselling_product',[ProductController::class,'get_bestselling_product']);
Route::get('/get_sale_product',[ProductController::class,'get_sale_product']);
Route::get('/blog_detail/{blog_id}',[PostController::class,'get_blog_detail']);
Route::get('/cart_remove',[CartController::class,'remove_cart']);
Route::get('/get_sub_total_cart',[CartController::class,'get_sub_total_cart']);
Route::get('/GetProductandQuantityProductInCart',[CartController::class,'GetProductandQuantityProductInCart']);





// Api này dùng để search product category theo tên . Truyền tên category vào sẽ ra sản phẩm theo nhóm category đó
Route::get('/get_product_by_category_name/{category_name}',[CategoryController::class,'get_product_by_category_name']);
// Api này dùng để search product category theo tên . Truyền tên category vào sẽ ra sản phẩm theo nhóm category đó
