<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[PageController::class,'home']);

Route::get('/shop',[PageController::class,'shop']);

Route::get('/sale',[PageController::class,'sale']);
Route::get('/search',[PageController::class,'search']);


Route::get('/about-us',[PageController::class,'about_us']);

Route::get('/blog',[PageController::class,'blog']);

Route::get('/contact',[PageController::class,'contact']);

Route::get('/my-account',[PageController::class,'my_account']);

Route::get('/my-account/lost-password',[PageController::class,'my_account_lost_pw']);


Route::get('/cart',[PageController::class,'cart']);

Route::get('/product-detail/{id}',[PageController::class,'product_detail']);

Route::get('/blog-detail/{id}',[PageController::class,'blog_detail']);