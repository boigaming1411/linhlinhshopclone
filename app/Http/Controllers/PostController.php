<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function get_blog_detail($id)
    {
        //$id = $request->blog_id;
        $regex = '/[0-9]+/';
        if(!preg_match($regex,$id) ){
            return response()->json(['message' => 'Only Just Type Number on Blog id'], 404);
        }
        $url = 'https://www.staging.balooworld.ca/wp-json/wp/v2/posts/'.$id;
        $json_data = json_decode($this->getApi($url));
        return $json_data;
    }



    // This Api Will Call For Some Api Require Auth
    private function authGetApi($url, $method = 'GET')
    {
        $consumer_key = 'boigaming1411@gmail.com';
        $consumer_secret = 'f3CJ p4sd 12lR wtJ8 zhKY Ik7A';
//        $consumer_secret = 'XdyT Kbcc ASUy uStK Gfkk ftyX';


        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return $resp;


    }

    private  function getApi($url, $method = 'GET'){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }


    private function getApiWoocomere($url, $method = 'GET'){



        $consumer_key = 'ck_3aacf618ef5a2e9c70aa2608c02740489ee49ad0';
        $consumer_secret = 'cs_b204adf2a666763de8ec813e97580dd189bdc956';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return $resp;
    }



}
