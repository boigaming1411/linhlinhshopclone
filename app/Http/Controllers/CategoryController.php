<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private string $url_host = "https://www.staging.balooworld.ca/";
    private $number_not_negative_regex = '/[0-9]+/';

    private function  check_id_category_is_exist($id_product){
        $url = "https://www.staging.balooworld.ca/wp-json/wc/v3/products/".$id_product;
        $consumer_key = 'ck_3aacf618ef5a2e9c70aa2608c02740489ee49ad0';
        $consumer_secret = 'cs_b204adf2a666763de8ec813e97580dd189bdc956';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
        $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if ($status_code == 200){
            return true;
        }
        else{
            return  false;
        }

    }



    public function get_product_category(Request $request)
    {
        $regex = '/[0-9]+/';
        $id = $request->id_category;
        $limit = 4;
        if (!is_null($request->limit)){
            if (preg_match($regex,$request->limit)){
                $limit = $request->limit;
            }
            else{
                return response()->json(['message' => 'Only Just Type Number on Limit'], 404);
            }
        }


        if(!preg_match($regex,$id) ){
            return response()->json(['message' => 'Only Just Type Number on Category id'], 404);
        }

        if (!$this->check_id_category_is_exist($id)){
            return response()->json(['message' => 'Category id is not exist'], 400);
        }



        $url = $this->url_host."wp-json/wc/v3/products?per_page=".$limit."&category=".$id;
//        return count(json_decode($this->getApiWoocomere($url)));
        return json_decode($this->getApiWoocomere($url));

    }

    public function get_product_by_category_name(Request $request)
    {
//        https://staging.dndcleaners.ca/wp-json/wc/v3/products/categories?search=wat
        $category_name = $request->category_name;
        $url = $this->url_host."wp-json/wc/v3/products/categories?search=".$category_name;
        $before_handle  = json_decode($this->getApiWoocomere($url))[0];
        $id_category_first = $before_handle->id;
        $url = $this->url_host."wp-json/wc/v3/products?per_page=4&category=".$id_category_first;
        return json_decode($this->getApiWoocomere($url));

    }



    public function get_list_category(Request $request)
    {
        $url = $this->url_host.'wp-json/wc/v3/products/categories';
        return json_decode($this->getApiWoocomere($url));

    }

    // This Api Will Call For Some Api Require Auth
    private function authGetApi($url, $method = 'GET')
    {
        $consumer_key = 'boigaming1411@gmail.com';
        $consumer_secret = 'f3CJ p4sd 12lR wtJ8 zhKY Ik7A';
//        $consumer_secret = 'XdyT Kbcc ASUy uStK Gfkk ftyX';


        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return $resp;


    }

    private  function getApi($url, $method = 'GET'){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }


    private function getApiWoocomere($url, $method = 'GET'){



        $consumer_key = 'ck_3aacf618ef5a2e9c70aa2608c02740489ee49ad0';
        $consumer_secret = 'cs_b204adf2a666763de8ec813e97580dd189bdc956';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return $resp;
    }

}
