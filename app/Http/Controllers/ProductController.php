<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    private string $url_host = "https://www.staging.balooworld.ca/";
    private $number_not_negative_regex = '/[0-9]+/';


    public function get_Product_Details($id)
    {


        if (!preg_match($this->number_not_negative_regex,$id)){
            return response()->json(['message' => 'Product id must be a number'], 400);
        }
        //$id = $request->id;
        $url = $this->url_host."wp-json/wc/v3/products/".$id;
        return json_decode($this->getApiWoocomere($url));
    }




    public function get_Related_Product(Request $request)
    {
        if (!preg_match($this->number_not_negative_regex,$request->id)){
            return response()->json(['message' => 'Product id must be a number'], 400);
        }
        $id = $request->id;


        $url = $this->url_host."wp-json/wc/v3/products/".$id;
        $product = json_decode($this->getApiWoocomere($url));
        $list_related = $product->related_ids;
        $str_query = '';
        for($i = 0; $i<count($list_related)-1;$i++) {
            if ($i+1 == count($list_related)){
                $str_query.= $list_related[$i];
            }
            else{
                $str_query.= $list_related[$i].",";
            }
        }

        $url_related = $this->url_host."wp-json/wc/v3/products?include=".$str_query;
        $related = json_decode($this->getApiWoocomere($url_related));
        return $related;


    }

    public function get_featured_product(Request $request)
    {
        $arr_kq= [];
        $orderby = "featured=true&orderby=id";
        $url = $this->url_host."wp-json/wc/v3/products?".$orderby;
        $data = json_decode($this->getApiWoocomere($url));

        $arr_kq[] = $data[1];  $arr_kq[] = $data[2];  $arr_kq[] = $data[3]; $arr_kq[] = $data[4];
        return  $arr_kq;


    }

    public function get_recent_product(Request $request)
    {
        $orderby = "orderby=id";
       $url = $this->url_host."wp-json/wc/v3/products?per_page=4&".$orderby;
        return json_decode($this->getApiWoocomere($url));
    }


    public function get_bestselling_product(Request $request)
    {
        $arr_kq= [];
        $orderby = "orderby=popularity&order=desc&per_page=20";

        $url = $this->url_host."wp-json/wc/v3/products?".$orderby;

        $data = json_decode($this->getApiWoocomere($url));

        $arr_kq[] = $data[0];  $arr_kq[] = $data[8];  $arr_kq[] = $data[9]; $arr_kq[] = $data[10];


        return  $arr_kq;


    }

    public function get_sale_product()
    {
        $arr_kq= [];
        $orderby = "on_sale=true&orderby=id&per_page=4";
        $url = $this->url_host."wp-json/wc/v3/products?".$orderby;
        $data = json_decode($this->getApiWoocomere($url));
        return $data;
//        $arr_name = [];
//        foreach ($data as $value){
//            $arr_name[]= $value->name;
//        }



//        $arr_kq[] = $data[0];  $arr_kq[] = $data[1];  $arr_kq[] = $data[count($data)-2]; $arr_kq[] = $data[count($data)-1];
//        return  $arr_kq;
    }







    // This Api Will Call For Some Api Require Auth
    private function authGetApi($url, $method = 'GET')
    {
        $consumer_key = 'boigaming1411@gmail.com';
        $consumer_secret = 'f3CJ p4sd 12lR wtJ8 zhKY Ik7A';
//        $consumer_secret = 'XdyT Kbcc ASUy uStK Gfkk ftyX';


        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return $resp;


    }

    private  function getApi($url, $method = 'GET'){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }


    private function getApiWoocomere($url, $method = 'GET'){



        $consumer_key = 'ck_3aacf618ef5a2e9c70aa2608c02740489ee49ad0';
        $consumer_secret = 'cs_b204adf2a666763de8ec813e97580dd189bdc956';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return $resp;
    }
}
