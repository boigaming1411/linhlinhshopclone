<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CartController extends Controller
{
    private string $url_host = "https://www.staging.balooworld.ca/";

    private $number_not_negative_regex = '/[0-9]+/';

    public function add_cart(Request $request)
    {
        $regex = '/[0-9]+/';
        if (!preg_match($regex,$request->product_id)){
            return response()->json(['message' => 'Only Just Type Number on product id'], 400);
        }
        if (!$this->check_id_product_is_exist($request->product_id)){
            return response()->json(['message' => 'product id  Invalid'], 400);
        }
//        $quantity = (is_null($request->quantity)) ? 1 : $request->quantity ;
        $quantity = "";
        if (!is_null($request->quantity)){
            if (!preg_match($regex,$request->quantity)){
                return response()->json(['message' => 'Only Just Type Number on quantity'], 400);
            }
            else{
                $quantity = $request->quantity;
            }
        }
        else{
            $quantity = 1;
        }


        $product_id = $request->product_id;

      $cookie = "_ga=GA1.2.73756067.1672128122; _fbp=fb.1.1672128122428.1544997044; __gads=ID=80e0a39efa7249f8-22df5040e7d90049:T=1672128122:RT=1672128122:S=ALNI_MagM10246QKW3MLGUBlwI4-b42wsw; __gpi=UID=00000b98881281db:T=1672128122:RT=1672128122:S=ALNI_MYO970K02vcHjjRoGgnRRia8EGSSg; wordpress_logged_in_e0351800481d55fe6d75bd022ea060b0=Tuan+Tuan%7C1673337764%7CFqC8C92ePXjCgydHkc4CyQsVXaZRNhK9t9wPOqUvHTx%7Cadc622bea8b67b1afdc74299cf4616bca5b4c551e45d9084b289c87ef5dd2922; mailpoet_subscriber=%7B%22subscriber_id%22%3A7083%7D; mailpoet_page_view=%7B%22timestamp%22%3A1672111196%7D; wp-settings-7031=urlbutton%3Dnone%26posts_list_mode%3Dlist; wp-settings-time-7031=1672129280; wpx_logged=1; tk_ai=woo%3AN4et%2FsaOJWRaFmdCN36ejvA3; woocommerce_items_in_cart=1; wp_woocommerce_session_e0351800481d55fe6d75bd022ea060b0=7031%7C%7C1672543417%7C%7C1672539817%7C%7C03cf7e33cbbefac646a19aea702a3efb; woocommerce_cart_hash=b9f570f4869b3310f9f13f7955bfe02a";
      $url = "https://www.staging.balooworld.ca/?wc-ajax=add_to_cart&elementor_page_id=3506592";

      $data = array(
            'product_id' => $product_id,
            'quantity' => $quantity
          );


      $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Cookie: ".$cookie));
        curl_setopt($curl,CURLOPT_POST,true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);


        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        if ($status_code == 200){
            $url = $this->url_host.'wp-json/wc/store/cart/';
            $data = json_decode($this->getApiWoocomere($url));
            return response()->json(['message' => 'Add To Cart Successfully', 'cart_data'=> $data ], 200);
        }
        else{
            return response()->json(['message' => 'Failed add to cart'], 400);
        }

    }

    public function user_cart()
    {
        $url = $this->url_host.'wp-json/wc/store/cart/';
       return json_decode($this->getApiWoocomere($url));
    }

    public function GetProductandQuantityProductInCart()
    {
        $url = $this->url_host.'wp-json/wc/store/cart/';
        $data = json_decode($this->getApiWoocomere($url));
        return $data->items;

    }


    public function remove_cart(Request $request)
    {

        if (!preg_match($this->number_not_negative_regex,$request->product_id)){
            return response()->json(['message' => 'Product id must be a number'], 400);
        }
        $id_product = $request->product_id;
        if (!$this->check_id_product_is_exist($id_product)){
            return response()->json(['message' => 'product id  Invalid'], 400);
        }
        $key = "";
        $url = $this->url_host.'wp-json/wc/store/cart/';
        $data = json_decode($this->getApiWoocomere($url));
        foreach ($data->items as $value){
           if ($value->id == $id_product){
               $key = $value;
               break;
           }
           else{
               continue;
           }
        }
        if (empty($key)){
            return response()->json(['message' => 'There No Your Product ID found in Cart'], 400);
        }
        $consumer_key = 'boigaming1411@gmail.com';
        $consumer_secret = 'f3CJ p4sd 12lR wtJ8 zhKY Ik7A';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );

        $url = "https://www.staging.balooworld.ca/wp-json/wc/store/cart/remove-item/";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, ['key'=>$key->key]);


        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return json_decode($resp);

    }

    public function get_sub_total_cart()
    {
        $url = $this->url_host.'wp-json/wc/store/cart/';
        $data = json_decode($this->getApiWoocomere($url));
        $total = 0;
        foreach ($data->items as $value){
         $total += $value->totals->line_subtotal;
        }
        return $total;
    }




    private function  check_id_product_is_exist($id_product){
        $url = "https://www.staging.balooworld.ca/wp-json/wc/v3/products/".$id_product;
        $consumer_key = 'ck_3aacf618ef5a2e9c70aa2608c02740489ee49ad0';
        $consumer_secret = 'cs_b204adf2a666763de8ec813e97580dd189bdc956';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if ($status_code == 200){
            return true;
        }
        else{
            return  false;
        }

    }



    // This Api Will Call For Some Api Require Auth
    private function authGetApi($url, $method = 'GET')
    {
        $consumer_key = 'boigaming1411@gmail.com';
        $consumer_secret = 'f3CJ p4sd 12lR wtJ8 zhKY Ik7A';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return $resp;


    }

    private  function getApi($url, $method = 'GET'){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }


    private function getApiWoocomere($url, $method = 'GET'){



        $consumer_key = 'ck_3aacf618ef5a2e9c70aa2608c02740489ee49ad0';
        $consumer_secret = 'cs_b204adf2a666763de8ec813e97580dd189bdc956';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return $resp;
    }


}
