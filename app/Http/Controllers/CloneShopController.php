<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use function PHPUnit\Framework\isNull;


class CloneShopController extends Controller
{
    private string $url_host = "https://www.staging.balooworld.ca/";

    private $number_not_negative_regex = '/[0-9]+/';

    public function product()
    {
        $url = 'https://www.staging.balooworld.ca/wp-json/wc/v3/products';
        return json_decode($this->getApiWoocomere($url));
    }

    public function searchbyprototype(Request $request)
    {
        // dd($request);
        // if(empty($request->query)){
        //     // dd("chạy vào đây");
        //     $query.= "?per_page=100&search=";
        //     $url = 'https://www.staging.balooworld.ca/wp-json/wc/v3/products'.$query;
        //      $data = json_decode($this->getApiWoocomere($url));
        //      return $data;
        // }




        $query = "";
        $post_type =  $request->post_type;
        $min_price = $request->min_price;
        $max_price = $request->max_price;

        if (!preg_match($this->number_not_negative_regex,$min_price)){
            return response()->json(['message' => 'min price must be a number'], 400);
        }

        if (!preg_match($this->number_not_negative_regex,$max_price)){
            return response()->json(['message' => 'max price must be a number'], 400);
        }

        if ($min_price>$max_price){
            return response()->json(['message' => 'max price must greater than min price'], 400);
        }


        $query = "";
        if (!is_null($request->s)){
            $query.= "?per_page=100&search=".$request->s;
        }elseif (empty($request->s)){
            $query.= "?per_page=100&search=";
        }

        $category_name = $request->product_cat;
        $url = $this->url_host."wp-json/wc/v3/products/categories?search=".$category_name;
        $before_handle  = json_decode($this->getApiWoocomere($url))[0];

        if (!is_null($request->product_cat)){
            $query.= "&category=".$before_handle->id;
        }
        if (!is_null($min_price) && !is_null($max_price)){
            $query.= "&min_price=".$min_price."&max_price=".$max_price;
        }
        if (!is_null($request->orderby)){
            $query.= "&orderby=".$request->orderby;
        }



        $url = 'https://www.staging.balooworld.ca/wp-json/wc/v3/products'.$query;
//        dd($url);
//        return count(json_decode($this->getApiWoocomere($url)));
//        return json_decode($this->getApiWoocomere($url));
        $data = json_decode($this->getApiWoocomere($url));
        return $data;
        // $arr = [];
        // foreach ($data as $value){
        //     if ($value->regular_price){
        //         $arr[]= $value;
        //     }
        // }
        //  return  $arr;





    }

    public function list_sale()
    {
        $url = 'https://www.staging.balooworld.ca/wp-json/wc/v3/products?on_sale=true';
        return  json_decode($this->getApiWoocomere($url));

    }

    public function list_blog()
    {
        // Update List blog -> Get Feature image add to blog item
        $url = 'https://www.staging.balooworld.ca/wp-json/wp/v2/posts';
        $json_data = json_decode($this->getApi($url));
        return $json_data;
    }

    public function filter_product(Request $request)
    {
        $query = $request->query_url;

        $url = 'https://www.staging.balooworld.ca/wp-json/wc/v3/products?' . $query;
//                return count(json_decode($this->getApiWoocomere($url)));
        $data = json_decode($this->getApiWoocomere($url));
        $arr = [];
        foreach ($data as $value){
            if ($value->regular_price){
                $arr[]= $value;
            }
        }
        return  $arr;
    }

    public function search_product(Request $request)
    {
        $name = $request->name;
        $url = 'https://www.staging.balooworld.ca/wp-json/wc/v3/products?search='.$name;
         return  json_decode($this->getApiWoocomere($url));
//        return  count(json_decode($this->getApiWoocomere($url)));

    }


    public function our_Story_page()
    {

        $url = 'https://www.staging.balooworld.ca/wp-json/wp/v2/pages/53/';
        return json_decode($this->getApi($url));
    }


    //    Call Auth Api

    public function getSetting()
    {
        $url = $this->url_host . "wp-json/wp/v2/settings";
        return json_decode($this->authGetApi($url));
    }

    public function getThemeInfo()
    {
        $url_function = 'wp/v2/themes';
        $url = $this->url_host . "wp-json/" . $url_function;
        return json_decode($this->authGetApi($url));
    }

    /*
    Config For Call Api
    1: If Api is Not Auth or Of Woocomere use getApi function
    2: If Api is Get Setting Or Must Auth use authGetApi function
    */



    // This Api Will Call For Some Api Require Auth
    private function authGetApi($url, $method = 'GET')
    {
        $consumer_key = 'boigaming1411@gmail.com';

        //        $consumer_secret = 'XFbW EAa6 1SBH ruHM uOnS o8SR';
        $consumer_secret = 'f3CJ p4sd 12lR wtJ8 zhKY Ik7A';



        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key . ':' . $consumer_secret)
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
        //       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return $resp;


    }

    private function getApi($url, $method = 'GET')
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }


    private function getApiWoocomere($url, $method = 'GET')
    {



        $consumer_key = 'ck_3aacf618ef5a2e9c70aa2608c02740489ee49ad0';
        $consumer_secret = 'cs_b204adf2a666763de8ec813e97580dd189bdc956';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key . ':' . $consumer_secret)
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
        //       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return $resp;
    }

}
