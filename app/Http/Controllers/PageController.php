<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ProductController;

class PageController extends Controller
{
    private  string $title = "unknow";
    private string $desc = "unknow";

    public function home()
    {
        return view('home.home');
    }
    public function shop(Request $request)
    {  
        if($request){
            $data = app('App\Http\Controllers\CloneShopController')->searchbyprototype($request);
            return view('shop.shop',['data'=> $data, 'request'=>$request]);
        }
        else{
            return view('shop.shop');
        }
        
    }
    
    public function search(Request $request)
    {  
        $data = app('App\Http\Controllers\CloneShopController')->searchbyprototype($request);
        return view('search_result.search_result',['data'=> $data, 'query'=>$request->s,'product_cat'=>$request->product_cat]);
    }

    public function sale()
    {
        return view('sale.sale');
    }

    public function about_us()
    {
        return view('about-us.about-us');
    }

    public function blog()
    {
        return view('blog.blog');
    }

    public function contact()
    {
        return view('contact.contact');
    }

    public function my_account()
    {
        return view('my-account.my-account');
    }

    public function my_account_lost_pw()
    {
        return view('my-account.lostpassword.lostpassword');
    }

     public function cart()
    {
        return view('cart.cart');
    }

    public function product_detail($id)
    {
        $data = app('App\Http\Controllers\ProductController')->get_Product_Details($id);
        return view('product-detail.product-detail',['product' => $data]);
    }

     public function blog_detail($id)
    {
        $data = app('App\Http\Controllers\PostController')->get_blog_detail($id);
        return view('blog-detail.blog-detail',['blog' => $data]);
    }

}
