<!DOCTYPE html>
<html dir="ltr" lang="en-US" prefix="og: https://ogp.me/ns#">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="profile" href="https://gmpg.org/xfn/11" />
        <title>Contact - Linh Linh Shop</title>

        <!-- All in One SEO 4.2.7.1 - aioseo.com -->
        <meta
            name="description"
            content="Looking Forward Find the home you always dreamed about Contact Lorem ipsum dolor sit ametcons etur adipiscing elit hello@yourdomain.com (442) 7621 3445 London SW3 4LY, United Kingdom+44 20 9023 5647uk@domain.com Social Follow us and stay updatedwith all the news and offers Facebook Twitter LinkedIn Jobs jobs@yourdomain.com(43) 677 889 9988 Press press@yourdomain.com(43) 677 889 5543 Finance"
        />
        <meta name="robots" content="noindex, nofollow, max-image-preview:large" />
        <link rel="canonical" href="/contact/" />
        <meta name="generator" content="All in One SEO (AIOSEO) 4.2.7.1 " />
        <meta property="og:locale" content="en_US" />
        <meta property="og:site_name" content="Linh Linh Shop - Professional Alternations &amp; Repairs" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Contact - Linh Linh Shop" />
        <meta
            property="og:description"
            content="Looking Forward Find the home you always dreamed about Contact Lorem ipsum dolor sit ametcons etur adipiscing elit hello@yourdomain.com (442) 7621 3445 London SW3 4LY, United Kingdom+44 20 9023 5647uk@domain.com Social Follow us and stay updatedwith all the news and offers Facebook Twitter LinkedIn Jobs jobs@yourdomain.com(43) 677 889 9988 Press press@yourdomain.com(43) 677 889 5543 Finance"
        />
        <meta property="og:url" content="/contact/" />
        <meta property="article:published_time" content="2019-04-18T11:12:45+00:00" />
        <meta property="article:modified_time" content="2019-04-18T11:12:45+00:00" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Contact - Linh Linh Shop" />
        <meta
            name="twitter:description"
            content="Looking Forward Find the home you always dreamed about Contact Lorem ipsum dolor sit ametcons etur adipiscing elit hello@yourdomain.com (442) 7621 3445 London SW3 4LY, United Kingdom+44 20 9023 5647uk@domain.com Social Follow us and stay updatedwith all the news and offers Facebook Twitter LinkedIn Jobs jobs@yourdomain.com(43) 677 889 9988 Press press@yourdomain.com(43) 677 889 5543 Finance"
        />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "BreadcrumbList",
                        "@id": "\/contact\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "\/",
                                    "name": "Home",
                                    "description": "Product categories Men Women Jewelry Watch Accessories Baby Sport Summer Sale Shop Now Learn More Men's Fashion Shop Now Women's Fashion Shop Now Featured Best Selling Recent Sale Featured Black pants $89 – $99 Lựa chọn các tùy chọn Giảm giá! Modern $25 $19 Thêm vào giỏ hàng Giảm giá! Single Shirt $30 $22 Thêm",
                                    "url": "\/"
                                },
                                "nextItem": "\/contact\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "\/contact\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "\/contact\/",
                                    "name": "Contact",
                                    "description": "Looking Forward Find the home you always dreamed about Contact Lorem ipsum dolor sit ametcons etur adipiscing elit hello@yourdomain.com (442) 7621 3445 London SW3 4LY, United Kingdom+44 20 9023 5647uk@domain.com Social Follow us and stay updatedwith all the news and offers Facebook Twitter LinkedIn Jobs jobs@yourdomain.com(43) 677 889 9988 Press press@yourdomain.com(43) 677 889 5543 Finance",
                                    "url": "\/contact\/"
                                },
                                "previousItem": "\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "Organization",
                        "@id": "\/#organization",
                        "name": "DND Cleaners",
                        "url": "\/"
                    },
                    {
                        "@type": "WebPage",
                        "@id": "\/contact\/#webpage",
                        "url": "\/contact\/",
                        "name": "Contact - Linh Linh Shop",
                        "description": "Looking Forward Find the home you always dreamed about Contact Lorem ipsum dolor sit ametcons etur adipiscing elit hello@yourdomain.com (442) 7621 3445 London SW3 4LY, United Kingdom+44 20 9023 5647uk@domain.com Social Follow us and stay updatedwith all the news and offers Facebook Twitter LinkedIn Jobs jobs@yourdomain.com(43) 677 889 9988 Press press@yourdomain.com(43) 677 889 5543 Finance",
                        "inLanguage": "en-US",
                        "isPartOf": { "@id": "\/#website" },
                        "breadcrumb": { "@id": "\/contact\/#breadcrumblist" },
                        "datePublished": "2019-04-18T11:12:45+00:00",
                        "dateModified": "2019-04-18T11:12:45+00:00"
                    },
                    {
                        "@type": "WebSite",
                        "@id": "\/#website",
                        "url": "\/",
                        "name": "Linh Linh Shop",
                        "description": "Professional Alternations & Repairs",
                        "inLanguage": "en-US",
                        "publisher": { "@id": "\/#organization" }
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->

        
        <link
            rel="stylesheet"
            id="elementor-icons-css"
            href="/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.16.0"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-frontend-css"
            href="/wp-content/plugins/elementor/assets/css/frontend-lite.min.css?ver=3.8.1"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-post-1847-css"
            href="/wp-content/uploads/elementor/css/post-1847.css?ver=1668492633"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="etww-frontend-css"
            href="/wp-content/plugins/envo-elementor-for-woocommerce/assets/css/etww-frontend.min.css?ver=d6eb82c324cab2f36e345863fad95090"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-pro-css"
            href="/wp-content/plugins/elementor-pro/assets/css/frontend-lite.min.css?ver=3.8.0"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-global-css"
            href="/wp-content/uploads/elementor/css/global.css?ver=1668492633"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-post-48-css"
            href="/wp-content/uploads/elementor/css/post-48.css?ver=1668520254"
            type="text/css"
            media="all"
        />
        
        <link
            rel="stylesheet"
            id="google-fonts-1-css"
            href="https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=swap&#038;ver=6.1"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-icons-shared-0-css"
            href="/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-icons-fa-regular-css"
            href="/wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min.css?ver=5.15.3"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-icons-fa-solid-css"
            href="/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.15.3"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-icons-fa-brands-css"
            href="/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.15.3"
            type="text/css"
            media="all"
        />
        @include('public.header')
    </head>
    <body
        data-rsssl="1"
        id="blog"
        class="page-template page-template-template-parts page-template-template-page-builders page-template-template-partstemplate-page-builders-php page page-id-48 theme-envo-storefront woocommerce-no-js elementor-default elementor-kit-1847 elementor-page elementor-page-48"
    >
        @include('public.top-content')
        <div class="page-wrap">
            @include('public.top-bar-section')

            <div class="main-menu">
                 <nav id="site-navigation" class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-header">
                            <span class="navbar-brand brand-absolute visible-xs">Menu</span>
                            <div class="mobile-cart visible-xs">
                                <div class="header-cart">
                                    <div class="header-cart-block">
                                        <div class="header-cart-inner">
                                            <a class="cart-contents" href="/cart/" title="View your shopping cart">
                                                <i class="fa fa-shopping-bag"><span class="count">0</span></i>
                                                <div class="amount-cart">&#036;0</div>
                                            </a>
                                            <ul class="site-header-cart menu list-unstyled text-center">
                                                <li>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mobile-account visible-xs">
                                <div class="header-my-account">
                                    <div class="header-login">
                                        <a href="/my-account/" title="My Account">
                                            <i class="fa fa-user-circle-o"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a href="#" id="main-menu-panel" class="open-panel" data-panel="main-menu-panel">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                        <div id="my-menu" class="menu-container">
                            <ul id="menu-main-menu" class="nav navbar-nav navbar-left">
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2048"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-2048 nav-item"
                                >
                                    <a title="Home" href="/" class="nav-link">Home</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2060"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2060 nav-item"
                                >
                                    <a title="Shop" href="/shop/" class="nav-link" >Shop</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2062"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2062 nav-item"
                                >
                                    <a title="Sale" href="/sale/" class="nav-link"
                                        >Sale<span class="menu-description">Hot!</span></a
                                    >
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2046"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2046 nav-item"
                                >
                                    <a title="About Us" href="/about-us/" class="nav-link">About Us</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2061"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2061 nav-item"
                                >
                                    <a title="Blog" href="/blog/" class="nav-link">Blog</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2047"
                                    class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item current_page_item active menu-item-2047 nav-item"
                                >
                                    <a title="Contact" href="/contact/" class="nav-link" aria-current="page">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

            <div id="site-content" class="page-builders" role="main">
                <div class="page-builders-content-area">
                    <!-- start content container -->
                    <div class="post-48 page type-page status-publish hentry">
                        <div data-elementor-type="wp-post" data-elementor-id="48" class="elementor elementor-48">
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-556e4afc elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="556e4afc"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-background-overlay"></div>
                                <div class="elementor-container elementor-column-gap-default">
                                    <div
                                        class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-dfde28a"
                                        data-id="dfde28a"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-7a2f75f9 elementor-widget elementor-widget-menu-anchor"
                                                data-id="7a2f75f9"
                                                data-element_type="widget"
                                                data-widget_type="menu-anchor.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        body.elementor-page .elementor-widget-menu-anchor {
                                                            margin-bottom: 0;
                                                        }
                                                    </style>
                                                    <div id="home" class="elementor-menu-anchor"></div>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-71543eb0 elementor-widget elementor-widget-heading"
                                                data-id="71543eb0"
                                                data-element_type="widget"
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-heading-title {
                                                            padding: 0;
                                                            margin: 0;
                                                            line-height: 1;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title[class*="elementor-size-"]
                                                            > a {
                                                            color: inherit;
                                                            font-size: inherit;
                                                            line-height: inherit;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-small {
                                                            font-size: 15px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-medium {
                                                            font-size: 19px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-large {
                                                            font-size: 29px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-xl {
                                                            font-size: 39px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-xxl {
                                                            font-size: 59px;
                                                        }
                                                    </style>
                                                    <h1 class="elementor-heading-title elementor-size-large">
                                                        Looking Forward
                                                    </h1>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-75e8ff0f elementor-widget-divider--view-line elementor-widget elementor-widget-divider"
                                                data-id="75e8ff0f"
                                                data-element_type="widget"
                                                data-widget_type="divider.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-widget-divider {
                                                            --divider-border-style: none;
                                                            --divider-border-width: 1px;
                                                            --divider-color: #2c2c2c;
                                                            --divider-icon-size: 20px;
                                                            --divider-element-spacing: 10px;
                                                            --divider-pattern-height: 24px;
                                                            --divider-pattern-size: 20px;
                                                            --divider-pattern-url: none;
                                                            --divider-pattern-repeat: repeat-x;
                                                        }
                                                        .elementor-widget-divider .elementor-divider {
                                                            display: -webkit-box;
                                                            display: -ms-flexbox;
                                                            display: flex;
                                                        }
                                                        .elementor-widget-divider .elementor-divider__text {
                                                            font-size: 15px;
                                                            line-height: 1;
                                                            max-width: 95%;
                                                        }
                                                        .elementor-widget-divider .elementor-divider__element {
                                                            margin: 0 var(--divider-element-spacing);
                                                            -ms-flex-negative: 0;
                                                            flex-shrink: 0;
                                                        }
                                                        .elementor-widget-divider .elementor-icon {
                                                            font-size: var(--divider-icon-size);
                                                        }
                                                        .elementor-widget-divider .elementor-divider-separator {
                                                            display: -webkit-box;
                                                            display: -ms-flexbox;
                                                            display: flex;
                                                            margin: 0;
                                                            direction: ltr;
                                                        }
                                                        .elementor-widget-divider--view-line_icon
                                                            .elementor-divider-separator,
                                                        .elementor-widget-divider--view-line_text
                                                            .elementor-divider-separator {
                                                            -webkit-box-align: center;
                                                            -ms-flex-align: center;
                                                            align-items: center;
                                                        }
                                                        .elementor-widget-divider--view-line_icon
                                                            .elementor-divider-separator:after,
                                                        .elementor-widget-divider--view-line_icon
                                                            .elementor-divider-separator:before,
                                                        .elementor-widget-divider--view-line_text
                                                            .elementor-divider-separator:after,
                                                        .elementor-widget-divider--view-line_text
                                                            .elementor-divider-separator:before {
                                                            display: block;
                                                            content: "";
                                                            border-bottom: 0;
                                                            -webkit-box-flex: 1;
                                                            -ms-flex-positive: 1;
                                                            flex-grow: 1;
                                                            border-top: var(--divider-border-width)
                                                                var(--divider-border-style) var(--divider-color);
                                                        }
                                                        .elementor-widget-divider--element-align-left
                                                            .elementor-divider
                                                            .elementor-divider-separator
                                                            > .elementor-divider__svg:first-of-type {
                                                            -webkit-box-flex: 0;
                                                            -ms-flex-positive: 0;
                                                            flex-grow: 0;
                                                            -ms-flex-negative: 100;
                                                            flex-shrink: 100;
                                                        }
                                                        .elementor-widget-divider--element-align-left
                                                            .elementor-divider-separator:before {
                                                            content: none;
                                                        }
                                                        .elementor-widget-divider--element-align-left
                                                            .elementor-divider__element {
                                                            margin-left: 0;
                                                        }
                                                        .elementor-widget-divider--element-align-right
                                                            .elementor-divider
                                                            .elementor-divider-separator
                                                            > .elementor-divider__svg:last-of-type {
                                                            -webkit-box-flex: 0;
                                                            -ms-flex-positive: 0;
                                                            flex-grow: 0;
                                                            -ms-flex-negative: 100;
                                                            flex-shrink: 100;
                                                        }
                                                        .elementor-widget-divider--element-align-right
                                                            .elementor-divider-separator:after {
                                                            content: none;
                                                        }
                                                        .elementor-widget-divider--element-align-right
                                                            .elementor-divider__element {
                                                            margin-right: 0;
                                                        }
                                                        .elementor-widget-divider:not(
                                                                .elementor-widget-divider--view-line_text
                                                            ):not(.elementor-widget-divider--view-line_icon)
                                                            .elementor-divider-separator {
                                                            border-top: var(--divider-border-width)
                                                                var(--divider-border-style) var(--divider-color);
                                                        }
                                                        .elementor-widget-divider--separator-type-pattern {
                                                            --divider-border-style: none;
                                                        }
                                                        .elementor-widget-divider--separator-type-pattern.elementor-widget-divider--view-line
                                                            .elementor-divider-separator,
                                                        .elementor-widget-divider--separator-type-pattern:not(
                                                                .elementor-widget-divider--view-line
                                                            )
                                                            .elementor-divider-separator:after,
                                                        .elementor-widget-divider--separator-type-pattern:not(
                                                                .elementor-widget-divider--view-line
                                                            )
                                                            .elementor-divider-separator:before,
                                                        .elementor-widget-divider--separator-type-pattern:not(
                                                                [class*="elementor-widget-divider--view"]
                                                            )
                                                            .elementor-divider-separator {
                                                            width: 100%;
                                                            min-height: var(--divider-pattern-height);
                                                            -webkit-mask-size: var(--divider-pattern-size) 100%;
                                                            mask-size: var(--divider-pattern-size) 100%;
                                                            -webkit-mask-repeat: var(--divider-pattern-repeat);
                                                            mask-repeat: var(--divider-pattern-repeat);
                                                            background-color: var(--divider-color);
                                                            -webkit-mask-image: var(--divider-pattern-url);
                                                            mask-image: var(--divider-pattern-url);
                                                        }
                                                        .elementor-widget-divider--no-spacing {
                                                            --divider-pattern-size: auto;
                                                        }
                                                        .elementor-widget-divider--bg-round {
                                                            --divider-pattern-repeat: round;
                                                        }
                                                        .rtl .elementor-widget-divider .elementor-divider__text {
                                                            direction: rtl;
                                                        }
                                                        .e-con-inner > .elementor-widget-divider,
                                                        .e-con > .elementor-widget-divider {
                                                            width: var(--container-widget-width, 100%);
                                                        }
                                                    </style>
                                                    <div class="elementor-divider">
                                                        <span class="elementor-divider-separator"> </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-4f0e3029 elementor-widget elementor-widget-heading"
                                                data-id="4f0e3029"
                                                data-element_type="widget"
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <h3 class="elementor-heading-title elementor-size-large">
                                                        Find the home you always dreamed about
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-4e4c06e4 elementor-section-full_width elementor-section-height-min-height elementor-section-items-stretch elementor-section-content-middle elementor-section-height-default"
                                data-id="4e4c06e4"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div
                                        class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-65626cdf"
                                        data-id="65626cdf"
                                        data-element_type="column"
                                        data-settings='{"background_background":"none"}'
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-28f311ba elementor-widget elementor-widget-heading"
                                                data-id="28f311ba"
                                                data-element_type="widget"
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <h2 class="elementor-heading-title elementor-size-large">
                                                        Contact
                                                    </h2>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-28e8c7ce elementor-widget-divider--view-line elementor-widget elementor-widget-divider"
                                                data-id="28e8c7ce"
                                                data-element_type="widget"
                                                data-widget_type="divider.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-divider">
                                                        <span class="elementor-divider-separator"> </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-6eb0fc4b elementor-widget elementor-widget-heading"
                                                data-id="6eb0fc4b"
                                                data-element_type="widget"
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <p class="elementor-heading-title elementor-size-large">
                                                        Lorem ipsum dolor sit ametcons etur adipiscing elit
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-1cda619d"
                                        data-id="1cda619d"
                                        data-element_type="column"
                                        data-settings='{"background_background":"classic"}'
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-65892f89 elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list"
                                                data-id="65892f89"
                                                data-element_type="widget"
                                                data-widget_type="icon-list.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <link
                                                        rel="stylesheet"
                                                        href="/wp-content/plugins/elementor/assets/css/widget-icon-list.min.css"
                                                    />
                                                    <ul class="elementor-icon-list-items">
                                                        <li class="elementor-icon-list-item">
                                                            <span class="elementor-icon-list-icon">
                                                                <i aria-hidden="true" class="far fa-envelope"></i>
                                                            </span>
                                                            <span class="elementor-icon-list-text"
                                                                >hello@yourdomain.com</span
                                                            >
                                                        </li>
                                                        <li class="elementor-icon-list-item">
                                                            <span class="elementor-icon-list-icon">
                                                                <i aria-hidden="true" class="fas fa-phone"></i>
                                                            </span>
                                                            <span class="elementor-icon-list-text"
                                                                >(442) 7621 3445</span
                                                            >
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-2479edec elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default"
                                data-id="2479edec"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-no">
                                    <div
                                        class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-72cf0f64"
                                        data-id="72cf0f64"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-c55c8a4 elementor-widget elementor-widget-google_maps"
                                                data-id="c55c8a4"
                                                data-element_type="widget"
                                                data-widget_type="google_maps.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-widget-google_maps .elementor-widget-container {
                                                            overflow: hidden;
                                                        }
                                                        .elementor-widget-google_maps iframe {
                                                            height: 300px;
                                                        }
                                                    </style>
                                                    <div class="elementor-custom-embed">
                                                        <iframe
                                                            frameborder="0"
                                                            scrolling="no"
                                                            marginheight="0"
                                                            marginwidth="0"
                                                            src="https://maps.google.com/maps?q=SW3%204LY%2C%20United%20Kingdom&#038;t=m&#038;z=14&#038;output=embed&#038;iwloc=near"
                                                            title="SW3 4LY, United Kingdom"
                                                            aria-label="SW3 4LY, United Kingdom"
                                                        ></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-7324b409"
                                        data-id="7324b409"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-7acb4f6 elementor-widget elementor-widget-heading"
                                                data-id="7acb4f6"
                                                data-element_type="widget"
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <h2 class="elementor-heading-title elementor-size-large">London</h2>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-36584420 elementor-widget-divider--view-line elementor-widget elementor-widget-divider"
                                                data-id="36584420"
                                                data-element_type="widget"
                                                data-widget_type="divider.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-divider">
                                                        <span class="elementor-divider-separator"> </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-2cebb61 elementor-widget elementor-widget-heading"
                                                data-id="2cebb61"
                                                data-element_type="widget"
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <p class="elementor-heading-title elementor-size-large">
                                                        SW3 4LY, United Kingdom+44 20 9023 5647uk@domain.com
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-5a57dacf elementor-section-full_width elementor-section-height-min-height elementor-section-items-stretch elementor-section-content-middle elementor-section-height-default"
                                data-id="5a57dacf"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div
                                        class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-63595cf9"
                                        data-id="63595cf9"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-5e7e0abf elementor-widget elementor-widget-heading"
                                                data-id="5e7e0abf"
                                                data-element_type="widget"
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <h2 class="elementor-heading-title elementor-size-large">Social</h2>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-7e6bc7b6 elementor-widget-divider--view-line elementor-widget elementor-widget-divider"
                                                data-id="7e6bc7b6"
                                                data-element_type="widget"
                                                data-widget_type="divider.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-divider">
                                                        <span class="elementor-divider-separator"> </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-34372567 elementor-widget elementor-widget-heading"
                                                data-id="34372567"
                                                data-element_type="widget"
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <p class="elementor-heading-title elementor-size-large">
                                                        Follow us and stay updatedwith all the news and offers
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-39b0188a"
                                        data-id="39b0188a"
                                        data-element_type="column"
                                        data-settings='{"background_background":"classic"}'
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-7eeda6e4 elementor-section-height-default elementor-section-boxed elementor-section-height-default"
                                                data-id="7eeda6e4"
                                                data-element_type="section"
                                            >
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div
                                                        class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-67848825 elementor-sm-33"
                                                        data-id="67848825"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-12005262 elementor-view-default elementor-widget elementor-widget-icon"
                                                                data-id="12005262"
                                                                data-element_type="widget"
                                                                data-widget_type="icon.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-icon-wrapper">
                                                                        <div class="elementor-icon">
                                                                            <i
                                                                                aria-hidden="true"
                                                                                class="fab fa-facebook-f"
                                                                            ></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-7faa733f elementor-widget elementor-widget-heading"
                                                                data-id="7faa733f"
                                                                data-element_type="widget"
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h5
                                                                        class="elementor-heading-title elementor-size-large"
                                                                    >
                                                                        Facebook
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-5b05b994 elementor-sm-33"
                                                        data-id="5b05b994"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-62e791e8 elementor-view-default elementor-widget elementor-widget-icon"
                                                                data-id="62e791e8"
                                                                data-element_type="widget"
                                                                data-widget_type="icon.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-icon-wrapper">
                                                                        <div class="elementor-icon">
                                                                            <i
                                                                                aria-hidden="true"
                                                                                class="fab fa-twitter"
                                                                            ></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-70727267 elementor-widget elementor-widget-heading"
                                                                data-id="70727267"
                                                                data-element_type="widget"
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h5
                                                                        class="elementor-heading-title elementor-size-large"
                                                                    >
                                                                        Twitter
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-4c59f844 elementor-sm-33"
                                                        data-id="4c59f844"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-904a7d5 elementor-view-default elementor-widget elementor-widget-icon"
                                                                data-id="904a7d5"
                                                                data-element_type="widget"
                                                                data-widget_type="icon.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-icon-wrapper">
                                                                        <div class="elementor-icon">
                                                                            <i
                                                                                aria-hidden="true"
                                                                                class="fab fa-linkedin-in"
                                                                            ></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-6aa1e860 elementor-widget elementor-widget-heading"
                                                                data-id="6aa1e860"
                                                                data-element_type="widget"
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h5
                                                                        class="elementor-heading-title elementor-size-large"
                                                                    >
                                                                        LinkedIn
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-3403254c elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="3403254c"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div
                                        class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-4ff1ccd7"
                                        data-id="4ff1ccd7"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-545bf1e0 elementor-view-default elementor-widget elementor-widget-icon"
                                                data-id="545bf1e0"
                                                data-element_type="widget"
                                                data-widget_type="icon.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-icon-wrapper">
                                                        <a class="elementor-icon" href="#home">
                                                            <i aria-hidden="true" class="fas fa-angle-double-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-11230707 elementor-section-height-default elementor-section-boxed elementor-section-height-default"
                                                data-id="11230707"
                                                data-element_type="section"
                                            >
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div
                                                        class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-48059724"
                                                        data-id="48059724"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-62a32a44 elementor-widget elementor-widget-heading"
                                                                data-id="62a32a44"
                                                                data-element_type="widget"
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h3
                                                                        class="elementor-heading-title elementor-size-large"
                                                                    >
                                                                        Jobs
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-340a29d elementor-widget-divider--view-line elementor-widget elementor-widget-divider"
                                                                data-id="340a29d"
                                                                data-element_type="widget"
                                                                data-widget_type="divider.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-divider">
                                                                        <span class="elementor-divider-separator">
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-430cee2b elementor-widget elementor-widget-heading"
                                                                data-id="430cee2b"
                                                                data-element_type="widget"
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h5
                                                                        class="elementor-heading-title elementor-size-large"
                                                                    >
                                                                        jobs@yourdomain.com(43) 677 889 9988
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-751f9aca"
                                                        data-id="751f9aca"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-68c9d226 elementor-widget elementor-widget-heading"
                                                                data-id="68c9d226"
                                                                data-element_type="widget"
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h3
                                                                        class="elementor-heading-title elementor-size-large"
                                                                    >
                                                                        Press
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-5fe74fc8 elementor-widget-divider--view-line elementor-widget elementor-widget-divider"
                                                                data-id="5fe74fc8"
                                                                data-element_type="widget"
                                                                data-widget_type="divider.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-divider">
                                                                        <span class="elementor-divider-separator">
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-16519e08 elementor-widget elementor-widget-heading"
                                                                data-id="16519e08"
                                                                data-element_type="widget"
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h5
                                                                        class="elementor-heading-title elementor-size-large"
                                                                    >
                                                                        press@yourdomain.com(43) 677 889 5543
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-55851a8d"
                                                        data-id="55851a8d"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-52b65f2c elementor-widget elementor-widget-heading"
                                                                data-id="52b65f2c"
                                                                data-element_type="widget"
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h3
                                                                        class="elementor-heading-title elementor-size-large"
                                                                    >
                                                                        Finance
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-1dfe52fe elementor-widget-divider--view-line elementor-widget elementor-widget-divider"
                                                                data-id="1dfe52fe"
                                                                data-element_type="widget"
                                                                data-widget_type="divider.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-divider">
                                                                        <span class="elementor-divider-separator">
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-2b77deeb elementor-widget elementor-widget-heading"
                                                                data-id="2b77deeb"
                                                                data-element_type="widget"
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h5
                                                                        class="elementor-heading-title elementor-size-large"
                                                                    >
                                                                        finance@yourdomain.com(43) 677 889 5663
                                                                    </h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>

                    <!-- end content container -->
                </div>
                <!-- end main-container -->
            </div>
            <!-- end page-area -->

            @include('public.footer')
        </div>
        <!-- end page-wrap -->
        @include('public.script')
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.8.0"
            id="elementor-pro-webpack-runtime-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.8.1"
            id="elementor-webpack-runtime-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.8.1"
            id="elementor-frontend-modules-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/vendor/regenerator-runtime.min.js?ver=0.13.9"
            id="regenerator-runtime-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=3.15.0"
            id="wp-polyfill-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/hooks.min.js?ver=4169d3cf8e8d95a3d6d5"
            id="wp-hooks-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/i18n.min.js?ver=9e794f35a71bb98672ae"
            id="wp-i18n-js"
        ></script>
        <script type="text/javascript" id="wp-i18n-js-after">
            wp.i18n.setLocaleData({ "text directionltr": ["ltr"] });
        </script>
        <script type="text/javascript" id="elementor-pro-frontend-js-before">
            var ElementorProFrontendConfig = {
                ajaxurl: "\/wp-admin\/admin-ajax.php",
                nonce: "ea03ce6765",
                urls: {
                    assets: "\/wp-content\/plugins\/elementor-pro\/assets\/",
                    rest: "\/wp-json\/",
                },
                shareButtonsNetworks: {
                    facebook: { title: "Facebook", has_counter: true },
                    twitter: { title: "Twitter" },
                    linkedin: { title: "LinkedIn", has_counter: true },
                    pinterest: { title: "Pinterest", has_counter: true },
                    reddit: { title: "Reddit", has_counter: true },
                    vk: { title: "VK", has_counter: true },
                    odnoklassniki: { title: "OK", has_counter: true },
                    tumblr: { title: "Tumblr" },
                    digg: { title: "Digg" },
                    skype: { title: "Skype" },
                    stumbleupon: { title: "StumbleUpon", has_counter: true },
                    mix: { title: "Mix" },
                    telegram: { title: "Telegram" },
                    pocket: { title: "Pocket", has_counter: true },
                    xing: { title: "XING", has_counter: true },
                    whatsapp: { title: "WhatsApp" },
                    email: { title: "Email" },
                    print: { title: "Print" },
                },
                woocommerce: {
                    menu_cart: {
                        cart_page_url: "\/cart\/",
                        checkout_page_url: "\/checkout\/",
                        fragments_nonce: "214bd84b63",
                    },
                },
                facebook_sdk: { lang: "en_US", app_id: "" },
                lottie: {
                    defaultAnimationUrl:
                        "\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json",
                },
            };
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.8.0"
            id="elementor-pro-frontend-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2"
            id="elementor-waypoints-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/jquery/ui/core.min.js?ver=1.13.2"
            id="jquery-ui-core-js"
        ></script>
        <script type="text/javascript" id="elementor-frontend-js-before">
            var elementorFrontendConfig = {
                environmentMode: { edit: false, wpPreview: false, isScriptDebug: false },
                i18n: {
                    shareOnFacebook: "Share on Facebook",
                    shareOnTwitter: "Share on Twitter",
                    pinIt: "Pin it",
                    download: "Download",
                    downloadImage: "Download image",
                    fullscreen: "Fullscreen",
                    zoom: "Zoom",
                    share: "Share",
                    playVideo: "Play Video",
                    previous: "Previous",
                    next: "Next",
                    close: "Close",
                },
                is_rtl: false,
                breakpoints: { xs: 0, sm: 480, md: 768, lg: 1025, xl: 1440, xxl: 1600 },
                responsive: {
                    breakpoints: {
                        mobile: { label: "Mobile", value: 767, default_value: 767, direction: "max", is_enabled: true },
                        mobile_extra: {
                            label: "Mobile Extra",
                            value: 880,
                            default_value: 880,
                            direction: "max",
                            is_enabled: false,
                        },
                        tablet: {
                            label: "Tablet",
                            value: 1024,
                            default_value: 1024,
                            direction: "max",
                            is_enabled: true,
                        },
                        tablet_extra: {
                            label: "Tablet Extra",
                            value: 1200,
                            default_value: 1200,
                            direction: "max",
                            is_enabled: false,
                        },
                        laptop: {
                            label: "Laptop",
                            value: 1366,
                            default_value: 1366,
                            direction: "max",
                            is_enabled: false,
                        },
                        widescreen: {
                            label: "Widescreen",
                            value: 2400,
                            default_value: 2400,
                            direction: "min",
                            is_enabled: false,
                        },
                    },
                },
                version: "3.8.1",
                is_static: false,
                experimentalFeatures: {
                    e_dom_optimization: true,
                    e_optimized_assets_loading: true,
                    e_optimized_css_loading: true,
                    a11y_improvements: true,
                    additional_custom_breakpoints: true,
                    e_import_export: true,
                    e_hidden_wordpress_widgets: true,
                    theme_builder_v2: true,
                    "landing-pages": true,
                    "elements-color-picker": true,
                    "favorite-widgets": true,
                    "admin-top-bar": true,
                    "page-transitions": true,
                    notes: true,
                    loop: true,
                    "form-submissions": true,
                    e_scroll_snap: true,
                },
                urls: { assets: "\/wp-content\/plugins\/elementor\/assets\/" },
                settings: { page: [], editorPreferences: [] },
                kit: {
                    active_breakpoints: ["viewport_mobile", "viewport_tablet"],
                    global_image_lightbox: "yes",
                    lightbox_enable_counter: "yes",
                    lightbox_enable_fullscreen: "yes",
                    lightbox_enable_zoom: "yes",
                    lightbox_enable_share: "yes",
                    lightbox_title_src: "title",
                    lightbox_description_src: "description",
                    woocommerce_notices_elements: [],
                },
                post: { id: 48, title: "Contact%20-%20Linh%20Linh%20Shop", excerpt: "", featuredImage: false },
            };
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.8.1"
            id="elementor-frontend-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor-pro/assets/js/elements-handlers.min.js?ver=3.8.0"
            id="pro-elements-handlers-js"
        ></script>
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-11-15 14:08:47 by W3 Total Cache
--></html>
