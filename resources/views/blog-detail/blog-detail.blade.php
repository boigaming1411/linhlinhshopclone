<!DOCTYPE html>
<html dir="ltr" lang="en-US" prefix="og: https://ogp.me/ns#">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="profile" href="https://gmpg.org/xfn/11" />
        <title><?php echo $blog->title->rendered; ?> - Linh Linh Shop</title>

        <!-- All in One SEO 4.2.7.1 - aioseo.com -->
        <meta
            name="description"
            content="Integer in sapien. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi scelerisque luctus velit. Curabitur vitae diam non enim vestibulum interdum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Curabitur sagittis"
        />
        <meta name="robots" content="noindex, nofollow, max-image-preview:large" />
        <link rel="canonical" href="/aliquam-erat-volutpat/" />
        <meta name="generator" content="All in One SEO (AIOSEO) 4.2.7.1 " />
        <meta property="og:locale" content="en_US" />
        <meta property="og:site_name" content="Linh Linh Shop - Professional Alternations &amp; Repairs" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Aliquam erat volutpat - Linh Linh Shop" />
        <meta
            property="og:description"
            content="Integer in sapien. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi scelerisque luctus velit. Curabitur vitae diam non enim vestibulum interdum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Curabitur sagittis"
        />
        <meta property="og:url" content="/aliquam-erat-volutpat/" />
        <meta property="article:published_time" content="2019-04-29T11:06:05+00:00" />
        <meta property="article:modified_time" content="2019-04-29T11:06:05+00:00" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Aliquam erat volutpat - Linh Linh Shop" />
        <meta
            name="twitter:description"
            content="Integer in sapien. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi scelerisque luctus velit. Curabitur vitae diam non enim vestibulum interdum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Curabitur sagittis"
        />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "BlogPosting",
                        "@id": "httpsstaging.dndcleaners.ca\/aliquam-erat-volutpat\/#blogposting",
                        "name": "Aliquam erat volutpat - Linh Linh Shop",
                        "headline": "Aliquam erat volutpat",
                        "author": { "@id": "httpsstaging.dndcleaners.ca\/author\/hugo\/#author" },
                        "publisher": { "@id": "httpsstaging.dndcleaners.ca\/#organization" },
                        "image": {
                            "@type": "ImageObject",
                            "url": "httpsstaging.dndcleaners.ca\/wp-content\/uploads\/2019\/40\/StockSnap_QXVSHD5FLF.jpg",
                            "width": 1280,
                            "height": 854
                        },
                        "datePublished": "2019-04-29T11:06:05+00:00",
                        "dateModified": "2019-04-29T11:06:05+00:00",
                        "inLanguage": "en-US",
                        "mainEntityOfPage": { "@id": "httpsstaging.dndcleaners.ca\/aliquam-erat-volutpat\/#webpage" },
                        "isPartOf": { "@id": "httpsstaging.dndcleaners.ca\/aliquam-erat-volutpat\/#webpage" },
                        "articleSection": "Category #2, tag 3, tag 4, tag1"
                    },
                    {
                        "@type": "BreadcrumbList",
                        "@id": "httpsstaging.dndcleaners.ca\/aliquam-erat-volutpat\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "Product categories Men Women Jewelry Watch Accessories Baby Sport Summer Sale Shop Now Learn More Men's Fashion Shop Now Women's Fashion Shop Now Featured Best Selling Recent Sale Featured Black pants $89 – $99 Lựa chọn các tùy chọn Giảm giá! Modern $25 $19 Thêm vào giỏ hàng Giảm giá! Single Shirt $30 $22 Thêm",
                                    "url": "httpsstaging.dndcleaners.ca\/"
                                },
                                "nextItem": "httpsstaging.dndcleaners.ca\/aliquam-erat-volutpat\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/aliquam-erat-volutpat\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/aliquam-erat-volutpat\/",
                                    "name": "Aliquam erat volutpat",
                                    "description": "Integer in sapien. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi scelerisque luctus velit. Curabitur vitae diam non enim vestibulum interdum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Curabitur sagittis",
                                    "url": "httpsstaging.dndcleaners.ca\/aliquam-erat-volutpat\/"
                                },
                                "previousItem": "httpsstaging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "Organization",
                        "@id": "httpsstaging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "httpsstaging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "WebPage",
                        "@id": "httpsstaging.dndcleaners.ca\/aliquam-erat-volutpat\/#webpage",
                        "url": "httpsstaging.dndcleaners.ca\/aliquam-erat-volutpat\/",
                        "name": "Aliquam erat volutpat - Linh Linh Shop",
                        "description": "Integer in sapien. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi scelerisque luctus velit. Curabitur vitae diam non enim vestibulum interdum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Curabitur sagittis",
                        "inLanguage": "en-US",
                        "isPartOf": { "@id": "httpsstaging.dndcleaners.ca\/#website" },
                        "breadcrumb": { "@id": "httpsstaging.dndcleaners.ca\/aliquam-erat-volutpat\/#breadcrumblist" },
                        "author": "httpsstaging.dndcleaners.ca\/author\/hugo\/#author",
                        "creator": "httpsstaging.dndcleaners.ca\/author\/hugo\/#author",
                        "image": {
                            "@type": "ImageObject",
                            "url": "httpsstaging.dndcleaners.ca\/wp-content\/uploads\/2019\/40\/StockSnap_QXVSHD5FLF.jpg",
                            "@id": "httpsstaging.dndcleaners.ca\/#mainImage",
                            "width": 1280,
                            "height": 854
                        },
                        "primaryImageOfPage": {
                            "@id": "httpsstaging.dndcleaners.ca\/aliquam-erat-volutpat\/#mainImage"
                        },
                        "datePublished": "2019-04-29T11:06:05+00:00",
                        "dateModified": "2019-04-29T11:06:05+00:00"
                    },
                    {
                        "@type": "WebSite",
                        "@id": "httpsstaging.dndcleaners.ca\/#website",
                        "url": "httpsstaging.dndcleaners.ca\/",
                        "name": "Linh Linh Shop",
                        "description": "Professional Alternations & Repairs",
                        "inLanguage": "en-US",
                        "publisher": { "@id": "httpsstaging.dndcleaners.ca\/#organization" }
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->
            @include('public.header')
      
        <link
            rel="alternate"
            type="application/rss+xml"
            title="Linh Linh Shop &raquo; Aliquam erat volutpat Comments Feed"
            href="/aliquam-erat-volutpat/feed/"
        />
    </head>
    <body
        data-rsssl="1"
        id="blog"
        class="post-template-default single single-post postid-2057 single-format-standard theme-envo-storefront woocommerce-no-js elementor-default elementor-kit-1847"
    >
        @include('public.top-content')
        <div class="page-wrap">
            @include('public.top-bar-section')

            <div class="main-menu">
                 <nav id="site-navigation" class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-header">
                            <span class="navbar-brand brand-absolute visible-xs">Menu</span>
                            <div class="mobile-cart visible-xs">
                                <div class="header-cart">
                                    <div class="header-cart-block">
                                        <div class="header-cart-inner">
                                            <a class="cart-contents" href="/cart/" title="View your shopping cart">
                                                <i class="fa fa-shopping-bag"><span class="count">0</span></i>
                                                <div class="amount-cart">&#036;0</div>
                                            </a>
                                            <ul class="site-header-cart menu list-unstyled text-center">
                                                <li>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mobile-account visible-xs">
                                <div class="header-my-account">
                                    <div class="header-login">
                                        <a href="/my-account/" title="My Account">
                                            <i class="fa fa-user-circle-o"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a href="#" id="main-menu-panel" class="open-panel" data-panel="main-menu-panel">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                        <div id="my-menu" class="menu-container">
                            <ul id="menu-main-menu" class="nav navbar-nav navbar-left">
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2048"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-2048 nav-item"
                                >
                                    <a title="Home" href="/" class="nav-link">Home</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2060"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2060 nav-item"
                                >
                                    <a title="Shop" href="/shop/" class="nav-link">Shop</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2062"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2062 nav-item"
                                >
                                    <a title="Sale" href="/sale/" class="nav-link"
                                        >Sale<span class="menu-description">Hot!</span></a
                                    >
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2046"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2046 nav-item"
                                >
                                    <a title="About Us" href="/about-us/" class="nav-link">About Us</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2061"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2061 nav-item"
                                >
                                    <a title="Blog" href="/blog/" class="nav-link">Blog</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2047"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2047 nav-item"
                                >
                                    <a title="Contact" href="/contact/" class="nav-link">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <div id="site-content" class="container main-container" role="main">
                <div class="page-area">
                    <!-- start content container -->
                    <!-- start content container -->
                    <div class="row">
                        <article class="col-md-9">
                            <div
                                class="post-2057 post type-post status-publish format-standard has-post-thumbnail hentry category-category-2 tag-tag-3 tag-tag-4 tag-tag1"
                            >
                                <div class="news-thumb">
                                    <img
                                        width="1140"
                                        height="641"
                                        src="<?php echo $blog->link_image; ?>"
                                        class="attachment-envo-storefront-single size-envo-storefront-single wp-post-image"
                                        alt=""
                                        decoding="async"
                                        sizes="(max-width: 1140px) 100vw, 1140px"
                                    />
                                </div>
                                <!-- .news-thumb -->
                                <div class="single-head has-thumbnail">
                                    <span class="posted-date"> <?php echo date("M  d,  Y",strtotime($blog->date)); ?> </span>
                                    <span class="author-meta">
                                        <span class="author-meta-by">By</span>
                                        <a href=""> hugo </a>
                                    </span>
                                    <span class="comments-meta">
                                        <a
                                            href=""
                                            rel="nofollow"
                                            title="Comment on Aliquam erat volutpat"
                                        >
                                            0
                                        </a>
                                        <i class="fa fa-comments-o"></i>
                                    </span>
                                    <h1 class="single-title"><?php echo $blog->title->rendered; ?></h1>
                                </div>
                                <div class="single-content">
                                    <div class="single-entry-summary">
                                        <?php echo $blog->content->rendered; ?>
                                    </div>
                                    <!-- .single-entry-summary -->
                                    <div class="entry-footer">
                                        <div class="cat-links">
                                            <span class="space-right">Category</span
                                            ><a href="/category/category-2/">Category #2</a>
                                        </div>
                                        <div class="tags-links">
                                            <span class="space-right">Tags</span><a href="/tag/tag-3/">tag 3</a>
                                            <a href="/tag/tag-4/">tag 4</a> <a href="/tag/tag1/">tag1</a>
                                        </div>
                                    </div>
                                </div>

                                <nav class="navigation post-navigation" aria-label="Posts">
                                    <h2 class="screen-reader-text">Post navigation</h2>
                                    <div class="nav-links">
                                        <div class="nav-previous">
                                            <a href="//" rel="prev"
                                                ><span class="screen-reader-text">Previous Post</span
                                                ><span aria-hidden="true" class="nav-subtitle">Previous</span>
                                                <span class="nav-title"
                                                    ><span class="nav-title-icon-wrapper"
                                                        ><i
                                                            class="fa fa-angle-double-left"
                                                            aria-hidden="true"
                                                        ></i></span
                                                    >Aliquam id dolor</span
                                                ></a
                                            >
                                        </div>
                                        <div class="nav-next">
                                            <a href="/etiam-bibendum-elit-eget-erat/" rel="next"
                                                ><span class="screen-reader-text">Next Post</span
                                                ><span aria-hidden="true" class="nav-subtitle">Next</span>
                                                <span class="nav-title"
                                                    >Etiam bibendum elit eget erat<span class="nav-title-icon-wrapper"
                                                        ><i
                                                            class="fa fa-angle-double-right"
                                                            aria-hidden="true"
                                                        ></i></span></span
                                            ></a>
                                        </div>
                                    </div>
                                </nav>
                                <div class="single-footer">
                                    <div id="comments" class="comments-template">
                                        <div id="respond" class="comment-respond">
                                            <h3 id="reply-title" class="comment-reply-title">
                                                Leave a Reply
                                                <small
                                                    ><a
                                                        rel="nofollow"
                                                        id="cancel-comment-reply-link"
                                                        href="/aliquam-erat-volutpat/#respond"
                                                        style="display: none"
                                                        >Cancel reply</a
                                                    ></small
                                                >
                                            </h3>
                                            <form
                                                action="https://staging.dndcleaners.ca/wp-comments-post.php"
                                                method="post"
                                                id="commentform"
                                                class="comment-form"
                                            >
                                                <p class="comment-notes">
                                                    <span id="email-notes"
                                                        >Your email address will not be published.</span
                                                    >
                                                    <span class="required-field-message"
                                                        >Required fields are marked
                                                        <span class="required">*</span></span
                                                    >
                                                </p>
                                                <p class="comment-form-comment">
                                                    <label for="comment">Comment <span class="required">*</span></label>
                                                    <textarea
                                                        id="comment"
                                                        name="comment"
                                                        cols="45"
                                                        rows="8"
                                                        maxlength="65525"
                                                        required="required"
                                                    ></textarea>
                                                </p>
                                                <p class="comment-form-author">
                                                    <label for="author">Name <span class="required">*</span></label>
                                                    <input
                                                        id="author"
                                                        name="author"
                                                        type="text"
                                                        value=""
                                                        size="30"
                                                        maxlength="245"
                                                        autocomplete="name"
                                                        required="required"
                                                    />
                                                </p>
                                                <p class="comment-form-email">
                                                    <label for="email">Email <span class="required">*</span></label>
                                                    <input
                                                        id="email"
                                                        name="email"
                                                        type="text"
                                                        value=""
                                                        size="30"
                                                        maxlength="100"
                                                        aria-describedby="email-notes"
                                                        autocomplete="email"
                                                        required="required"
                                                    />
                                                </p>
                                                <p class="comment-form-url">
                                                    <label for="url">Website</label>
                                                    <input
                                                        id="url"
                                                        name="url"
                                                        type="text"
                                                        value=""
                                                        size="30"
                                                        maxlength="200"
                                                        autocomplete="url"
                                                    />
                                                </p>
                                                <p class="comment-form-cookies-consent">
                                                    <input
                                                        id="wp-comment-cookies-consent"
                                                        name="wp-comment-cookies-consent"
                                                        type="checkbox"
                                                        value="yes"
                                                    />
                                                    <label for="wp-comment-cookies-consent"
                                                        >Save my name, email, and website in this browser for the next
                                                        time I comment.</label
                                                    >
                                                </p>
                                                <p class="form-submit">
                                                    <input
                                                        name="submit"
                                                        type="submit"
                                                        id="submit"
                                                        class="submit"
                                                        value="Post Comment"
                                                    />
                                                    <input
                                                        type="hidden"
                                                        name="comment_post_ID"
                                                        value="2057"
                                                        id="comment_post_ID"
                                                    />
                                                    <input
                                                        type="hidden"
                                                        name="comment_parent"
                                                        id="comment_parent"
                                                        value="0"
                                                    />
                                                </p>
                                                <p style="display: none !important">
                                                    <label
                                                        >&#916;<textarea
                                                            name="ak_hp_textarea"
                                                            cols="45"
                                                            rows="8"
                                                            maxlength="100"
                                                        ></textarea></label
                                                    ><input type="hidden" id="ak_js_1" name="ak_js" value="244" />
                                                    <script>
                                                        document
                                                            .getElementById("ak_js_1")
                                                            .setAttribute("value", new Date().getTime());
                                                    </script>
                                                </p>
                                            </form>
                                        </div>
                                        <!-- #respond -->
                                    </div>
                                </div>
                            </div>
                        </article>
                        <aside id="sidebar" class="col-md-3">
                            <div id="woocommerce_widget_cart-1" class="widget woocommerce widget_shopping_cart">
                                <div class="widget-title"><h3>Cart</h3></div>
                                <div class="widget_shopping_cart_content"></div>
                            </div>
                            <div id="block-10" class="widget widget_block">
                                <div class="widget-title"><h3>Search Products</h3></div>
                                <div class="wp-widget-group__inner-blocks">
                                    <form
                                        role="search"
                                        method="get"
                                        action="https://staging.dndcleaners.ca/"
                                        class="wp-block-search__button-outside wp-block-search__text-button wp-block-search"
                                    >
                                        <label
                                            for="wp-block-search__input-2"
                                            class="wp-block-search__label screen-reader-text"
                                            >Search</label
                                        >
                                        <div class="wp-block-search__inside-wrapper">
                                            <input
                                                type="search"
                                                id="wp-block-search__input-2"
                                                class="wp-block-search__input wp-block-search__input"
                                                name="s"
                                                value=""
                                                placeholder="Search products..."
                                                required
                                            /><input type="hidden" name="post_type" value="product" /><button
                                                type="submit"
                                                class="wp-block-search__button wp-element-button"
                                            >
                                                Search
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="woocommerce_products-1" class="widget woocommerce widget_products">
                                <div class="widget-title"><h3>Products</h3></div>
                                <div id="side_bar"></div>
                            </div>
                        </aside>
                    </div>
                    <!-- end content container -->
                    <!-- end content container -->
                </div>
                <!-- end main-container -->
            </div>
            <!-- end page-area -->

            @include('public.footer')
        </div>
        <!-- end page-wrap -->
         <script
            type="text/javascript"
            src="/js/vuejs/vue.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/vuejs/moment.min.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/blog-detail.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/sidebar.js"
        ></script>
       @include('public.script')
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-11-15 14:00:15 by W3 Total Cache
--></html>
