<!DOCTYPE html>
<html dir="ltr" lang="en-US" prefix="og: https://ogp.me/ns#">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="profile" href="https://gmpg.org/xfn/11" />
        <title>My account - Linh Linh Shop</title>

        <!-- All in One SEO 4.2.7.1 - aioseo.com -->
        <meta name="robots" content="noindex, nofollow, max-image-preview:large" />
        <link rel="canonical" href="/my-account/" />
        <meta name="generator" content="All in One SEO (AIOSEO) 4.2.7.1 " />
        <meta property="og:locale" content="en_US" />
        <meta property="og:site_name" content="Linh Linh Shop - Quality - Reputation" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="My account - Linh Linh Shop" />
        <meta property="og:url" content="/my-account/" />
        <meta property="article:published_time" content="2019-04-18T11:07:31+00:00" />
        <meta property="article:modified_time" content="2019-04-18T11:07:31+00:00" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="My account - Linh Linh Shop" />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "BreadcrumbList",
                        "@id": "https:\/staging.dndcleaners.ca\/my-account\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "https:\/staging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "https:\/staging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "Product categories Men Women Jewelry Watch Accessories Baby Sport Summer Sale Shop Now Learn More Men's Fashion Shop Now Women's Fashion Shop Now Featured Best Selling Recent Sale Featured Black pants $89 – $99 Lựa chọn các tùy chọn Giảm giá! Modern $25 $19 Thêm vào giỏ hàng Giảm giá! Single Shirt $30 $22 Thêm",
                                    "url": "https:\/staging.dndcleaners.ca\/"
                                },
                                "nextItem": "https:\/staging.dndcleaners.ca\/my-account\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "https:\/staging.dndcleaners.ca\/my-account\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "https:\/staging.dndcleaners.ca\/my-account\/",
                                    "name": "My account",
                                    "url": "https:\/staging.dndcleaners.ca\/my-account\/"
                                },
                                "previousItem": "https:\/staging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "Organization",
                        "@id": "https:\/staging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "https:\/staging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "WebPage",
                        "@id": "https:\/staging.dndcleaners.ca\/my-account\/#webpage",
                        "url": "https:\/staging.dndcleaners.ca\/my-account\/",
                        "name": "My account - Linh Linh Shop",
                        "inLanguage": "en-US",
                        "isPartOf": { "@id": "https:\/staging.dndcleaners.ca\/#website" },
                        "breadcrumb": { "@id": "https:\/staging.dndcleaners.ca\/my-account\/#breadcrumblist" },
                        "datePublished": "2019-04-18T11:07:31+00:00",
                        "dateModified": "2019-04-18T11:07:31+00:00"
                    },
                    {
                        "@type": "WebSite",
                        "@id": "https:\/staging.dndcleaners.ca\/#website",
                        "url": "https:\/staging.dndcleaners.ca\/",
                        "name": "Linh Linh Shop",
                        "description": "Quality - Reputation",
                        "inLanguage": "en-US",
                        "publisher": { "@id": "https:\/staging.dndcleaners.ca\/#organization" }
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->

        
        <link
            rel="stylesheet"
            id="envo-extra-gutenberg-css"
            href="/wp-content/plugins/envo-extra/css/gutenberg.css?ver=1.4.4"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="envo-extra-css"
            href="/wp-content/plugins/envo-extra/css/style.css?ver=1.4.4"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="select2-css"
            href="/wp-content/plugins/woocommerce/assets/css/select2.css?ver=7.1.0"
            type="text/css"
            media="all"
        />
        
        @include('public.header')
    </head>
    <body
        data-rsssl="1"
        id="blog"
        class="page-template-default page page-id-10 theme-envo-storefront gutenberg-on woocommerce-account woocommerce-page woocommerce-no-js elementor-default elementor-kit-1847"
    >
        @include('public.top-content')
        <div class="page-wrap">
            <div class="top-bar-section container-fluid">
                <div class="container">
                    <div class="row">
                        <div id="text-2" class="widget widget_text col-sm-4">
                            <div class="textwidget">
                                <p>
                                    Popular searches: <a href="#">Women </a>// <a href="#">Modern</a> //  <a href="#"
                                        >New</a
                                    >
                                    // <a href="#">Sale</a>
                                </p>
                            </div>
                        </div>
                        <div id="text-3" class="widget widget_text col-sm-4">
                            <div class="textwidget">
                                <p>Limited offer: -20% on all products</p>
                            </div>
                        </div>
                        <div id="text-4" class="widget widget_text col-sm-4">
                            <div class="textwidget">
                                <p>
                                    <i class="fab fa-twitter-square"></i> &nbsp;<i class="fab fa-facebook-square"></i>
                                    &nbsp; <i class="fab fa-youtube-square"></i>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-header container-fluid">
                <div class="container">
                    <div class="heading-row row">
                        <div class="site-heading col-md-4 col-xs-12">
                            <div class="site-branding-logo"></div>
                            <div class="site-branding-text">
                                <p class="site-title"><a href="/" rel="home">Linh Linh Shop</a></p>

                                <p class="site-description">Quality &#8211; Reputation</p>
                            </div>
                            <!-- .site-branding-text -->
                        </div>
                        <div class="search-heading col-md-6 col-xs-12">
                            <div class="header-search-form">
                                <form role="search" method="get" action="https://staging.dndcleaners.ca/">
                                    <input type="hidden" name="post_type" value="product" />
                                    <input
                                        class="header-search-input"
                                        name="s"
                                        type="text"
                                        placeholder="Search products..."
                                    />
                                    <select class="header-search-select" name="product_cat">
                                        <option value="">All Categories</option>
                                        <option value="clothing">Clothing (9)</option>
                                        <option value="mens-clothing">Men&#039;s Clothing (3)</option>
                                        <option value="watches">Watches (3)</option>
                                        <option value="womens-clothing">Women&#039;s Clothing (6)</option>
                                    </select>
                                    <button class="header-search-button" type="submit">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </div>
                            <div class="site-heading-sidebar">
                                <div id="text-5" class="widget widget_text">
                                    <div class="textwidget">
                                        <p>
                                            <i class="fas fa-phone" aria-hidden="true"></i> + 123 654 6548 ||
                                            <i class="far fa-envelope"></i> info@your-mail.com || London Street 569, DH6
                                            SE London &#8211; United Kingdom
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-right col-md-2 hidden-xs">
                            <div class="header-cart">
                                <div class="header-cart-block">
                                    <div class="header-cart-inner">
                                        <a class="cart-contents" href="/cart/" title="View your shopping cart">
                                            <i class="fa fa-shopping-bag"><span class="count">0</span></i>
                                            <div class="amount-cart">&#036;0</div>
                                        </a>
                                        <ul class="site-header-cart menu list-unstyled text-center">
                                            <li>
                                                <div class="widget woocommerce widget_shopping_cart">
                                                    <div class="widget_shopping_cart_content"></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="header-my-account">
                                <div class="header-login">
                                    <a href="/my-account/" title="My Account">
                                        <i class="fa fa-user-circle-o"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main-menu">
                <nav id="site-navigation" class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-header">
                            <span class="navbar-brand brand-absolute visible-xs">Menu</span>
                            <div class="mobile-cart visible-xs">
                                <div class="header-cart">
                                    <div class="header-cart-block">
                                        <div class="header-cart-inner">
                                            <a class="cart-contents" href="/cart/" title="View your shopping cart">
                                                <i class="fa fa-shopping-bag"><span class="count">0</span></i>
                                                <div class="amount-cart">&#036;0</div>
                                            </a>
                                            <ul class="site-header-cart menu list-unstyled text-center">
                                                <li>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mobile-account visible-xs">
                                <div class="header-my-account">
                                    <div class="header-login">
                                        <a href="/my-account/" title="My Account">
                                            <i class="fa fa-user-circle-o"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a href="#" id="main-menu-panel" class="open-panel" data-panel="main-menu-panel">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                        <div id="my-menu" class="menu-container">
                            <ul id="menu-main-menu" class="nav navbar-nav navbar-left">
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2048"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-2048 nav-item"
                                >
                                    <a title="Home" href="/" class="nav-link">Home</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2060"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2060 nav-item"
                                >
                                    <a title="Shop" href="/shop/" class="nav-link">Shop</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2062"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2062 nav-item"
                                >
                                    <a title="Sale" href="/sale/" class="nav-link"
                                        >Sale<span class="menu-description">Hot!</span></a
                                    >
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2046"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2046 nav-item"
                                >
                                    <a title="About Us" href="/about-us/" class="nav-link">About Us</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2061"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2061 nav-item"
                                >
                                    <a title="Blog" href="/blog/" class="nav-link">Blog</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2047"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2047 nav-item"
                                >
                                    <a title="Contact" href="/contact/" class="nav-link">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <div id="site-content" class="container main-container" role="main">
                <div class="page-area">
                    <!-- start content container -->
                    <!-- start content container -->
                    <div class="row">
                        <article class="col-md-9">
                            <div class="post-10 page type-page status-publish hentry">
                                <header class="single-head page-head no-thumbnail">
                                    <h1 class="single-title">My account</h1>
                                    <time class="posted-on published" datetime="2019-04-18"></time>
                                </header>

                                <div class="main-content-page single-content">
                                    <div class="single-entry-summary">
                                        <div class="woocommerce">
                                            <div class="woocommerce-notices-wrapper"></div>

                                            <h2>Login</h2>

                                            <form class="woocommerce-form woocommerce-form-login login" method="post">
                                                <p
                                                    class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide"
                                                >
                                                    <label for="username"
                                                        >Username or email address&nbsp;<span class="required"
                                                            >*</span
                                                        ></label
                                                    >
                                                    <input
                                                        type="text"
                                                        class="woocommerce-Input woocommerce-Input--text input-text"
                                                        name="username"
                                                        id="username"
                                                        autocomplete="username"
                                                        value=""
                                                    />
                                                </p>
                                                <p
                                                    class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide"
                                                >
                                                    <label for="password"
                                                        >Password&nbsp;<span class="required">*</span></label
                                                    >
                                                    <input
                                                        class="woocommerce-Input woocommerce-Input--text input-text"
                                                        type="password"
                                                        name="password"
                                                        id="password"
                                                        autocomplete="current-password"
                                                    />
                                                </p>

                                                <p class="form-row">
                                                    <label
                                                        class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme"
                                                    >
                                                        <input
                                                            class="woocommerce-form__input woocommerce-form__input-checkbox"
                                                            name="rememberme"
                                                            type="checkbox"
                                                            id="rememberme"
                                                            value="forever"
                                                        />
                                                        <span>Remember me</span>
                                                    </label>
                                                    <input
                                                        type="hidden"
                                                        id="woocommerce-login-nonce"
                                                        name="woocommerce-login-nonce"
                                                        value="99c97bf25e"
                                                    /><input
                                                        type="hidden"
                                                        name="_wp_http_referer"
                                                        value="/my-account/"
                                                    />
                                                    <button
                                                        type="submit"
                                                        class="woocommerce-button button woocommerce-form-login__submit wp-element-button"
                                                        name="login"
                                                        value="Log in"
                                                    >
                                                        Log in
                                                    </button>
                                                </p>
                                                <p class="woocommerce-LostPassword lost_password">
                                                    <a href="/my-account/lost-password/">Lost your password?</a>
                                                </p>
                                            </form>
                                        </div>
                                    </div>

                                    <div id="comments" class="comments-template"></div>
                                </div>
                            </div>
                        </article>
                        <aside id="sidebar" class="col-md-3">
                            <div id="woocommerce_widget_cart-1" class="widget woocommerce widget_shopping_cart">
                                <div class="widget-title"><h3>Cart</h3></div>
                                <div class="widget_shopping_cart_content"></div>
                            </div>
                            <div id="block-10" class="widget widget_block">
                                <div class="widget-title"><h3>Search Products</h3></div>
                                <div class="wp-widget-group__inner-blocks">
                                    <form
                                        role="search"
                                        method="get"
                                        action="https://staging.dndcleaners.ca/"
                                        class="wp-block-search__button-outside wp-block-search__text-button wp-block-search"
                                    >
                                        <label
                                            for="wp-block-search__input-2"
                                            class="wp-block-search__label screen-reader-text"
                                            >Search</label
                                        >
                                        <div class="wp-block-search__inside-wrapper">
                                            <input
                                                type="search"
                                                id="wp-block-search__input-2"
                                                class="wp-block-search__input wp-block-search__input"
                                                name="s"
                                                value=""
                                                placeholder="Search products..."
                                                required
                                            /><input type="hidden" name="post_type" value="product" /><button
                                                type="submit"
                                                class="wp-block-search__button wp-element-button"
                                            >
                                                Search
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="woocommerce_products-1" class="widget woocommerce widget_products">
                                <div class="widget-title"><h3>Products</h3></div>
                                <ul class="product_list_widget">
                                    <li>
                                        <a href="/product/single-shirt/">
                                            <img
                                                width="300"
                                                height="300"
                                                src="/wp-content/uploads/2019/40/StockSnap_QXVSHD5FLF-300x300.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt=""
                                                decoding="async"
                                                loading="lazy"
                                                srcset="
                                                    /wp-content/uploads/2019/40/StockSnap_QXVSHD5FLF-300x300.jpg 300w,
                                                    /wp-content/uploads/2019/40/StockSnap_QXVSHD5FLF-100x100.jpg 100w,
                                                    /wp-content/uploads/2019/40/StockSnap_QXVSHD5FLF-150x150.jpg 150w
                                                "
                                                sizes="(max-width: 300px) 100vw, 300px"
                                            />
                                            <span class="product-title">Single Shirt</span>
                                        </a>

                                        <del aria-hidden="true"
                                            ><span class="woocommerce-Price-amount amount"
                                                ><bdi
                                                    ><span class="woocommerce-Price-currencySymbol">&#36;</span>30</bdi
                                                ></span
                                            ></del
                                        >
                                        <ins
                                            ><span class="woocommerce-Price-amount amount"
                                                ><bdi
                                                    ><span class="woocommerce-Price-currencySymbol">&#36;</span>22</bdi
                                                ></span
                                            ></ins
                                        >
                                    </li>
                                    <li>
                                        <a href="/product/polo-t-shirt/">
                                            <img
                                                width="300"
                                                height="300"
                                                src="/wp-content/uploads/2019/40/StockSnap_TE9BDY8EYB-300x300.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt=""
                                                decoding="async"
                                                loading="lazy"
                                                srcset="
                                                    /wp-content/uploads/2019/40/StockSnap_TE9BDY8EYB-300x300.jpg 300w,
                                                    /wp-content/uploads/2019/40/StockSnap_TE9BDY8EYB-100x100.jpg 100w,
                                                    /wp-content/uploads/2019/40/StockSnap_TE9BDY8EYB-150x150.jpg 150w
                                                "
                                                sizes="(max-width: 300px) 100vw, 300px"
                                            />
                                            <span class="product-title">Polo T-shirt</span>
                                        </a>

                                        <span class="woocommerce-Price-amount amount"
                                            ><bdi
                                                ><span class="woocommerce-Price-currencySymbol">&#36;</span>20</bdi
                                            ></span
                                        >
                                        &ndash;
                                        <span class="woocommerce-Price-amount amount"
                                            ><bdi
                                                ><span class="woocommerce-Price-currencySymbol">&#36;</span>25</bdi
                                            ></span
                                        >
                                    </li>
                                    <li>
                                        <a href="/product/hard-top/">
                                            <img
                                                width="300"
                                                height="300"
                                                src="/wp-content/uploads/2019/40/StockSnap_AQ62CU8K64-300x300.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt=""
                                                decoding="async"
                                                loading="lazy"
                                                srcset="
                                                    /wp-content/uploads/2019/40/StockSnap_AQ62CU8K64-300x300.jpg 300w,
                                                    /wp-content/uploads/2019/40/StockSnap_AQ62CU8K64-100x100.jpg 100w,
                                                    /wp-content/uploads/2019/40/StockSnap_AQ62CU8K64-150x150.jpg 150w
                                                "
                                                sizes="(max-width: 300px) 100vw, 300px"
                                            />
                                            <span class="product-title">Hard top</span>
                                        </a>

                                        <span class="woocommerce-Price-amount amount"
                                            ><bdi
                                                ><span class="woocommerce-Price-currencySymbol">&#36;</span>30</bdi
                                            ></span
                                        >
                                    </li>
                                    <li>
                                        <a href="/product/black-pants/">
                                            <img
                                                width="300"
                                                height="300"
                                                src="/wp-content/uploads/2019/40/StockSnap_Y6SDOYW0KA-300x300.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt=""
                                                decoding="async"
                                                loading="lazy"
                                                srcset="
                                                    /wp-content/uploads/2019/40/StockSnap_Y6SDOYW0KA-300x300.jpg 300w,
                                                    /wp-content/uploads/2019/40/StockSnap_Y6SDOYW0KA-100x100.jpg 100w,
                                                    /wp-content/uploads/2019/40/StockSnap_Y6SDOYW0KA-150x150.jpg 150w
                                                "
                                                sizes="(max-width: 300px) 100vw, 300px"
                                            />
                                            <span class="product-title">Black pants</span>
                                        </a>

                                        <span class="woocommerce-Price-amount amount"
                                            ><bdi
                                                ><span class="woocommerce-Price-currencySymbol">&#36;</span>89</bdi
                                            ></span
                                        >
                                        &ndash;
                                        <span class="woocommerce-Price-amount amount"
                                            ><bdi
                                                ><span class="woocommerce-Price-currencySymbol">&#36;</span>99</bdi
                                            ></span
                                        >
                                    </li>
                                    <li>
                                        <a href="/product/evening-trousers/">
                                            <img
                                                width="300"
                                                height="300"
                                                src="/wp-content/uploads/2019/40/StockSnap_XPI5DN9KGA-300x300.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt=""
                                                decoding="async"
                                                loading="lazy"
                                                srcset="
                                                    /wp-content/uploads/2019/40/StockSnap_XPI5DN9KGA-300x300.jpg 300w,
                                                    /wp-content/uploads/2019/40/StockSnap_XPI5DN9KGA-100x100.jpg 100w,
                                                    /wp-content/uploads/2019/40/StockSnap_XPI5DN9KGA-150x150.jpg 150w
                                                "
                                                sizes="(max-width: 300px) 100vw, 300px"
                                            />
                                            <span class="product-title">Evening trousers</span>
                                        </a>

                                        <del aria-hidden="true"
                                            ><span class="woocommerce-Price-amount amount"
                                                ><bdi
                                                    ><span class="woocommerce-Price-currencySymbol">&#36;</span>55</bdi
                                                ></span
                                            ></del
                                        >
                                        <ins
                                            ><span class="woocommerce-Price-amount amount"
                                                ><bdi
                                                    ><span class="woocommerce-Price-currencySymbol">&#36;</span>29</bdi
                                                ></span
                                            ></ins
                                        >
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <!-- end content container -->
                    <!-- end content container -->
                </div>
                <!-- end main-container -->
            </div>
            <!-- end page-area -->

            @include('public.footer')
        </div>
        <!-- end page-wrap -->
        
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/selectWoo/selectWoo.full.min.js?ver=1.0.9-wc.7.1.0"
            id="selectWoo-js"
        ></script>
       @include('public.script')
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced (DONOTCACHEPAGE constant is defined) 

Served from: staging.dndcleaners.ca @ 2022-12-14 01:22:41 by W3 Total Cache
--></html>
