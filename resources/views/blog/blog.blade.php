<!DOCTYPE html>
<html dir="ltr" lang="en-US" prefix="og: https://ogp.me/ns#">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="profile" href="https://gmpg.org/xfn/11" />
        <title>Blog - Linh Linh Shop</title>

        <!-- All in One SEO 4.2.7.1 - aioseo.com -->
        <meta name="description" content="Quality - Reputation" />
        <meta name="robots" content="noindex, nofollow, max-image-preview:large" />
        <link rel="canonical" href="/blog/" />
        <meta name="generator" content="All in One SEO (AIOSEO) 4.2.7.1 " />
        <meta property="og:locale" content="en_US" />
        <meta property="og:site_name" content="Linh Linh Shop - Quality - Reputation" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Blog - Linh Linh Shop" />
        <meta property="og:url" content="/blog/" />
        <meta property="article:published_time" content="2019-04-18T11:25:54+00:00" />
        <meta property="article:modified_time" content="2019-04-18T11:25:54+00:00" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Blog - Linh Linh Shop" />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "BreadcrumbList",
                        "@id": "https:\/staging.dndcleaners.ca\/blog\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "https:\/staging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "https:\/staging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "Quality - Reputation",
                                    "url": "https:\/staging.dndcleaners.ca\/"
                                },
                                "nextItem": "https:\/staging.dndcleaners.ca\/blog\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "https:\/staging.dndcleaners.ca\/blog\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "https:\/staging.dndcleaners.ca\/blog\/",
                                    "name": "Blog",
                                    "description": "Quality - Reputation",
                                    "url": "https:\/staging.dndcleaners.ca\/blog\/"
                                },
                                "previousItem": "https:\/staging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "CollectionPage",
                        "@id": "https:\/staging.dndcleaners.ca\/blog\/#collectionpage",
                        "url": "https:\/staging.dndcleaners.ca\/blog\/",
                        "name": "Blog - Linh Linh Shop",
                        "description": "Quality - Reputation",
                        "inLanguage": "en-US",
                        "isPartOf": { "@id": "https:\/staging.dndcleaners.ca\/#website" },
                        "breadcrumb": { "@id": "https:\/staging.dndcleaners.ca\/blog\/#breadcrumblist" }
                    },
                    {
                        "@type": "Organization",
                        "@id": "https:\/staging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "https:\/staging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "WebSite",
                        "@id": "https:\/staging.dndcleaners.ca\/#website",
                        "url": "https:\/staging.dndcleaners.ca\/",
                        "name": "Linh Linh Shop",
                        "description": "Quality - Reputation",
                        "inLanguage": "en-US",
                        "publisher": { "@id": "https:\/staging.dndcleaners.ca\/#organization" }
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->
            @include('public.header')
    </head>
    <body
        data-rsssl="1"
        id="blog"
        class="blog theme-envo-storefront woocommerce-no-js elementor-default elementor-kit-1847"
    >
       @include('public.top-content')
        <div class="page-wrap">
            @include('public.top-bar-section')

            <div class="main-menu">
                 <nav id="site-navigation" class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-header">
                            <span class="navbar-brand brand-absolute visible-xs">Menu</span>
                            <div class="mobile-cart visible-xs">
                                <div class="header-cart">
                                    <div class="header-cart-block">
                                        <div class="header-cart-inner">
                                            <a class="cart-contents" href="/cart/" title="View your shopping cart">
                                                <i class="fa fa-shopping-bag"><span class="count">0</span></i>
                                                <div class="amount-cart">&#036;0</div>
                                            </a>
                                            <ul class="site-header-cart menu list-unstyled text-center">
                                                <li>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mobile-account visible-xs">
                                <div class="header-my-account">
                                    <div class="header-login">
                                        <a href="/my-account/" title="My Account">
                                            <i class="fa fa-user-circle-o"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a href="#" id="main-menu-panel" class="open-panel" data-panel="main-menu-panel">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                        <div id="my-menu" class="menu-container">
                            <ul id="menu-main-menu" class="nav navbar-nav navbar-left">
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2048"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-2048 nav-item"
                                >
                                    <a title="Home" href="/" class="nav-link">Home</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2060"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2060 nav-item"
                                >
                                    <a title="Shop" href="/shop/" class="nav-link" >Shop</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2062"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2062 nav-item"
                                >
                                    <a title="Sale" href="/sale/" class="nav-link"
                                        >Sale<span class="menu-description">Hot!</span></a
                                    >
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2046"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2046 nav-item"
                                >
                                    <a title="About Us" href="/about-us/" class="nav-link">About Us</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2061"
                                    class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item current_page_item active menu-item-2061 nav-item"
                                >
                                    <a title="Blog" href="/blog/" class="nav-link" aria-current="page">Blog</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2047"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2047 nav-item"
                                >
                                    <a title="Contact" href="/contact/" class="nav-link">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <div id="site-content" class="container main-container" role="main">
                <div class="page-area">
                    <!-- start content container -->
                    <div class="row">
                        <div class="col-md-9">
                            <div id="all_blog"></div>
                           
                        </div>

                        <aside id="sidebar" class="col-md-3">
                            <div id="woocommerce_widget_cart-1" class="widget woocommerce widget_shopping_cart">
                                <div class="widget-title"><h3>Cart</h3></div>
                                <div class="widget_shopping_cart_content"></div>
                            </div>
                            <div id="block-10" class="widget widget_block">
                                <div class="widget-title"><h3>Search Products</h3></div>
                                <div class="wp-widget-group__inner-blocks">
                                    <form
                                        role="search"
                                        method="get"
                                        action="https://staging.dndcleaners.ca/"
                                        class="wp-block-search__button-outside wp-block-search__text-button wp-block-search"
                                    >
                                        <label
                                            for="wp-block-search__input-2"
                                            class="wp-block-search__label screen-reader-text"
                                            >Search</label
                                        >
                                        <div class="wp-block-search__inside-wrapper">
                                            <input
                                                type="search"
                                                id="wp-block-search__input-2"
                                                class="wp-block-search__input wp-block-search__input"
                                                name="s"
                                                value=""
                                                placeholder="Search products..."
                                                required
                                            /><input type="hidden" name="post_type" value="product" /><button
                                                type="submit"
                                                class="wp-block-search__button wp-element-button"
                                            >
                                                Search
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="woocommerce_products-1" class="widget woocommerce widget_products">
                                <div class="widget-title"><h3>Products</h3></div>
                               <div id="side_bar"></div>
                            </div>
                        </aside>
                    </div>
                    <!-- end content container -->
                </div>
                <!-- end main-container -->
            </div>
            <!-- end page-area -->

           @include('public.footer')
        </div>
        <!-- end page-wrap -->
        <script
            type="text/javascript"
            src="/js/vuejs/vue.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/vuejs/moment.min.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/blog.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/sidebar.js"
        ></script>
        @include('public.script')
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-12-13 07:05:51 by W3 Total Cache
--></html>
