
<!DOCTYPE html>
<html dir="ltr" lang="en-US" prefix="og: https://ogp.me/ns#">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="profile" href="https://gmpg.org/xfn/11" />
        <title>Home - Linh Linh Shop</title>

        <!-- All in One SEO 4.2.7.1 - aioseo.com -->
        <meta
            name="description"
            content="Product categories Men Women Jewelry Watch Accessories Baby Sport Summer Sale Shop Now Learn More Men&#039;s Fashion Shop Now Women&#039;s Fashion Shop Now Featured Best Selling Recent Sale Featured Black pants $89 – $99 Lựa chọn các tùy chọn Giảm giá! Modern $25 $19 Thêm vào giỏ hàng Giảm giá! Single Shirt $30 $22 Thêm"
        />
        <meta name="robots" content="noindex, nofollow, max-image-preview:large" />
        <link rel="canonical" href="/" />
        <meta name="generator" content="All in One SEO (AIOSEO) 4.2.7.1 " />
        <meta property="og:locale" content="en_US" />
        <meta property="og:site_name" content="Linh Linh Shop - Quality - Reputation" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Home - Linh Linh Shop" />
        <meta
            property="og:description"
            content="Product categories Men Women Jewelry Watch Accessories Baby Sport Summer Sale Shop Now Learn More Men&#039;s Fashion Shop Now Women&#039;s Fashion Shop Now Featured Best Selling Recent Sale Featured Black pants $89 – $99 Lựa chọn các tùy chọn Giảm giá! Modern $25 $19 Thêm vào giỏ hàng Giảm giá! Single Shirt $30 $22 Thêm"
        />
        <meta property="og:url" content="/" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Home - Linh Linh Shop" />
        <meta
            name="twitter:description"
            content="Product categories Men Women Jewelry Watch Accessories Baby Sport Summer Sale Shop Now Learn More Men&#039;s Fashion Shop Now Women&#039;s Fashion Shop Now Featured Best Selling Recent Sale Featured Black pants $89 – $99 Lựa chọn các tùy chọn Giảm giá! Modern $25 $19 Thêm vào giỏ hàng Giảm giá! Single Shirt $30 $22 Thêm"
        />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "BreadcrumbList",
                        "@id": "https:\/staging.dndcleaners.ca\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "https:\/staging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "https:\/staging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "Product categories Men Women Jewelry Watch Accessories Baby Sport Summer Sale Shop Now Learn More Men's Fashion Shop Now Women's Fashion Shop Now Featured Best Selling Recent Sale Featured Black pants $89 – $99 Lựa chọn các tùy chọn Giảm giá! Modern $25 $19 Thêm vào giỏ hàng Giảm giá! Single Shirt $30 $22 Thêm",
                                    "url": "https:\/staging.dndcleaners.ca\/"
                                },
                                "nextItem": "https:\/staging.dndcleaners.ca\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "https:\/staging.dndcleaners.ca\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "https:\/staging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "Product categories Men Women Jewelry Watch Accessories Baby Sport Summer Sale Shop Now Learn More Men's Fashion Shop Now Women's Fashion Shop Now Featured Best Selling Recent Sale Featured Black pants $89 – $99 Lựa chọn các tùy chọn Giảm giá! Modern $25 $19 Thêm vào giỏ hàng Giảm giá! Single Shirt $30 $22 Thêm",
                                    "url": "https:\/staging.dndcleaners.ca\/"
                                },
                                "previousItem": "https:\/staging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "Organization",
                        "@id": "https:\/staging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "https:\/staging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "WebPage",
                        "@id": "https:\/staging.dndcleaners.ca\/#webpage",
                        "url": "https:\/staging.dndcleaners.ca\/",
                        "name": "Home - Linh Linh Shop",
                        "description": "Product categories Men Women Jewelry Watch Accessories Baby Sport Summer Sale Shop Now Learn More Men's Fashion Shop Now Women's Fashion Shop Now Featured Best Selling Recent Sale Featured Black pants $89 – $99 Lựa chọn các tùy chọn Giảm giá! Modern $25 $19 Thêm vào giỏ hàng Giảm giá! Single Shirt $30 $22 Thêm",
                        "inLanguage": "en-US",
                        "isPartOf": { "@id": "https:\/staging.dndcleaners.ca\/#website" },
                        "breadcrumb": { "@id": "https:\/staging.dndcleaners.ca\/#breadcrumblist" },
                        "datePublished": "2019-04-18T11:09:40+00:00",
                        "dateModified": "2022-11-15T13:34:07+00:00"
                    },
                    {
                        "@type": "WebSite",
                        "@id": "https:\/staging.dndcleaners.ca\/#website",
                        "url": "https:\/staging.dndcleaners.ca\/",
                        "name": "Linh Linh Shop",
                        "description": "Quality - Reputation",
                        "inLanguage": "en-US",
                        "publisher": { "@id": "https:\/staging.dndcleaners.ca\/#organization" },
                        "potentialAction": {
                            "@type": "SearchAction",
                            "target": {
                                "@type": "EntryPoint",
                                "urlTemplate": "https:\/staging.dndcleaners.ca\/?s={search_term_string}"
                            },
                            "query-input": "required name=search_term_string"
                        }
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->

       
       
        <link
            rel="stylesheet"
            id="elementor-icons-css"
            href="/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.16.0"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-frontend-css"
            href="/wp-content/plugins/elementor/assets/css/frontend-lite.min.css?ver=3.8.1"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-post-1847-css"
            href="/wp-content/uploads/elementor/css/post-1847.css?ver=1668492633"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="etww-frontend-css"
            href="/wp-content/plugins/envo-elementor-for-woocommerce/assets/css/etww-frontend.min.css?ver=d6eb82c324cab2f36e345863fad95090"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-pro-css"
            href="/wp-content/plugins/elementor-pro/assets/css/frontend-lite.min.css?ver=3.8.0"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-global-css"
            href="/wp-content/uploads/elementor/css/global.css?ver=1668492633"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-post-40-css"
            href="/wp-content/uploads/elementor/css/post-40.css?ver=1668519252"
            type="text/css"
            media="all"
        />
        
        <link
            rel="stylesheet"
            id="google-fonts-1-css"
            href="https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=swap&#038;ver=6.1"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-icons-shared-0-css"
            href="/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-icons-fa-solid-css"
            href="/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.15.3"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-icons-fa-regular-css"
            href="/wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min.css?ver=5.15.3"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-icons-fa-brands-css"
            href="/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.15.3"
            type="text/css"
            media="all"
        />
       @include('public.header')
    </head>
    <body
        data-rsssl="1"
        id="blog"
        class="home page-template page-template-template-parts page-template-template-page-builders page-template-template-partstemplate-page-builders-php page page-id-40 theme-envo-storefront woocommerce-no-js elementor-default elementor-kit-1847 elementor-page elementor-page-40"
    >
        @include('public.top-content')
        <div class="page-wrap">
           @include('public.top-bar-section')

            <div class="main-menu">
                 <nav id="site-navigation" class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-header">
                            <span class="navbar-brand brand-absolute visible-xs">Menu</span>
                            <div class="mobile-cart visible-xs">
                                <div class="header-cart">
                                    <div class="header-cart-block">
                                        <div class="header-cart-inner">
                                            <a class="cart-contents" href="/cart/" title="View your shopping cart">
                                                <i class="fa fa-shopping-bag"><span class="count">0</span></i>
                                                <div class="amount-cart">&#036;0</div>
                                            </a>
                                            <ul class="site-header-cart menu list-unstyled text-center">
                                                <li>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mobile-account visible-xs">
                                <div class="header-my-account">
                                    <div class="header-login">
                                        <a href="/my-account/" title="My Account">
                                            <i class="fa fa-user-circle-o"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a href="#" id="main-menu-panel" class="open-panel" data-panel="main-menu-panel">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                        <div id="my-menu" class="menu-container">
                            <ul id="menu-main-menu" class="nav navbar-nav navbar-left">
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2048"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item current_page_item active menu-item-2048 nav-item"
                                >
                                    <a title="Home" href="/" class="nav-link" aria-current="page">Home</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2060"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2060 nav-item"
                                >
                                    <a title="Shop" href="/shop/" class="nav-link" >Shop</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2062"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2062 nav-item"
                                >
                                    <a title="Sale" href="/sale/" class="nav-link"
                                        >Sale<span class="menu-description">Hot!</span></a
                                    >
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2046"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2046 nav-item"
                                >
                                    <a title="About Us" href="/about-us/" class="nav-link">About Us</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2061"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2061 nav-item"
                                >
                                    <a title="Blog" href="/blog/" class="nav-link">Blog</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2047"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2047 nav-item"
                                >
                                    <a title="Contact" href="/contact/" class="nav-link">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

            <div id="site-content" class="page-builders" role="main">
                <div class="page-builders-content-area">
                    <!-- start content container -->
                    <div class="post-40 page type-page status-publish hentry">
                        <div data-elementor-type="wp-post" data-elementor-id="40" class="elementor elementor-40">
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-243e9c01 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="243e9c01"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-background-overlay"></div>
                                <div class="elementor-container elementor-column-gap-default">
                                    <div
                                        class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-4a4d34fc elementor-hidden-mobile"
                                        data-id="4a4d34fc"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-60e1e85 elementor-align-left elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-invisible elementor-widget elementor-widget-icon-list"
                                                data-id="60e1e85"
                                                data-element_type="widget"
                                                data-settings='{"_animation":"fadeInLeft"}'
                                                data-widget_type="icon-list.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <link
                                                        rel="stylesheet"
                                                        href="/wp-content/plugins/elementor/assets/css/widget-icon-list.min.css"
                                                    />
                                                    <ul class="elementor-icon-list-items">
                                                        <li class="elementor-icon-list-item">
                                                            <a href="#">
                                                                <span class="elementor-icon-list-icon">
                                                                    <i aria-hidden="true" class="fas fa-bars"></i>
                                                                </span>
                                                                <span class="elementor-icon-list-text"
                                                                    >Product categories</span
                                                                >
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-073a23a elementor-align-left elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-invisible elementor-widget elementor-widget-icon-list"
                                                data-id="073a23a"
                                                data-element_type="widget"
                                                data-settings='{"_animation":"fadeInUp","_animation_delay":200}'
                                                data-widget_type="icon-list.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <ul class="elementor-icon-list-items">
                                                        <li class="elementor-icon-list-item">
                                                            <a href="#">
                                                                <span class="elementor-icon-list-icon">
                                                                    <i aria-hidden="true" class="fas fa-snowman"></i>
                                                                </span>
                                                                <span class="elementor-icon-list-text">Men</span>
                                                            </a>
                                                        </li>
                                                        <li class="elementor-icon-list-item">
                                                            <a href="#">
                                                                <span class="elementor-icon-list-icon">
                                                                    <i aria-hidden="true" class="fas fa-user-nurse"></i>
                                                                </span>
                                                                <span class="elementor-icon-list-text">Women</span>
                                                            </a>
                                                        </li>
                                                        <li class="elementor-icon-list-item">
                                                            <a href="#">
                                                                <span class="elementor-icon-list-icon">
                                                                    <i aria-hidden="true" class="far fa-gem"></i>
                                                                </span>
                                                                <span class="elementor-icon-list-text">Jewelry</span>
                                                            </a>
                                                        </li>
                                                        <li class="elementor-icon-list-item">
                                                            <a href="#">
                                                                <span class="elementor-icon-list-icon">
                                                                    <i aria-hidden="true" class="far fa-clock"></i>
                                                                </span>
                                                                <span class="elementor-icon-list-text">Watch</span>
                                                            </a>
                                                        </li>
                                                        <li class="elementor-icon-list-item">
                                                            <a href="#">
                                                                <span class="elementor-icon-list-icon">
                                                                    <i aria-hidden="true" class="far fa-lightbulb"></i>
                                                                </span>
                                                                <span class="elementor-icon-list-text"
                                                                    >Accessories</span
                                                                >
                                                            </a>
                                                        </li>
                                                        <li class="elementor-icon-list-item">
                                                            <a href="#">
                                                                <span class="elementor-icon-list-icon">
                                                                    <i aria-hidden="true" class="fas fa-baby"></i>
                                                                </span>
                                                                <span class="elementor-icon-list-text">Baby</span>
                                                            </a>
                                                        </li>
                                                        <li class="elementor-icon-list-item">
                                                            <a href="#">
                                                                <span class="elementor-icon-list-icon">
                                                                    <i aria-hidden="true" class="fas fa-bicycle"></i>
                                                                </span>
                                                                <span class="elementor-icon-list-text">Sport</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-b971da8"
                                        data-id="b971da8"
                                        data-element_type="column"
                                        data-settings='{"background_background":"classic"}'
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-5ece5cb elementor-widget elementor-widget-heading"
                                                data-id="5ece5cb"
                                                data-element_type="widget"
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-heading-title {
                                                            padding: 0;
                                                            margin: 0;
                                                            line-height: 1;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title[class*="elementor-size-"]
                                                            > a {
                                                            color: inherit;
                                                            font-size: inherit;
                                                            line-height: inherit;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-small {
                                                            font-size: 15px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-medium {
                                                            font-size: 19px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-large {
                                                            font-size: 29px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-xl {
                                                            font-size: 39px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-xxl {
                                                            font-size: 59px;
                                                        }
                                                    </style>
                                                    <h2 class="elementor-heading-title elementor-size-default">
                                                        <b>Summer</b> Sale
                                                    </h2>
                                                </div>
                                            </div>
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-8d79494 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="8d79494"
                                                data-element_type="section"
                                            >
                                                <div class="elementor-container elementor-column-gap-no">
                                                    <div
                                                        class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-6f1460b"
                                                        data-id="6f1460b"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-65aedd0 elementor-align-left elementor-invisible elementor-widget elementor-widget-button"
                                                                data-id="65aedd0"
                                                                data-element_type="widget"
                                                                data-settings='{"_animation":"zoomIn","_animation_delay":100}'
                                                                data-widget_type="button.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-button-wrapper">
                                                                        <a
                                                                            href="#shop"
                                                                            class="elementor-button-link elementor-button elementor-size-sm"
                                                                            role="button"
                                                                        >
                                                                            <span
                                                                                class="elementor-button-content-wrapper"
                                                                            >
                                                                                <span class="elementor-button-text"
                                                                                    >Shop Now</span
                                                                                >
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-57fc70b"
                                                        data-id="57fc70b"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-85f7d56 elementor-align-left elementor-invisible elementor-widget elementor-widget-button"
                                                                data-id="85f7d56"
                                                                data-element_type="widget"
                                                                data-settings='{"_animation":"zoomIn","_animation_delay":300}'
                                                                data-widget_type="button.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-button-wrapper">
                                                                        <a
                                                                            href="#about"
                                                                            class="elementor-button-link elementor-button elementor-size-sm"
                                                                            role="button"
                                                                        >
                                                                            <span
                                                                                class="elementor-button-content-wrapper"
                                                                            >
                                                                                <span class="elementor-button-text"
                                                                                    >Learn More</span
                                                                                >
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-90c3131 elementor-hidden-tablet elementor-hidden-mobile"
                                        data-id="90c3131"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-dfe8636 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-invisible"
                                                data-id="dfe8636"
                                                data-element_type="section"
                                                data-settings='{"animation":"fadeInRight"}'
                                            >
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div
                                                        class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-ee68b33"
                                                        data-id="ee68b33"
                                                        data-element_type="column"
                                                        data-settings='{"animation":"none"}'
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-8b00e87 elementor-widget elementor-widget-image"
                                                                data-id="8b00e87"
                                                                data-element_type="widget"
                                                                data-widget_type="image.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <style>
                                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                                        .elementor-widget-image {
                                                                            text-align: center;
                                                                        }
                                                                        .elementor-widget-image a {
                                                                            display: inline-block;
                                                                        }
                                                                        .elementor-widget-image a img[src$=".svg"] {
                                                                            width: 48px;
                                                                        }
                                                                        .elementor-widget-image img {
                                                                            vertical-align: middle;
                                                                            display: inline-block;
                                                                        }
                                                                    </style>
                                                                    <img
                                                                        decoding="async"
                                                                        src="https://cdn.stocksnap.io/img-thumbs/960w/man-sitting_XPI5DN9KGA.jpg"
                                                                        title=""
                                                                        alt=""
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-83f76e5 elementor-absolute elementor-widget elementor-widget-heading"
                                                                data-id="83f76e5"
                                                                data-element_type="widget"
                                                                data-settings='{"_position":"absolute"}'
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h2
                                                                        class="elementor-heading-title elementor-size-default"
                                                                    >
                                                                        Men's Fashion
                                                                    </h2>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-f19e96f elementor-absolute elementor-widget elementor-widget-button"
                                                                data-id="f19e96f"
                                                                data-element_type="widget"
                                                                data-settings='{"_position":"absolute"}'
                                                                data-widget_type="button.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-button-wrapper">
                                                                        <a
                                                                            href="#"
                                                                            class="elementor-button-link elementor-button elementor-size-sm"
                                                                            role="button"
                                                                        >
                                                                            <span
                                                                                class="elementor-button-content-wrapper"
                                                                            >
                                                                                <span class="elementor-button-text"
                                                                                    >Shop Now</span
                                                                                >
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-4a2aec4 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-invisible"
                                                data-id="4a2aec4"
                                                data-element_type="section"
                                                data-settings='{"animation":"fadeInRight","animation_delay":100}'
                                            >
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div
                                                        class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-0dd11fd"
                                                        data-id="0dd11fd"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-73c0b09 elementor-widget elementor-widget-image"
                                                                data-id="73c0b09"
                                                                data-element_type="widget"
                                                                data-widget_type="image.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <img
                                                                        decoding="async"
                                                                        src="https://cdn.stocksnap.io/img-thumbs/960w/female-model_KQECHSBAOC.jpg"
                                                                        title=""
                                                                        alt=""
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-e94fbd7 elementor-absolute elementor-widget elementor-widget-heading"
                                                                data-id="e94fbd7"
                                                                data-element_type="widget"
                                                                data-settings='{"_position":"absolute"}'
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h2
                                                                        class="elementor-heading-title elementor-size-default"
                                                                    >
                                                                        Women's <br />
                                                                        Fashion
                                                                    </h2>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-fabec24 elementor-absolute elementor-widget elementor-widget-button"
                                                                data-id="fabec24"
                                                                data-element_type="widget"
                                                                data-settings='{"_position":"absolute"}'
                                                                data-widget_type="button.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-button-wrapper">
                                                                        <a
                                                                            href="#"
                                                                            class="elementor-button-link elementor-button elementor-size-sm"
                                                                            role="button"
                                                                        >
                                                                            <span
                                                                                class="elementor-button-content-wrapper"
                                                                            >
                                                                                <span class="elementor-button-text"
                                                                                    >Shop Now</span
                                                                                >
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-a875da2 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="a875da2"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-no">
                                    <div
                                        class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1bf8213 elementor-invisible"
                                        data-id="1bf8213"
                                        data-element_type="column"
                                        data-settings='{"animation":"fadeIn"}'
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-a1a6a22 text-center elementor-tabs-view-horizontal elementor-widget elementor-widget-tabs"
                                                data-id="a1a6a22"
                                                data-element_type="widget"
                                                data-widget_type="tabs.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-widget-tabs.elementor-tabs-view-vertical
                                                            .elementor-tabs-wrapper {
                                                            width: 25%;
                                                            -ms-flex-negative: 0;
                                                            flex-shrink: 0;
                                                        }
                                                        .elementor-widget-tabs.elementor-tabs-view-vertical
                                                            .elementor-tab-desktop-title.elementor-active {
                                                            border-right-style: none;
                                                        }
                                                        .elementor-widget-tabs.elementor-tabs-view-vertical
                                                            .elementor-tab-desktop-title.elementor-active:after,
                                                        .elementor-widget-tabs.elementor-tabs-view-vertical
                                                            .elementor-tab-desktop-title.elementor-active:before {
                                                            height: 999em;
                                                            width: 0;
                                                            right: 0;
                                                            border-right-style: solid;
                                                        }
                                                        .elementor-widget-tabs.elementor-tabs-view-vertical
                                                            .elementor-tab-desktop-title.elementor-active:before {
                                                            top: 0;
                                                            -webkit-transform: translateY(-100%);
                                                            -ms-transform: translateY(-100%);
                                                            transform: translateY(-100%);
                                                        }
                                                        .elementor-widget-tabs.elementor-tabs-view-vertical
                                                            .elementor-tab-desktop-title.elementor-active:after {
                                                            top: 100%;
                                                        }
                                                        .elementor-widget-tabs.elementor-tabs-view-horizontal
                                                            .elementor-tab-desktop-title {
                                                            display: table-cell;
                                                        }
                                                        .elementor-widget-tabs.elementor-tabs-view-horizontal
                                                            .elementor-tab-desktop-title.elementor-active {
                                                            border-bottom-style: none;
                                                        }
                                                        .elementor-widget-tabs.elementor-tabs-view-horizontal
                                                            .elementor-tab-desktop-title.elementor-active:after,
                                                        .elementor-widget-tabs.elementor-tabs-view-horizontal
                                                            .elementor-tab-desktop-title.elementor-active:before {
                                                            bottom: 0;
                                                            height: 0;
                                                            width: 999em;
                                                            border-bottom-style: solid;
                                                        }
                                                        .elementor-widget-tabs.elementor-tabs-view-horizontal
                                                            .elementor-tab-desktop-title.elementor-active:before {
                                                            right: 100%;
                                                        }
                                                        .elementor-widget-tabs.elementor-tabs-view-horizontal
                                                            .elementor-tab-desktop-title.elementor-active:after {
                                                            left: 100%;
                                                        }
                                                        .elementor-widget-tabs .elementor-tab-content,
                                                        .elementor-widget-tabs .elementor-tab-title,
                                                        .elementor-widget-tabs .elementor-tab-title:after,
                                                        .elementor-widget-tabs .elementor-tab-title:before,
                                                        .elementor-widget-tabs .elementor-tabs-content-wrapper {
                                                            border: 1px #d4d4d4;
                                                        }
                                                        .elementor-widget-tabs .elementor-tabs {
                                                            text-align: left;
                                                        }
                                                        .elementor-widget-tabs .elementor-tabs-wrapper {
                                                            overflow: hidden;
                                                        }
                                                        .elementor-widget-tabs .elementor-tab-title {
                                                            cursor: pointer;
                                                            outline: var(--focus-outline, none);
                                                        }
                                                        .elementor-widget-tabs .elementor-tab-desktop-title {
                                                            position: relative;
                                                            padding: 20px 25px;
                                                            font-weight: 700;
                                                            line-height: 1;
                                                            border: solid transparent;
                                                        }
                                                        .elementor-widget-tabs
                                                            .elementor-tab-desktop-title.elementor-active {
                                                            border-color: #d4d4d4;
                                                        }
                                                        .elementor-widget-tabs
                                                            .elementor-tab-desktop-title.elementor-active:after,
                                                        .elementor-widget-tabs
                                                            .elementor-tab-desktop-title.elementor-active:before {
                                                            display: block;
                                                            content: "";
                                                            position: absolute;
                                                        }
                                                        .elementor-widget-tabs
                                                            .elementor-tab-desktop-title:focus-visible {
                                                            border: 1px solid #000;
                                                        }
                                                        .elementor-widget-tabs .elementor-tab-mobile-title {
                                                            padding: 10px;
                                                            cursor: pointer;
                                                        }
                                                        .elementor-widget-tabs .elementor-tab-content {
                                                            padding: 20px;
                                                            display: none;
                                                        }
                                                        @media (max-width: 767px) {
                                                            .elementor-tabs .elementor-tab-content,
                                                            .elementor-tabs .elementor-tab-title {
                                                                border-style: solid solid none;
                                                            }
                                                            .elementor-tabs .elementor-tabs-wrapper {
                                                                display: none;
                                                            }
                                                            .elementor-tabs .elementor-tabs-content-wrapper {
                                                                border-bottom-style: solid;
                                                            }
                                                            .elementor-tabs .elementor-tab-content {
                                                                padding: 10px;
                                                            }
                                                        }
                                                        @media (min-width: 768px) {
                                                            .elementor-widget-tabs.elementor-tabs-view-vertical
                                                                .elementor-tabs {
                                                                display: -webkit-box;
                                                                display: -ms-flexbox;
                                                                display: flex;
                                                            }
                                                            .elementor-widget-tabs.elementor-tabs-view-vertical
                                                                .elementor-tabs-wrapper {
                                                                -webkit-box-orient: vertical;
                                                                -webkit-box-direction: normal;
                                                                -ms-flex-direction: column;
                                                                flex-direction: column;
                                                            }
                                                            .elementor-widget-tabs.elementor-tabs-view-vertical
                                                                .elementor-tabs-content-wrapper {
                                                                -webkit-box-flex: 1;
                                                                -ms-flex-positive: 1;
                                                                flex-grow: 1;
                                                                border-style: solid solid solid none;
                                                            }
                                                            .elementor-widget-tabs.elementor-tabs-view-horizontal
                                                                .elementor-tab-content {
                                                                border-style: none solid solid;
                                                            }
                                                            .elementor-widget-tabs.elementor-tabs-alignment-center
                                                                .elementor-tabs-wrapper,
                                                            .elementor-widget-tabs.elementor-tabs-alignment-end
                                                                .elementor-tabs-wrapper,
                                                            .elementor-widget-tabs.elementor-tabs-alignment-stretch
                                                                .elementor-tabs-wrapper {
                                                                display: -webkit-box;
                                                                display: -ms-flexbox;
                                                                display: flex;
                                                            }
                                                            .elementor-widget-tabs.elementor-tabs-alignment-center
                                                                .elementor-tabs-wrapper {
                                                                -webkit-box-pack: center;
                                                                -ms-flex-pack: center;
                                                                justify-content: center;
                                                            }
                                                            .elementor-widget-tabs.elementor-tabs-alignment-end
                                                                .elementor-tabs-wrapper {
                                                                -webkit-box-pack: end;
                                                                -ms-flex-pack: end;
                                                                justify-content: flex-end;
                                                            }
                                                            .elementor-widget-tabs.elementor-tabs-alignment-stretch.elementor-tabs-view-horizontal
                                                                .elementor-tab-title {
                                                                width: 100%;
                                                            }
                                                            .elementor-widget-tabs.elementor-tabs-alignment-stretch.elementor-tabs-view-vertical
                                                                .elementor-tab-title {
                                                                height: 100%;
                                                            }
                                                            .elementor-tabs .elementor-tab-mobile-title {
                                                                display: none;
                                                            }
                                                        }
                                                    </style>
                                                    <div class="elementor-tabs">
                                                        <div class="elementor-tabs-wrapper" role="tablist">
                                                            <div
                                                                id="elementor-tab-title-1691"
                                                                class="elementor-tab-title elementor-tab-desktop-title"
                                                                aria-selected="true"
                                                                data-tab="1"
                                                                role="tab"
                                                                tabindex="0"
                                                                aria-controls="elementor-tab-content-1691"
                                                                aria-expanded="false"
                                                            >
                                                                Featured
                                                            </div>
                                                            <div
                                                                id="elementor-tab-title-1692"
                                                                class="elementor-tab-title elementor-tab-desktop-title"
                                                                aria-selected="false"
                                                                data-tab="2"
                                                                role="tab"
                                                                tabindex="-1"
                                                                aria-controls="elementor-tab-content-1692"
                                                                aria-expanded="false"
                                                            >
                                                                Best Selling
                                                            </div>
                                                            <div
                                                                id="elementor-tab-title-1693"
                                                                class="elementor-tab-title elementor-tab-desktop-title"
                                                                aria-selected="false"
                                                                data-tab="3"
                                                                role="tab"
                                                                tabindex="-1"
                                                                aria-controls="elementor-tab-content-1693"
                                                                aria-expanded="false"
                                                            >
                                                                Recent
                                                            </div>
                                                            <div
                                                                id="elementor-tab-title-1694"
                                                                class="elementor-tab-title elementor-tab-desktop-title"
                                                                aria-selected="false"
                                                                data-tab="4"
                                                                role="tab"
                                                                tabindex="-1"
                                                                aria-controls="elementor-tab-content-1694"
                                                                aria-expanded="false"
                                                            >
                                                                Sale
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-tabs-content-wrapper"
                                                            role="tablist"
                                                            aria-orientation="vertical"
                                                        >
                                                            <div
                                                                class="elementor-tab-title elementor-tab-mobile-title"
                                                                aria-selected="true"
                                                                data-tab="1"
                                                                role="tab"
                                                                tabindex="0"
                                                                aria-controls="elementor-tab-content-1691"
                                                                aria-expanded="false"
                                                            >
                                                                Featured
                                                            </div>
                                                            <div
                                                                id="elementor-tab-content-1691"
                                                                class="elementor-tab-content elementor-clearfix"
                                                                data-tab="1"
                                                                role="tabpanel"
                                                                aria-labelledby="elementor-tab-title-1691"
                                                                tabindex="0"
                                                                hidden="false"
                                                            >
                                                                
                                                                <div class="woocommerce columns-4">
                                                                   <div id="featured"></div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-tab-title elementor-tab-mobile-title"
                                                                aria-selected="false"
                                                                data-tab="2"
                                                                role="tab"
                                                                tabindex="-1"
                                                                aria-controls="elementor-tab-content-1692"
                                                                aria-expanded="false"
                                                            >
                                                                Best Selling
                                                            </div>
                                                            <div
                                                                id="elementor-tab-content-1692"
                                                                class="elementor-tab-content elementor-clearfix"
                                                                data-tab="2"
                                                                role="tabpanel"
                                                                aria-labelledby="elementor-tab-title-1692"
                                                                tabindex="0"
                                                                hidden="hidden"
                                                            >
                                                                <div class="woocommerce columns-4">
                                                                    <div id="best_selling"></div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-tab-title elementor-tab-mobile-title"
                                                                aria-selected="false"
                                                                data-tab="3"
                                                                role="tab"
                                                                tabindex="-1"
                                                                aria-controls="elementor-tab-content-1693"
                                                                aria-expanded="false"
                                                            >
                                                                Recent
                                                            </div>
                                                            <div
                                                                id="elementor-tab-content-1693"
                                                                class="elementor-tab-content elementor-clearfix"
                                                                data-tab="3"
                                                                role="tabpanel"
                                                                aria-labelledby="elementor-tab-title-1693"
                                                                tabindex="0"
                                                                hidden="hidden"
                                                            >
                                                                <div class="woocommerce columns-4">
                                                                    <div id="recent"></div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-tab-title elementor-tab-mobile-title"
                                                                aria-selected="false"
                                                                data-tab="4"
                                                                role="tab"
                                                                tabindex="-1"
                                                                aria-controls="elementor-tab-content-1694"
                                                                aria-expanded="false"
                                                            >
                                                                Sale
                                                            </div>
                                                            <div
                                                                id="elementor-tab-content-1694"
                                                                class="elementor-tab-content elementor-clearfix"
                                                                data-tab="4"
                                                                role="tabpanel"
                                                                aria-labelledby="elementor-tab-title-1694"
                                                                tabindex="0"
                                                                hidden="hidden"
                                                            >
                                                                <div class="woocommerce columns-4 quick-sale">
                                                                    <div id="sale"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-9dbfb33 elementor-section-height-min-height elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-items-middle"
                                data-id="9dbfb33"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-background-overlay"></div>
                                <div class="elementor-container elementor-column-gap-default">
                                    <div
                                        class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-5232f7d"
                                        data-id="5232f7d"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-67b3435 elementor-invisible elementor-widget elementor-widget-heading"
                                                data-id="67b3435"
                                                data-element_type="widget"
                                                data-settings='{"_animation":"fadeInDown"}'
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <h5 class="elementor-heading-title elementor-size-default">Sale</h5>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-f348318 elementor-invisible elementor-widget elementor-widget-heading"
                                                data-id="f348318"
                                                data-element_type="widget"
                                                data-settings='{"_animation":"fadeIn"}'
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <h2 class="elementor-heading-title elementor-size-default">
                                                        Sale up to <b>50%</b>
                                                    </h2>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-8a6d663 elementor-invisible elementor-widget elementor-widget-text-editor"
                                                data-id="8a6d663"
                                                data-element_type="widget"
                                                data-settings='{"_animation":"fadeIn","_animation_delay":100}'
                                                data-widget_type="text-editor.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-widget-text-editor.elementor-drop-cap-view-stacked
                                                            .elementor-drop-cap {
                                                            background-color: #818a91;
                                                            color: #fff;
                                                        }
                                                        .elementor-widget-text-editor.elementor-drop-cap-view-framed
                                                            .elementor-drop-cap {
                                                            color: #818a91;
                                                            border: 3px solid;
                                                            background-color: transparent;
                                                        }
                                                        .elementor-widget-text-editor:not(
                                                                .elementor-drop-cap-view-default
                                                            )
                                                            .elementor-drop-cap {
                                                            margin-top: 8px;
                                                        }
                                                        .elementor-widget-text-editor:not(
                                                                .elementor-drop-cap-view-default
                                                            )
                                                            .elementor-drop-cap-letter {
                                                            width: 1em;
                                                            height: 1em;
                                                        }
                                                        .elementor-widget-text-editor .elementor-drop-cap {
                                                            float: left;
                                                            text-align: center;
                                                            line-height: 1;
                                                            font-size: 50px;
                                                        }
                                                        .elementor-widget-text-editor .elementor-drop-cap-letter {
                                                            display: inline-block;
                                                        }
                                                    </style>
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit
                                                        tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                                                    </p>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-32bbc69 elementor-invisible elementor-widget elementor-widget-button"
                                                data-id="32bbc69"
                                                data-element_type="widget"
                                                data-settings='{"_animation":"fadeInUp","_animation_delay":400}'
                                                data-widget_type="button.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-button-wrapper">
                                                        <a
                                                            href="/sale"
                                                            class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                            role="button"
                                                        >
                                                            <span class="elementor-button-content-wrapper">
                                                                <span class="elementor-button-text">Read More</span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-0114cea elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="0114cea"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-no">
                                    <div
                                        class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3a8ee19 elementor-invisible"
                                        data-id="3a8ee19"
                                        data-element_type="column"
                                        data-settings='{"animation":"fadeIn"}'
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-91373e5 elementor-tabs-view-vertical text-center elementor-widget elementor-widget-tabs"
                                                data-id="91373e5"
                                                data-element_type="widget"
                                                data-widget_type="tabs.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-tabs">
                                                        <div class="elementor-tabs-wrapper" role="tablist">
                                                            <div
                                                                id="elementor-tab-title-1521"
                                                                class="elementor-tab-title elementor-tab-desktop-title"
                                                                aria-selected="true"
                                                                data-tab="1"
                                                                role="tab"
                                                                tabindex="0"
                                                                aria-controls="elementor-tab-content-1521"
                                                                aria-expanded="false"
                                                            >
                                                                Women's Clothing
                                                            </div>
                                                            <div
                                                                id="elementor-tab-title-1522"
                                                                class="elementor-tab-title elementor-tab-desktop-title"
                                                                aria-selected="false"
                                                                data-tab="2"
                                                                role="tab"
                                                                tabindex="-1"
                                                                aria-controls="elementor-tab-content-1522"
                                                                aria-expanded="false"
                                                            >
                                                                Men's Clothing
                                                            </div>
                                                            <div
                                                                id="elementor-tab-title-1523"
                                                                class="elementor-tab-title elementor-tab-desktop-title"
                                                                aria-selected="false"
                                                                data-tab="3"
                                                                role="tab"
                                                                tabindex="-1"
                                                                aria-controls="elementor-tab-content-1523"
                                                                aria-expanded="false"
                                                            >
                                                                Watches
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-tabs-content-wrapper"
                                                            role="tablist"
                                                            aria-orientation="vertical"
                                                        >
                                                            <div
                                                                class="elementor-tab-title elementor-tab-mobile-title"
                                                                aria-selected="true"
                                                                data-tab="1"
                                                                role="tab"
                                                                tabindex="0"
                                                                aria-controls="elementor-tab-content-1521"
                                                                aria-expanded="false"
                                                            >
                                                                Women's Clothing
                                                            </div>
                                                            <div
                                                                id="elementor-tab-content-1521"
                                                                class="elementor-tab-content elementor-clearfix"
                                                                data-tab="1"
                                                                role="tabpanel"
                                                                aria-labelledby="elementor-tab-title-1521"
                                                                tabindex="0"
                                                                hidden="false"
                                                            >
                                                                <div class="woocommerce columns-4">
                                                                    <div id="women_product"></div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-tab-title elementor-tab-mobile-title"
                                                                aria-selected="false"
                                                                data-tab="2"
                                                                role="tab"
                                                                tabindex="-1"
                                                                aria-controls="elementor-tab-content-1522"
                                                                aria-expanded="false"
                                                            >
                                                                Men's Clothing
                                                            </div>
                                                            <div
                                                                id="elementor-tab-content-1522"
                                                                class="elementor-tab-content elementor-clearfix"
                                                                data-tab="2"
                                                                role="tabpanel"
                                                                aria-labelledby="elementor-tab-title-1522"
                                                                tabindex="0"
                                                                hidden="hidden"
                                                            >
                                                                <div class="woocommerce columns-4">
                                                                    <div id="men_product"></div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-tab-title elementor-tab-mobile-title"
                                                                aria-selected="false"
                                                                data-tab="3"
                                                                role="tab"
                                                                tabindex="-1"
                                                                aria-controls="elementor-tab-content-1523"
                                                                aria-expanded="false"
                                                            >
                                                                Watches
                                                            </div>
                                                            <div
                                                                id="elementor-tab-content-1523"
                                                                class="elementor-tab-content elementor-clearfix"
                                                                data-tab="3"
                                                                role="tabpanel"
                                                                aria-labelledby="elementor-tab-title-1523"
                                                                tabindex="0"
                                                                hidden="hidden"
                                                            >
                                                                <div class="woocommerce columns-4">
                                                                    <div id="watch_product"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-164d0947 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="164d0947"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-no">
                                    <div
                                        class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-5cee834e"
                                        data-id="5cee834e"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-2f5bc6d5 elementor-widget elementor-widget-heading"
                                                data-id="2f5bc6d5"
                                                data-element_type="widget"
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <h4 class="elementor-heading-title elementor-size-default">
                                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-3444210f"
                                        data-id="3444210f"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-4c1049ea elementor-align-right elementor-mobile-align-center elementor-widget elementor-widget-button"
                                                data-id="4c1049ea"
                                                data-element_type="widget"
                                                data-widget_type="button.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-button-wrapper">
                                                        <a
                                                            href="#"
                                                            class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
                                                            role="button"
                                                        >
                                                            <span class="elementor-button-content-wrapper">
                                                                <span class="elementor-button-text">Start Free</span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-5d53b1ae elementor-section-height-min-height elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-items-middle"
                                data-id="5d53b1ae"
                                data-element_type="section"
                                data-settings='{"background_background":"classic","shape_divider_top":"arrow"}'
                            >
                                <div class="elementor-shape elementor-shape-top" data-negative="false">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewbox="0 0 700 10"
                                        preserveaspectratio="none"
                                    >
                                        <path class="elementor-shape-fill" d="M350,10L340,0h20L350,10z"></path>
                                    </svg>
                                </div>
                                <div class="elementor-container elementor-column-gap-no">
                                    <div
                                        class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-38629215"
                                        data-id="38629215"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-41bfa526 elementor-widget elementor-widget-text-editor"
                                                data-id="41bfa526"
                                                data-element_type="widget"
                                                data-widget_type="text-editor.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <p>Etiam quis quam. Nullam <strong>lectus</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-839471a elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="839471a"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-no">
                                    <div
                                        class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-0b031c9"
                                        data-id="0b031c9"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-9808c42 elementor-widget elementor-widget-counter"
                                                data-id="9808c42"
                                                data-element_type="widget"
                                                data-widget_type="counter.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-counter .elementor-counter-number-wrapper {
                                                            display: -webkit-box;
                                                            display: -ms-flexbox;
                                                            display: flex;
                                                            font-size: 69px;
                                                            font-weight: 600;
                                                            line-height: 1;
                                                        }
                                                        .elementor-counter .elementor-counter-number-prefix,
                                                        .elementor-counter .elementor-counter-number-suffix {
                                                            -webkit-box-flex: 1;
                                                            -ms-flex-positive: 1;
                                                            flex-grow: 1;
                                                            white-space: pre-wrap;
                                                        }
                                                        .elementor-counter .elementor-counter-number-prefix {
                                                            text-align: right;
                                                        }
                                                        .elementor-counter .elementor-counter-number-suffix {
                                                            text-align: left;
                                                        }
                                                        .elementor-counter .elementor-counter-title {
                                                            text-align: center;
                                                            font-size: 19px;
                                                            font-weight: 400;
                                                            line-height: 2.5;
                                                        }
                                                    </style>
                                                    <div class="elementor-counter">
                                                        <div class="elementor-counter-number-wrapper">
                                                            <span class="elementor-counter-number-prefix"></span>
                                                            <span
                                                                class="elementor-counter-number"
                                                                data-duration="2000"
                                                                data-to-value="85"
                                                                data-from-value="1"
                                                                data-delimiter=","
                                                                >1</span
                                                            >
                                                            <span class="elementor-counter-number-suffix">%</span>
                                                        </div>
                                                        <div class="elementor-counter-title">Support</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-84192a2"
                                        data-id="84192a2"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-44ea1ab elementor-widget elementor-widget-counter"
                                                data-id="44ea1ab"
                                                data-element_type="widget"
                                                data-widget_type="counter.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-counter">
                                                        <div class="elementor-counter-number-wrapper">
                                                            <span class="elementor-counter-number-prefix"></span>
                                                            <span
                                                                class="elementor-counter-number"
                                                                data-duration="2000"
                                                                data-to-value="90"
                                                                data-from-value="1"
                                                                data-delimiter=","
                                                                >1</span
                                                            >
                                                            <span class="elementor-counter-number-suffix">%</span>
                                                        </div>
                                                        <div class="elementor-counter-title">Design</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-2c72efd"
                                        data-id="2c72efd"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-2f74528 elementor-widget elementor-widget-counter"
                                                data-id="2f74528"
                                                data-element_type="widget"
                                                data-widget_type="counter.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-counter">
                                                        <div class="elementor-counter-number-wrapper">
                                                            <span class="elementor-counter-number-prefix"></span>
                                                            <span
                                                                class="elementor-counter-number"
                                                                data-duration="2000"
                                                                data-to-value="99"
                                                                data-from-value="1"
                                                                data-delimiter=","
                                                                >1</span
                                                            >
                                                            <span class="elementor-counter-number-suffix">%</span>
                                                        </div>
                                                        <div class="elementor-counter-title">Happiness</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-c8faad9"
                                        data-id="c8faad9"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-979ea0e elementor-widget elementor-widget-counter"
                                                data-id="979ea0e"
                                                data-element_type="widget"
                                                data-widget_type="counter.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-counter">
                                                        <div class="elementor-counter-number-wrapper">
                                                            <span class="elementor-counter-number-prefix"></span>
                                                            <span
                                                                class="elementor-counter-number"
                                                                data-duration="2000"
                                                                data-to-value="100"
                                                                data-from-value="1"
                                                                data-delimiter=","
                                                                >1</span
                                                            >
                                                            <span class="elementor-counter-number-suffix">%</span>
                                                        </div>
                                                        <div class="elementor-counter-title">Delivery</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-718243b elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                                data-id="718243b"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-no">
                                    <div
                                        class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-7f2af7c"
                                        data-id="7f2af7c"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-2594c5ce elementor-widget elementor-widget-google_maps"
                                                data-id="2594c5ce"
                                                data-element_type="widget"
                                                data-widget_type="google_maps.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-widget-google_maps .elementor-widget-container {
                                                            overflow: hidden;
                                                        }
                                                        .elementor-widget-google_maps iframe {
                                                            height: 300px;
                                                        }
                                                    </style>
                                                    <div class="elementor-custom-embed">
                                                        <iframe
                                                            frameborder="0"
                                                            scrolling="no"
                                                            marginheight="0"
                                                            marginwidth="0"
                                                            src="https://maps.google.com/maps?q=London%20Eye%2C%20London%2C%20United%20Kingdom&#038;t=m&#038;z=18&#038;output=embed&#038;iwloc=near"
                                                            title="London Eye, London, United Kingdom"
                                                            aria-label="London Eye, London, United Kingdom"
                                                        ></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-62d9883a elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="62d9883a"
                                                data-element_type="section"
                                            >
                                                <div class="elementor-container elementor-column-gap-no">
                                                    <div
                                                        class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-2683b2f6"
                                                        data-id="2683b2f6"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-26c64805 elementor-widget elementor-widget-heading"
                                                                data-id="26c64805"
                                                                data-element_type="widget"
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h6
                                                                        class="elementor-heading-title elementor-size-default"
                                                                    >
                                                                        Location
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-7cae3aae elementor-widget elementor-widget-text-editor"
                                                                data-id="7cae3aae"
                                                                data-element_type="widget"
                                                                data-widget_type="text-editor.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <p>709 Honey Creek Dr. <br />London 10028</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-6650d6fe"
                                                        data-id="6650d6fe"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-7b1ed6da elementor-widget elementor-widget-heading"
                                                                data-id="7b1ed6da"
                                                                data-element_type="widget"
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h6
                                                                        class="elementor-heading-title elementor-size-default"
                                                                    >
                                                                        Our hours
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-a5cd3cc elementor-widget elementor-widget-text-editor"
                                                                data-id="a5cd3cc"
                                                                data-element_type="widget"
                                                                data-widget_type="text-editor.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <p>
                                                                        10:00 AM &#8211; 22.00 PM<br />Monday &#8211;
                                                                        Sunday
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-3da57a80"
                                                        data-id="3da57a80"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-71740446 elementor-widget elementor-widget-heading"
                                                                data-id="71740446"
                                                                data-element_type="widget"
                                                                data-widget_type="heading.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <h6
                                                                        class="elementor-heading-title elementor-size-default"
                                                                    >
                                                                        Contact us
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="elementor-element elementor-element-280a2976 elementor-widget elementor-widget-text-editor"
                                                                data-id="280a2976"
                                                                data-element_type="widget"
                                                                data-widget_type="text-editor.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <p>
                                                                        Phone: 1 555 755 60 20<br />Email:
                                                                        contact@contact.com
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <div
                                                class="elementor-element elementor-element-5f27c713 elementor-shape-rounded elementor-grid-0 e-grid-align-center elementor-widget elementor-widget-social-icons"
                                                data-id="5f27c713"
                                                data-element_type="widget"
                                                data-widget_type="social-icons.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-widget-social-icons.elementor-grid-0
                                                            .elementor-widget-container,
                                                        .elementor-widget-social-icons.elementor-grid-mobile-0
                                                            .elementor-widget-container,
                                                        .elementor-widget-social-icons.elementor-grid-tablet-0
                                                            .elementor-widget-container {
                                                            line-height: 1;
                                                            font-size: 0;
                                                        }
                                                        .elementor-widget-social-icons:not(.elementor-grid-0):not(
                                                                .elementor-grid-tablet-0
                                                            ):not(.elementor-grid-mobile-0)
                                                            .elementor-grid {
                                                            display: inline-grid;
                                                        }
                                                        .elementor-widget-social-icons .elementor-grid {
                                                            grid-column-gap: var(--grid-column-gap, 5px);
                                                            grid-row-gap: var(--grid-row-gap, 5px);
                                                            grid-template-columns: var(--grid-template-columns);
                                                            -webkit-box-pack: var(--justify-content, center);
                                                            -ms-flex-pack: var(--justify-content, center);
                                                            justify-content: var(--justify-content, center);
                                                            justify-items: var(--justify-content, center);
                                                        }
                                                        .elementor-icon.elementor-social-icon {
                                                            font-size: var(--icon-size, 25px);
                                                            line-height: var(--icon-size, 25px);
                                                            width: calc(
                                                                var(--icon-size, 25px) +
                                                                    (2 * var(--icon-padding, 0.5em))
                                                            );
                                                            height: calc(
                                                                var(--icon-size, 25px) +
                                                                    (2 * var(--icon-padding, 0.5em))
                                                            );
                                                        }
                                                        .elementor-social-icon {
                                                            --e-social-icon-icon-color: #fff;
                                                            display: -webkit-inline-box;
                                                            display: -ms-inline-flexbox;
                                                            display: inline-flex;
                                                            background-color: #818a91;
                                                            -webkit-box-align: center;
                                                            -ms-flex-align: center;
                                                            align-items: center;
                                                            -webkit-box-pack: center;
                                                            -ms-flex-pack: center;
                                                            justify-content: center;
                                                            text-align: center;
                                                            cursor: pointer;
                                                        }
                                                        .elementor-social-icon i {
                                                            color: var(--e-social-icon-icon-color);
                                                        }
                                                        .elementor-social-icon svg {
                                                            fill: var(--e-social-icon-icon-color);
                                                        }
                                                        .elementor-social-icon:last-child {
                                                            margin: 0;
                                                        }
                                                        .elementor-social-icon:hover {
                                                            opacity: 0.9;
                                                            color: #fff;
                                                        }
                                                        .elementor-social-icon-android {
                                                            background-color: #a4c639;
                                                        }
                                                        .elementor-social-icon-apple {
                                                            background-color: #999;
                                                        }
                                                        .elementor-social-icon-behance {
                                                            background-color: #1769ff;
                                                        }
                                                        .elementor-social-icon-bitbucket {
                                                            background-color: #205081;
                                                        }
                                                        .elementor-social-icon-codepen {
                                                            background-color: #000;
                                                        }
                                                        .elementor-social-icon-delicious {
                                                            background-color: #39f;
                                                        }
                                                        .elementor-social-icon-deviantart {
                                                            background-color: #05cc47;
                                                        }
                                                        .elementor-social-icon-digg {
                                                            background-color: #005be2;
                                                        }
                                                        .elementor-social-icon-dribbble {
                                                            background-color: #ea4c89;
                                                        }
                                                        .elementor-social-icon-elementor {
                                                            background-color: #d30c5c;
                                                        }
                                                        .elementor-social-icon-envelope {
                                                            background-color: #ea4335;
                                                        }
                                                        .elementor-social-icon-facebook,
                                                        .elementor-social-icon-facebook-f {
                                                            background-color: #3b5998;
                                                        }
                                                        .elementor-social-icon-flickr {
                                                            background-color: #0063dc;
                                                        }
                                                        .elementor-social-icon-foursquare {
                                                            background-color: #2d5be3;
                                                        }
                                                        .elementor-social-icon-free-code-camp,
                                                        .elementor-social-icon-freecodecamp {
                                                            background-color: #006400;
                                                        }
                                                        .elementor-social-icon-github {
                                                            background-color: #333;
                                                        }
                                                        .elementor-social-icon-gitlab {
                                                            background-color: #e24329;
                                                        }
                                                        .elementor-social-icon-globe {
                                                            background-color: #818a91;
                                                        }
                                                        .elementor-social-icon-google-plus,
                                                        .elementor-social-icon-google-plus-g {
                                                            background-color: #dd4b39;
                                                        }
                                                        .elementor-social-icon-houzz {
                                                            background-color: #7ac142;
                                                        }
                                                        .elementor-social-icon-instagram {
                                                            background-color: #262626;
                                                        }
                                                        .elementor-social-icon-jsfiddle {
                                                            background-color: #487aa2;
                                                        }
                                                        .elementor-social-icon-link {
                                                            background-color: #818a91;
                                                        }
                                                        .elementor-social-icon-linkedin,
                                                        .elementor-social-icon-linkedin-in {
                                                            background-color: #0077b5;
                                                        }
                                                        .elementor-social-icon-medium {
                                                            background-color: #00ab6b;
                                                        }
                                                        .elementor-social-icon-meetup {
                                                            background-color: #ec1c40;
                                                        }
                                                        .elementor-social-icon-mixcloud {
                                                            background-color: #273a4b;
                                                        }
                                                        .elementor-social-icon-odnoklassniki {
                                                            background-color: #f4731c;
                                                        }
                                                        .elementor-social-icon-pinterest {
                                                            background-color: #bd081c;
                                                        }
                                                        .elementor-social-icon-product-hunt {
                                                            background-color: #da552f;
                                                        }
                                                        .elementor-social-icon-reddit {
                                                            background-color: #ff4500;
                                                        }
                                                        .elementor-social-icon-rss {
                                                            background-color: #f26522;
                                                        }
                                                        .elementor-social-icon-shopping-cart {
                                                            background-color: #4caf50;
                                                        }
                                                        .elementor-social-icon-skype {
                                                            background-color: #00aff0;
                                                        }
                                                        .elementor-social-icon-slideshare {
                                                            background-color: #0077b5;
                                                        }
                                                        .elementor-social-icon-snapchat {
                                                            background-color: #fffc00;
                                                        }
                                                        .elementor-social-icon-soundcloud {
                                                            background-color: #f80;
                                                        }
                                                        .elementor-social-icon-spotify {
                                                            background-color: #2ebd59;
                                                        }
                                                        .elementor-social-icon-stack-overflow {
                                                            background-color: #fe7a15;
                                                        }
                                                        .elementor-social-icon-steam {
                                                            background-color: #00adee;
                                                        }
                                                        .elementor-social-icon-stumbleupon {
                                                            background-color: #eb4924;
                                                        }
                                                        .elementor-social-icon-telegram {
                                                            background-color: #2ca5e0;
                                                        }
                                                        .elementor-social-icon-thumb-tack {
                                                            background-color: #1aa1d8;
                                                        }
                                                        .elementor-social-icon-tripadvisor {
                                                            background-color: #589442;
                                                        }
                                                        .elementor-social-icon-tumblr {
                                                            background-color: #35465c;
                                                        }
                                                        .elementor-social-icon-twitch {
                                                            background-color: #6441a5;
                                                        }
                                                        .elementor-social-icon-twitter {
                                                            background-color: #1da1f2;
                                                        }
                                                        .elementor-social-icon-viber {
                                                            background-color: #665cac;
                                                        }
                                                        .elementor-social-icon-vimeo {
                                                            background-color: #1ab7ea;
                                                        }
                                                        .elementor-social-icon-vk {
                                                            background-color: #45668e;
                                                        }
                                                        .elementor-social-icon-weibo {
                                                            background-color: #dd2430;
                                                        }
                                                        .elementor-social-icon-weixin {
                                                            background-color: #31a918;
                                                        }
                                                        .elementor-social-icon-whatsapp {
                                                            background-color: #25d366;
                                                        }
                                                        .elementor-social-icon-wordpress {
                                                            background-color: #21759b;
                                                        }
                                                        .elementor-social-icon-xing {
                                                            background-color: #026466;
                                                        }
                                                        .elementor-social-icon-yelp {
                                                            background-color: #af0606;
                                                        }
                                                        .elementor-social-icon-youtube {
                                                            background-color: #cd201f;
                                                        }
                                                        .elementor-social-icon-500px {
                                                            background-color: #0099e5;
                                                        }
                                                        .elementor-shape-rounded .elementor-icon.elementor-social-icon {
                                                            border-radius: 10%;
                                                        }
                                                        .elementor-shape-circle .elementor-icon.elementor-social-icon {
                                                            border-radius: 50%;
                                                        }
                                                    </style>
                                                    <div class="elementor-social-icons-wrapper elementor-grid">
                                                        <span class="elementor-grid-item">
                                                            <a
                                                                class="elementor-icon elementor-social-icon elementor-social-icon-facebook-f elementor-animation-grow elementor-repeater-item-9c86cde"
                                                                target="_blank"
                                                            >
                                                                <span class="elementor-screen-only">Facebook-f</span>
                                                                <i class="fab fa-facebook-f"></i>
                                                            </a>
                                                        </span>
                                                        <span class="elementor-grid-item">
                                                            <a
                                                                class="elementor-icon elementor-social-icon elementor-social-icon-twitter elementor-animation-grow elementor-repeater-item-7f0e5c7"
                                                                target="_blank"
                                                            >
                                                                <span class="elementor-screen-only">Twitter</span>
                                                                <i class="fab fa-twitter"></i>
                                                            </a>
                                                        </span>
                                                        <span class="elementor-grid-item">
                                                            <a
                                                                class="elementor-icon elementor-social-icon elementor-social-icon-google-plus-g elementor-animation-grow elementor-repeater-item-7e1d56b"
                                                                target="_blank"
                                                            >
                                                                <span class="elementor-screen-only">Google-plus-g</span>
                                                                <i class="fab fa-google-plus-g"></i>
                                                            </a>
                                                        </span>
                                                        <span class="elementor-grid-item">
                                                            <a
                                                                class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-d9a9500"
                                                                target="_blank"
                                                            >
                                                                <span class="elementor-screen-only">Instagram</span>
                                                                <i class="fab fa-instagram"></i>
                                                            </a>
                                                        </span>
                                                        <span class="elementor-grid-item">
                                                            <a
                                                                class="elementor-icon elementor-social-icon elementor-social-icon-dribbble elementor-animation-grow elementor-repeater-item-fd37624"
                                                                target="_blank"
                                                            >
                                                                <span class="elementor-screen-only">Dribbble</span>
                                                                <i class="fab fa-dribbble"></i>
                                                            </a>
                                                        </span>
                                                        <span class="elementor-grid-item">
                                                            <a
                                                                class="elementor-icon elementor-social-icon elementor-social-icon-youtube elementor-animation-grow elementor-repeater-item-75dde21"
                                                                target="_blank"
                                                            >
                                                                <span class="elementor-screen-only">Youtube</span>
                                                                <i class="fab fa-youtube"></i>
                                                            </a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>

                    <!-- end content container -->
                </div>
                <!-- end main-container -->
            </div>
            <!-- end page-area -->

            @include('public.footer')
        </div>
        <!-- end page-wrap -->
<<<<<<< HEAD
         <script
            type="text/javascript"
            src="/js/vuejs/vue.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/vuejs/moment.min.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/home.js"
        ></script>
=======
>>>>>>> 9523bd23581db70c83f8e7d07c2c1c1b40358df8
        @include('public.script')
        <link
            rel="stylesheet"
            id="e-animations-css"
            href="/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.8.1"
            type="text/css"
            media="all"
        />
        
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/lib/jquery-numerator/jquery-numerator.min.js?ver=0.2.1"
            id="jquery-numerator-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.8.0"
            id="elementor-pro-webpack-runtime-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.8.1"
            id="elementor-webpack-runtime-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.8.1"
            id="elementor-frontend-modules-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/vendor/regenerator-runtime.min.js?ver=0.13.9"
            id="regenerator-runtime-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=3.15.0"
            id="wp-polyfill-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/hooks.min.js?ver=4169d3cf8e8d95a3d6d5"
            id="wp-hooks-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/i18n.min.js?ver=9e794f35a71bb98672ae"
            id="wp-i18n-js"
        ></script>
        <script type="text/javascript" id="wp-i18n-js-after">
            wp.i18n.setLocaleData({ "text directionltr": ["ltr"] });
        </script>
        <script type="text/javascript" id="elementor-pro-frontend-js-before">
            var ElementorProFrontendConfig = {
                ajaxurl: "https:\/staging.dndcleaners.ca\/wp-admin\/admin-ajax.php",
                nonce: "1c2f89a654",
                urls: {
                    assets: "https:\/staging.dndcleaners.ca\/wp-content\/plugins\/elementor-pro\/assets\/",
                    rest: "https:\/staging.dndcleaners.ca\/wp-json\/",
                },
                shareButtonsNetworks: {
                    facebook: { title: "Facebook", has_counter: true },
                    twitter: { title: "Twitter" },
                    linkedin: { title: "LinkedIn", has_counter: true },
                    pinterest: { title: "Pinterest", has_counter: true },
                    reddit: { title: "Reddit", has_counter: true },
                    vk: { title: "VK", has_counter: true },
                    odnoklassniki: { title: "OK", has_counter: true },
                    tumblr: { title: "Tumblr" },
                    digg: { title: "Digg" },
                    skype: { title: "Skype" },
                    stumbleupon: { title: "StumbleUpon", has_counter: true },
                    mix: { title: "Mix" },
                    telegram: { title: "Telegram" },
                    pocket: { title: "Pocket", has_counter: true },
                    xing: { title: "XING", has_counter: true },
                    whatsapp: { title: "WhatsApp" },
                    email: { title: "Email" },
                    print: { title: "Print" },
                },
                woocommerce: {
                    menu_cart: {
                        cart_page_url: "https:\/staging.dndcleaners.ca\/cart\/",
                        checkout_page_url: "https:\/staging.dndcleaners.ca\/checkout\/",
                        fragments_nonce: "152b334529",
                    },
                },
                facebook_sdk: { lang: "en_US", app_id: "" },
                lottie: {
                    defaultAnimationUrl:
                        "https:\/staging.dndcleaners.ca\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json",
                },
            };
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.8.0"
            id="elementor-pro-frontend-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2"
            id="elementor-waypoints-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/jquery/ui/core.min.js?ver=1.13.2"
            id="jquery-ui-core-js"
        ></script>
        <script type="text/javascript" id="elementor-frontend-js-before">
            var elementorFrontendConfig = {
                environmentMode: { edit: false, wpPreview: false, isScriptDebug: false },
                i18n: {
                    shareOnFacebook: "Share on Facebook",
                    shareOnTwitter: "Share on Twitter",
                    pinIt: "Pin it",
                    download: "Download",
                    downloadImage: "Download image",
                    fullscreen: "Fullscreen",
                    zoom: "Zoom",
                    share: "Share",
                    playVideo: "Play Video",
                    previous: "Previous",
                    next: "Next",
                    close: "Close",
                },
                is_rtl: false,
                breakpoints: { xs: 0, sm: 480, md: 768, lg: 1025, xl: 1440, xxl: 1600 },
                responsive: {
                    breakpoints: {
                        mobile: { label: "Mobile", value: 767, default_value: 767, direction: "max", is_enabled: true },
                        mobile_extra: {
                            label: "Mobile Extra",
                            value: 880,
                            default_value: 880,
                            direction: "max",
                            is_enabled: false,
                        },
                        tablet: {
                            label: "Tablet",
                            value: 1024,
                            default_value: 1024,
                            direction: "max",
                            is_enabled: true,
                        },
                        tablet_extra: {
                            label: "Tablet Extra",
                            value: 1200,
                            default_value: 1200,
                            direction: "max",
                            is_enabled: false,
                        },
                        laptop: {
                            label: "Laptop",
                            value: 1366,
                            default_value: 1366,
                            direction: "max",
                            is_enabled: false,
                        },
                        widescreen: {
                            label: "Widescreen",
                            value: 2400,
                            default_value: 2400,
                            direction: "min",
                            is_enabled: false,
                        },
                    },
                },
                version: "3.8.1",
                is_static: false,
                experimentalFeatures: {
                    e_dom_optimization: true,
                    e_optimized_assets_loading: true,
                    e_optimized_css_loading: true,
                    a11y_improvements: true,
                    additional_custom_breakpoints: true,
                    e_import_export: true,
                    e_hidden_wordpress_widgets: true,
                    theme_builder_v2: true,
                    "landing-pages": true,
                    "elements-color-picker": true,
                    "favorite-widgets": true,
                    "admin-top-bar": true,
                    "page-transitions": true,
                    notes: true,
                    loop: true,
                    "form-submissions": true,
                    e_scroll_snap: true,
                },
                urls: { assets: "https:\/staging.dndcleaners.ca\/wp-content\/plugins\/elementor\/assets\/" },
                settings: { page: [], editorPreferences: [] },
                kit: {
                    active_breakpoints: ["viewport_mobile", "viewport_tablet"],
                    global_image_lightbox: "yes",
                    lightbox_enable_counter: "yes",
                    lightbox_enable_fullscreen: "yes",
                    lightbox_enable_zoom: "yes",
                    lightbox_enable_share: "yes",
                    lightbox_title_src: "title",
                    lightbox_description_src: "description",
                    woocommerce_notices_elements: [],
                },
                post: { id: 40, title: "Home%20-%20Linh%20Linh%20Shop", excerpt: "", featuredImage: false },
            };
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.8.1"
            id="elementor-frontend-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor-pro/assets/js/elements-handlers.min.js?ver=3.8.0"
            id="pro-elements-handlers-js"
        ></script>
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-11-28 09:43:00 by W3 Total Cache
--></html>
