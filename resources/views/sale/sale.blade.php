<!DOCTYPE html>
<html dir="ltr" lang="en-US" prefix="og: https://ogp.me/ns#">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="profile" href="https://gmpg.org/xfn/11" />
        <title>Sale - Linh Linh Shop</title>

        <!-- All in One SEO 4.2.7.1 - aioseo.com -->
        <meta
            name="description"
            content="Sale 30% Off Sale! Single Shirt $30 $22 Add to cart Sale! Polo T-shirt $20 – $25 Select options Sale! Evening trousers $55 $29 Add to cart Sale! Black trousers $25 $16 Add to cart Sale! Bikini $25 $20 Add to cart Sale! Modern $25 $19 Add to cart"
        />
        <meta name="robots" content="noindex, nofollow, max-image-preview:large" />
        <link rel="canonical" href="/sale/" />
        <meta name="generator" content="All in One SEO (AIOSEO) 4.2.7.1 " />
        <meta property="og:locale" content="en_US" />
        <meta property="og:site_name" content="Linh Linh Shop - Professional Alternations &amp; Repairs" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Sale - Linh Linh Shop" />
        <meta
            property="og:description"
            content="Sale 30% Off Sale! Single Shirt $30 $22 Add to cart Sale! Polo T-shirt $20 – $25 Select options Sale! Evening trousers $55 $29 Add to cart Sale! Black trousers $25 $16 Add to cart Sale! Bikini $25 $20 Add to cart Sale! Modern $25 $19 Add to cart"
        />
        <meta property="og:url" content="/sale/" />
        <meta property="article:published_time" content="2019-04-18T11:20:25+00:00" />
        <meta property="article:modified_time" content="2022-11-15T13:49:04+00:00" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Sale - Linh Linh Shop" />
        <meta
            name="twitter:description"
            content="Sale 30% Off Sale! Single Shirt $30 $22 Add to cart Sale! Polo T-shirt $20 – $25 Select options Sale! Evening trousers $55 $29 Add to cart Sale! Black trousers $25 $16 Add to cart Sale! Bikini $25 $20 Add to cart Sale! Modern $25 $19 Add to cart"
        />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "BreadcrumbList",
                        "@id": "httpsstaging.dndcleaners.ca\/sale\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "Product categories Men Women Jewelry Watch Accessories Baby Sport Summer Sale Shop Now Learn More Men's Fashion Shop Now Women's Fashion Shop Now Featured Best Selling Recent Sale Featured Black pants $89 – $99 Lựa chọn các tùy chọn Giảm giá! Modern $25 $19 Thêm vào giỏ hàng Giảm giá! Single Shirt $30 $22 Thêm",
                                    "url": "httpsstaging.dndcleaners.ca\/"
                                },
                                "nextItem": "httpsstaging.dndcleaners.ca\/sale\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/sale\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/sale\/",
                                    "name": "Sale",
                                    "description": "Sale 30% Off Sale! Single Shirt $30 $22 Add to cart Sale! Polo T-shirt $20 – $25 Select options Sale! Evening trousers $55 $29 Add to cart Sale! Black trousers $25 $16 Add to cart Sale! Bikini $25 $20 Add to cart Sale! Modern $25 $19 Add to cart",
                                    "url": "httpsstaging.dndcleaners.ca\/sale\/"
                                },
                                "previousItem": "httpsstaging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "Organization",
                        "@id": "httpsstaging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "httpsstaging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "WebPage",
                        "@id": "httpsstaging.dndcleaners.ca\/sale\/#webpage",
                        "url": "httpsstaging.dndcleaners.ca\/sale\/",
                        "name": "Sale - Linh Linh Shop",
                        "description": "Sale 30% Off Sale! Single Shirt $30 $22 Add to cart Sale! Polo T-shirt $20 – $25 Select options Sale! Evening trousers $55 $29 Add to cart Sale! Black trousers $25 $16 Add to cart Sale! Bikini $25 $20 Add to cart Sale! Modern $25 $19 Add to cart",
                        "inLanguage": "en-US",
                        "isPartOf": { "@id": "httpsstaging.dndcleaners.ca\/#website" },
                        "breadcrumb": { "@id": "httpsstaging.dndcleaners.ca\/sale\/#breadcrumblist" },
                        "datePublished": "2019-04-18T11:20:25+00:00",
                        "dateModified": "2022-11-15T13:49:04+00:00"
                    },
                    {
                        "@type": "WebSite",
                        "@id": "httpsstaging.dndcleaners.ca\/#website",
                        "url": "httpsstaging.dndcleaners.ca\/",
                        "name": "Linh Linh Shop",
                        "description": "Professional Alternations & Repairs",
                        "inLanguage": "en-US",
                        "publisher": { "@id": "httpsstaging.dndcleaners.ca\/#organization" }
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->

        
        <link
            rel="stylesheet"
            id="envo-extra-css"
            href="/wp-content/plugins/envo-extra/css/style.css?ver=1.4.3"
            type="text/css"
            media="all"
        />
       
        <link
            rel="stylesheet"
            id="elementor-icons-css"
            href="/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.16.0"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-frontend-css"
            href="/wp-content/plugins/elementor/assets/css/frontend-lite.min.css?ver=3.8.1"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-post-1847-css"
            href="/wp-content/uploads/elementor/css/post-1847.css?ver=1668492633"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="etww-frontend-css"
            href="/wp-content/plugins/envo-elementor-for-woocommerce/assets/css/etww-frontend.min.css?ver=d6eb82c324cab2f36e345863fad95090"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-pro-css"
            href="/wp-content/plugins/elementor-pro/assets/css/frontend-lite.min.css?ver=3.8.0"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-global-css"
            href="/wp-content/uploads/elementor/css/global.css?ver=1668492633"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-post-68-css"
            href="/wp-content/uploads/elementor/css/post-68.css?ver=1668520153"
            type="text/css"
            media="all"
        />
        
        <link
            rel="stylesheet"
            id="google-fonts-1-css"
            href="https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=swap&#038;ver=6.1"
            type="text/css"
            media="all"
        />
        @include('public.header')
    </head>
    <body
        data-rsssl="1"
        id="blog"
        class="page-template page-template-template-parts page-template-template-page-builders page-template-template-partstemplate-page-builders-php page page-id-68 theme-envo-storefront woocommerce-no-js elementor-default elementor-kit-1847 elementor-page elementor-page-68"
    >
        @include('public.top-content')
        <div class="page-wrap">
            <div class="top-bar-section container-fluid">
                <div class="container">
                    <div class="row">
                        <div id="text-2" class="widget widget_text col-sm-4">
                            <div class="textwidget">
                                <p>
                                    Popular searches: <a href="#">Women </a>// <a href="#">Modern</a> //  <a href="#"
                                        >New</a
                                    >
                                    // <a href="#">Sale</a>
                                </p>
                            </div>
                        </div>
                        <div id="text-3" class="widget widget_text col-sm-4">
                            <div class="textwidget">
                                <p>Limited offer: -20% on all products</p>
                            </div>
                        </div>
                        <div id="text-4" class="widget widget_text col-sm-4">
                            <div class="textwidget">
                                <p>
                                    <i class="fab fa-twitter-square"></i> &nbsp;<i class="fab fa-facebook-square"></i>
                                    &nbsp; <i class="fab fa-youtube-square"></i>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-header container-fluid">
                <div class="container">
                    <div class="heading-row row">
                        <div class="site-heading col-md-4 col-xs-12">
                            <div class="site-branding-logo"></div>
                            <div class="site-branding-text">
                                <p class="site-title"><a href="/" rel="home">Linh Linh Shop</a></p>

                                <p class="site-description">Professional Alternations &amp; Repairs</p>
                            </div>
                            <!-- .site-branding-text -->
                        </div>
                        <div class="search-heading col-md-6 col-xs-12">
                            <div class="header-search-form">
                                <form role="search" method="get" action="https://staging.dndcleaners.ca/">
                                    <input type="hidden" name="post_type" value="product" />
                                    <input
                                        class="header-search-input"
                                        name="s"
                                        type="text"
                                        placeholder="Search products..."
                                    />
                                    <select class="header-search-select" name="product_cat">
                                        <option value="">All Categories</option>
                                        <option value="clothing">Clothing (9)</option>
                                        <option value="mens-clothing">Men&#039;s Clothing (3)</option>
                                        <option value="watches">Watches (3)</option>
                                        <option value="womens-clothing">Women&#039;s Clothing (6)</option>
                                    </select>
                                    <button class="header-search-button" type="submit">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </div>
                            <div class="site-heading-sidebar">
                                <div id="text-5" class="widget widget_text">
                                    <div class="textwidget">
                                        <p>
                                            <i class="fas fa-phone" aria-hidden="true"></i> + 123 654 6548 ||
                                            <i class="far fa-envelope"></i> info@your-mail.com || London Street 569, DH6
                                            SE London &#8211; United Kingdom
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-right col-md-2 hidden-xs">
                            <div class="header-cart">
                                <div class="header-cart-block">
                                    <div class="header-cart-inner">
                                        <a class="cart-contents" href="/cart/" title="View your shopping cart">
                                            <i class="fa fa-shopping-bag"><span class="count">0</span></i>
                                            <div class="amount-cart">&#036;0</div>
                                        </a>
                                        <ul class="site-header-cart menu list-unstyled text-center">
                                            <li>
                                                <div class="widget woocommerce widget_shopping_cart">
                                                    <div class="widget_shopping_cart_content"></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="header-my-account">
                                <div class="header-login">
                                    <a href="/my-account/" title="My Account">
                                        <i class="fa fa-user-circle-o"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main-menu">
                <nav id="site-navigation" class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-header">
                            <span class="navbar-brand brand-absolute visible-xs">Menu</span>
                            <div class="mobile-cart visible-xs">
                                <div class="header-cart">
                                    <div class="header-cart-block">
                                        <div class="header-cart-inner">
                                            <a class="cart-contents" href="/cart/" title="View your shopping cart">
                                                <i class="fa fa-shopping-bag"><span class="count">0</span></i>
                                                <div class="amount-cart">&#036;0</div>
                                            </a>
                                            <ul class="site-header-cart menu list-unstyled text-center">
                                                <li>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mobile-account visible-xs">
                                <div class="header-my-account">
                                    <div class="header-login">
                                        <a href="/my-account/" title="My Account">
                                            <i class="fa fa-user-circle-o"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a href="#" id="main-menu-panel" class="open-panel" data-panel="main-menu-panel">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                        <div id="my-menu" class="menu-container">
                            <ul id="menu-main-menu" class="nav navbar-nav navbar-left">
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2048"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-2048 nav-item"
                                >
                                    <a title="Home" href="/" class="nav-link">Home</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2060"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2060 nav-item"
                                >
                                    <a title="Shop" href="/shop/" class="nav-link">Shop</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2062"
                                    class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-68 current_page_item active menu-item-2062 nav-item"
                                >
                                    <a title="Sale" href="/sale/" class="nav-link" aria-current="page"
                                        >Sale<span class="menu-description">Hot!</span></a
                                    >
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2046"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2046 nav-item"
                                >
                                    <a title="About Us" href="/about-us/" class="nav-link">About Us</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2061"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2061 nav-item"
                                >
                                    <a title="Blog" href="/blog/" class="nav-link">Blog</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2047"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2047 nav-item"
                                >
                                    <a title="Contact" href="/contact/" class="nav-link">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

            <div id="site-content" class="page-builders" role="main">
                <div class="page-builders-content-area">
                    <!-- start content container -->
                    <div class="post-68 page type-page status-publish hentry">
                        <div data-elementor-type="wp-post" data-elementor-id="68" class="elementor elementor-68">
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-f5c33db elementor-section-height-min-height elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-items-middle"
                                data-id="f5c33db"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-background-overlay"></div>
                                <div class="elementor-container elementor-column-gap-no">
                                    <div
                                        class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6e3fa25c"
                                        data-id="6e3fa25c"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-7fcf1d6c elementor-widget elementor-widget-heading"
                                                data-id="7fcf1d6c"
                                                data-element_type="widget"
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-heading-title {
                                                            padding: 0;
                                                            margin: 0;
                                                            line-height: 1;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title[class*="elementor-size-"]
                                                            > a {
                                                            color: inherit;
                                                            font-size: inherit;
                                                            line-height: inherit;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-small {
                                                            font-size: 15px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-medium {
                                                            font-size: 19px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-large {
                                                            font-size: 29px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-xl {
                                                            font-size: 39px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-xxl {
                                                            font-size: 59px;
                                                        }
                                                    </style>
                                                    <h1 class="elementor-heading-title elementor-size-default">Sale</h1>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-7abc37af elementor-widget elementor-widget-text-editor"
                                                data-id="7abc37af"
                                                data-element_type="widget"
                                                data-widget_type="text-editor.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-widget-text-editor.elementor-drop-cap-view-stacked
                                                            .elementor-drop-cap {
                                                            background-color: #818a91;
                                                            color: #fff;
                                                        }
                                                        .elementor-widget-text-editor.elementor-drop-cap-view-framed
                                                            .elementor-drop-cap {
                                                            color: #818a91;
                                                            border: 3px solid;
                                                            background-color: transparent;
                                                        }
                                                        .elementor-widget-text-editor:not(
                                                                .elementor-drop-cap-view-default
                                                            )
                                                            .elementor-drop-cap {
                                                            margin-top: 8px;
                                                        }
                                                        .elementor-widget-text-editor:not(
                                                                .elementor-drop-cap-view-default
                                                            )
                                                            .elementor-drop-cap-letter {
                                                            width: 1em;
                                                            height: 1em;
                                                        }
                                                        .elementor-widget-text-editor .elementor-drop-cap {
                                                            float: left;
                                                            text-align: center;
                                                            line-height: 1;
                                                            font-size: 50px;
                                                        }
                                                        .elementor-widget-text-editor .elementor-drop-cap-letter {
                                                            display: inline-block;
                                                        }
                                                    </style>
                                                    <p>30% Off</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-65c9551 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="65c9551"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-default">
                                    <div
                                        class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6b36de2"
                                        data-id="6b36de2"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-8f0a6b0 elementor-widget elementor-widget-shortcode"
                                                data-id="8f0a6b0"
                                                data-element_type="widget"
                                                data-widget_type="shortcode.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-shortcode">
                                                        <div class="woocommerce columns-3 quick-sale">
                                                            <!-- Product Sale -->
                                                            <div id="product_sale"></div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>

                    <!-- end content container -->
                </div>
                <!-- end main-container -->
            </div>
            <!-- end page-area -->

            @include('public.footer')
        </div>
        <!-- end page-wrap -->
         <script
            type="text/javascript"
            src="/js/vuejs/vue.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/vuejs/moment.min.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/sale.js"
        ></script>
        @include('public.script')
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.8.0"
            id="elementor-pro-webpack-runtime-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.8.1"
            id="elementor-webpack-runtime-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.8.1"
            id="elementor-frontend-modules-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/vendor/regenerator-runtime.min.js?ver=0.13.9"
            id="regenerator-runtime-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=3.15.0"
            id="wp-polyfill-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/hooks.min.js?ver=4169d3cf8e8d95a3d6d5"
            id="wp-hooks-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/i18n.min.js?ver=9e794f35a71bb98672ae"
            id="wp-i18n-js"
        ></script>
        <script type="text/javascript" id="wp-i18n-js-after">
            wp.i18n.setLocaleData({ "text directionltr": ["ltr"] });
        </script>
        <script type="text/javascript" id="elementor-pro-frontend-js-before">
            var ElementorProFrontendConfig = {
                ajaxurl: "httpsstaging.dndcleaners.ca\/wp-admin\/admin-ajax.php",
                nonce: "ea03ce6765",
                urls: {
                    assets: "https:\/staging.dndcleaners.ca\/wp-content\/plugins\/elementor-pro\/assets\/",
                    rest: "httpsstaging.dndcleaners.ca\/wp-json\/",
                },
                shareButtonsNetworks: {
                    facebook: { title: "Facebook", has_counter: true },
                    twitter: { title: "Twitter" },
                    linkedin: { title: "LinkedIn", has_counter: true },
                    pinterest: { title: "Pinterest", has_counter: true },
                    reddit: { title: "Reddit", has_counter: true },
                    vk: { title: "VK", has_counter: true },
                    odnoklassniki: { title: "OK", has_counter: true },
                    tumblr: { title: "Tumblr" },
                    digg: { title: "Digg" },
                    skype: { title: "Skype" },
                    stumbleupon: { title: "StumbleUpon", has_counter: true },
                    mix: { title: "Mix" },
                    telegram: { title: "Telegram" },
                    pocket: { title: "Pocket", has_counter: true },
                    xing: { title: "XING", has_counter: true },
                    whatsapp: { title: "WhatsApp" },
                    email: { title: "Email" },
                    print: { title: "Print" },
                },
                woocommerce: {
                    menu_cart: {
                        cart_page_url: "httpsstaging.dndcleaners.ca\/cart\/",
                        checkout_page_url: "httpsstaging.dndcleaners.ca\/checkout\/",
                        fragments_nonce: "214bd84b63",
                    },
                },
                facebook_sdk: { lang: "en_US", app_id: "" },
                lottie: {
                    defaultAnimationUrl:
                        "https:\/staging.dndcleaners.ca\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json",
                },
            };
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.8.0"
            id="elementor-pro-frontend-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2"
            id="elementor-waypoints-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/jquery/ui/core.min.js?ver=1.13.2"
            id="jquery-ui-core-js"
        ></script>
        <script type="text/javascript" id="elementor-frontend-js-before">
            var elementorFrontendConfig = {
                environmentMode: { edit: false, wpPreview: false, isScriptDebug: false },
                i18n: {
                    shareOnFacebook: "Share on Facebook",
                    shareOnTwitter: "Share on Twitter",
                    pinIt: "Pin it",
                    download: "Download",
                    downloadImage: "Download image",
                    fullscreen: "Fullscreen",
                    zoom: "Zoom",
                    share: "Share",
                    playVideo: "Play Video",
                    previous: "Previous",
                    next: "Next",
                    close: "Close",
                },
                is_rtl: false,
                breakpoints: { xs: 0, sm: 480, md: 768, lg: 1025, xl: 1440, xxl: 1600 },
                responsive: {
                    breakpoints: {
                        mobile: { label: "Mobile", value: 767, default_value: 767, direction: "max", is_enabled: true },
                        mobile_extra: {
                            label: "Mobile Extra",
                            value: 880,
                            default_value: 880,
                            direction: "max",
                            is_enabled: false,
                        },
                        tablet: {
                            label: "Tablet",
                            value: 1024,
                            default_value: 1024,
                            direction: "max",
                            is_enabled: true,
                        },
                        tablet_extra: {
                            label: "Tablet Extra",
                            value: 1200,
                            default_value: 1200,
                            direction: "max",
                            is_enabled: false,
                        },
                        laptop: {
                            label: "Laptop",
                            value: 1366,
                            default_value: 1366,
                            direction: "max",
                            is_enabled: false,
                        },
                        widescreen: {
                            label: "Widescreen",
                            value: 2400,
                            default_value: 2400,
                            direction: "min",
                            is_enabled: false,
                        },
                    },
                },
                version: "3.8.1",
                is_static: false,
                experimentalFeatures: {
                    e_dom_optimization: true,
                    e_optimized_assets_loading: true,
                    e_optimized_css_loading: true,
                    a11y_improvements: true,
                    additional_custom_breakpoints: true,
                    e_import_export: true,
                    e_hidden_wordpress_widgets: true,
                    theme_builder_v2: true,
                    "landing-pages": true,
                    "elements-color-picker": true,
                    "favorite-widgets": true,
                    "admin-top-bar": true,
                    "page-transitions": true,
                    notes: true,
                    loop: true,
                    "form-submissions": true,
                    e_scroll_snap: true,
                },
                urls: { assets: "https:\/staging.dndcleaners.ca\/wp-content\/plugins\/elementor\/assets\/" },
                settings: { page: [], editorPreferences: [] },
                kit: {
                    active_breakpoints: ["viewport_mobile", "viewport_tablet"],
                    global_image_lightbox: "yes",
                    lightbox_enable_counter: "yes",
                    lightbox_enable_fullscreen: "yes",
                    lightbox_enable_zoom: "yes",
                    lightbox_enable_share: "yes",
                    lightbox_title_src: "title",
                    lightbox_description_src: "description",
                    woocommerce_notices_elements: [],
                },
                post: { id: 68, title: "Sale%20-%20Linh%20Linh%20Shop", excerpt: "", featuredImage: false },
            };
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.8.1"
            id="elementor-frontend-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor-pro/assets/js/elements-handlers.min.js?ver=3.8.0"
            id="pro-elements-handlers-js"
        ></script>
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-11-15 14:06:35 by W3 Total Cache
--></html>
