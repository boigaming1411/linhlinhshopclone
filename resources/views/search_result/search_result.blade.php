<!DOCTYPE html>
<html dir="ltr" lang="en-US" prefix="og: https://ogp.me/ns#">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="profile" href="https://gmpg.org/xfn/11" />
        <title>Search Result - Linh Linh Shop</title>

        <!-- All in One SEO 4.2.7.1 - aioseo.com -->
        <meta name="robots" content="noindex, nofollow, max-image-preview:large" />
        <link rel="canonical" href="/shop/" />
        <meta name="generator" content="All in One SEO (AIOSEO) 4.2.7.1 " />
        <meta property="og:locale" content="en_US" />
        <meta property="og:site_name" content="Linh Linh Shop - Professional Alternations &amp; Repairs" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Shop - Linh Linh Shop" />
        <meta property="og:url" content="/shop/" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Shop - Linh Linh Shop" />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "BreadcrumbList",
                        "@id": "https:\/staging.dndcleaners.ca\/shop\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "https:\/staging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "https:\/staging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "Professional Alternations & Repairs",
                                    "url": "https:\/staging.dndcleaners.ca\/"
                                },
                                "nextItem": "https:\/staging.dndcleaners.ca\/shop\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "https:\/staging.dndcleaners.ca\/shop\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "https:\/staging.dndcleaners.ca\/shop\/",
                                    "name": "Shop",
                                    "url": "https:\/staging.dndcleaners.ca\/shop\/"
                                },
                                "previousItem": "https:\/staging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "CollectionPage",
                        "@id": "https:\/staging.dndcleaners.ca\/shop\/#collectionpage",
                        "url": "https:\/staging.dndcleaners.ca\/shop\/",
                        "name": "Shop - Linh Linh Shop",
                        "inLanguage": "en-US",
                        "isPartOf": { "@id": "https:\/staging.dndcleaners.ca\/#website" },
                        "breadcrumb": { "@id": "https:\/staging.dndcleaners.ca\/shop\/#breadcrumblist" }
                    },
                    {
                        "@type": "Organization",
                        "@id": "https:\/staging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "https:\/staging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "WebSite",
                        "@id": "https:\/staging.dndcleaners.ca\/#website",
                        "url": "https:\/staging.dndcleaners.ca\/",
                        "name": "Linh Linh Shop",
                        "description": "Professional Alternations & Repairs",
                        "inLanguage": "en-US",
                        "publisher": { "@id": "https:\/staging.dndcleaners.ca\/#organization" }
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->

       
        <link
            rel="alternate"
            type="application/rss+xml"
            title="Linh Linh Shop &raquo; Products Feed"
            href="/shop/feed/"
        />
       
        <link
            rel="stylesheet"
            id="envo-extra-css"
            href="/wp-content/plugins/envo-extra/css/style.css?ver=1.4.3"
            type="text/css"
            media="all"
        />
        @include('public.header')
    </head>
    <body
        data-rsssl="1"
        id="blog"
        class="archive post-type-archive post-type-archive-product theme-envo-storefront woocommerce-shop woocommerce woocommerce-page woocommerce-no-js elementor-default elementor-kit-1847"
    >
        @include('public.top-content')
        <div class="page-wrap">
           <div class="top-bar-section container-fluid">
                @include('public.top-bar-section')
                
            </div>
            <div class="main-menu">
               @include('public.navbar')
            </div>
            <div id="site-content" class="container main-container" role="main">
                <div class="page-area">
                    <!-- start content container -->
                    <div class="row">
                        <article class="col-md-9">
                            <h3 id="query">Search Result: "{{$query}}"</h3>
                            <h1 class="page-title">Shop</h1>                           
                            <div class="woocommerce-notices-wrapper"></div>
                            <p class="woocommerce-result-count">Showing all 12 results</p>
                            <form class="woocommerce-ordering" method="get">
                                <select name="orderby" class="orderby" aria-label="Shop order">
                                    <option value="menu_order" selected>Default sorting</option>
                                    <option value="popularity">Sort by popularity</option>
                                    <option value="rating">Sort by average rating</option>
                                    <option value="date">Sort by latest</option>
                                    <option value="price">Sort by price: low to high</option>
                                    <option value="price-desc">Sort by price: high to low</option>
                                </select>
                                <input type="hidden" name="paged" value="1" />
                            </form>
                                        <!-- Show product -->
                            
                               <ul class="products columns-4" style="display: flex;flex-wrap: wrap;">
                                @if(!is_null($data))
                                    @foreach ($data as $item)
                                        <li
                                            style="flex: 0 0 21%;"
                                            class="product type-product post-22 status-publish first instock product_cat-clothing product_cat-womens-clothing has-post-thumbnail sale shipping-taxable purchasable product-type-simple"
                                        >
                                            <a
                                                href="/product/bikini/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                            >
                                                @if(($item->on_sale))
                                                    <span class="onsale">Sale!</span>
                                                @endif
                                                <img
                                                    width="300"
                                                    height="300"
                                                    src="{{$item->images[0]->src}}"
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                    alt=""
                                                    decoding="async"
                                                    loading="lazy"
                                                    sizes="(max-width: 300px) 100vw, 300px"
                                                />
                                                <h2 class="woocommerce-loop-product__title">{{$item->name}}</h2>
                                                <span class="price"
                                                    >{!!$item->price_html!!}</span
                                                > </a
                                                ><a
                                                    href="/shop/?add-to-cart=22"
                                                    data-quantity="1"
                                                    class="button wp-element-button product_type_simple add_to_cart_button ajax_add_to_cart"
                                                    data-product_id="22"
                                                    data-product_sku=""
                                                    aria-label="Add &ldquo;Bikini&rdquo; to your cart"
                                                    rel="nofollow"
                                                    >Add to cart</a
                                                >
                                            </li>
                                        @endforeach
                                     @endif
                                </ul>
                                                         
                        </article>

                        <aside id="sidebar" class="col-md-3">
                            <div id="woocommerce_price_filter-1" class="widget woocommerce widget_price_filter">
                                <div class="widget-title"><h3>Filter by price</h3></div>
                                <form method="get" action="https://staging.dndcleaners.ca/shop/">
                                    <div class="price_slider_wrapper">
                                        <div class="price_slider" style="display: none"></div>
                                        <div class="price_slider_amount" data-step="10">
                                            <label class="screen-reader-text" for="min_price">Min price</label>
                                            <input
                                                type="text"
                                                id="min_price"
                                                name="min_price"
                                                value="10"
                                                data-min="10"
                                                placeholder="Min price"
                                            />
                                            <label class="screen-reader-text" for="max_price">Max price</label>
                                            <input
                                                type="text"
                                                id="max_price"
                                                name="max_price"
                                                value="50"
                                                data-max="50"
                                                placeholder="Max price"
                                            />
                                            <button type="submit" class="button wp-element-button">Filter</button>
                                            <div class="price_label" style="display: none">
                                                Price: <span class="from"></span> &mdash; <span class="to"></span>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="woocommerce_widget_cart-1" class="widget woocommerce widget_shopping_cart">
                                <div class="widget-title"><h3>Cart</h3></div>
                                <div class="widget_shopping_cart_content"></div>
                            </div>
                            <div id="block-10" class="widget widget_block">
                                <div class="widget-title"><h3>Search Products</h3></div>
                                <div class="wp-widget-group__inner-blocks">
                                    <form
                                        role="search"
                                        method="get"
                                        action="/search"
                                        class="wp-block-search__button-outside wp-block-search__text-button wp-block-search"
                                    >
                                        <label
                                            for="wp-block-search__input-2"
                                            class="wp-block-search__label screen-reader-text"
                                            >Search</label
                                        >
                                        <div class="wp-block-search__inside-wrapper">
                                            <input
                                                type="search"
                                                id="wp-block-search__input-2"
                                                class="wp-block-search__input wp-block-search__input"
                                                name="s"
                                                value=""
                                                placeholder="Search products..."
                                                required
                                            /><input type="hidden" name="post_type" value="product" /><button
                                                type="submit"
                                                class="wp-block-search__button wp-element-button"
                                            >
                                                Search
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="woocommerce_products-1" class="widget woocommerce widget_products">
                                <div class="widget-title"><h3>Products</h3></div>
                                <div id="side_bar"></div>
                            </div>
                        </aside>
                    </div>
                    <!-- end content container -->
                </div>
                <!-- end main-container -->
            </div>
            <!-- end page-area -->

            @include('public.footer')
        </div>
        <!-- end page-wrap -->
        <script
            type="text/javascript"
            src="/js/vuejs/vue.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/vuejs/moment.min.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/search.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/sidebar.js"
        ></script>
        @include('public.script')
        <script
            type="text/javascript"
            src="/wp-includes/js/jquery/ui/core.min.js?ver=1.13.2"
            id="jquery-ui-core-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/jquery/ui/mouse.min.js?ver=1.13.2"
            id="jquery-ui-mouse-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/jquery/ui/slider.min.js?ver=1.13.2"
            id="jquery-ui-slider-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/jquery-ui-touch-punch/jquery-ui-touch-punch.min.js?ver=7.1.0"
            id="wc-jquery-ui-touchpunch-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/accounting/accounting.min.js?ver=0.4.2"
            id="accounting-js"
        ></script>
        <script type="text/javascript" id="wc-price-slider-js-extra">
            /* <![CDATA[ */
            var woocommerce_price_slider_params = {
                currency_format_num_decimals: "0",
                currency_format_symbol: "$",
                currency_format_decimal_sep: ".",
                currency_format_thousand_sep: ",",
                currency_format: "%s%v",
            };
            /* ]]> */
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/frontend/price-slider.min.js?ver=7.1.0"
            id="wc-price-slider-js"
        ></script>
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-11-15 14:05:20 by W3 Total Cache
--></html>
