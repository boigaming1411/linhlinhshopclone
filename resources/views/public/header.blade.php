        <link rel="dns-prefetch" href="//fonts.googleapis.com" />
        <link rel="dns-prefetch" href="//use.fontawesome.com" />
        <link href="https://fonts.gstatic.com/" crossorigin rel="preconnect" />
        
        
        <link rel="alternate" type="application/rss+xml" title="Linh Linh Shop &raquo; Feed" href="/feed/" />
        <link
            rel="alternate"
            type="application/rss+xml"
            title="Linh Linh Shop &raquo; Comments Feed"
            href="/comments/feed/"
        />

        
        <script type="text/javascript"> 
            window._wpemojiSettings = {
                baseUrl: "https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/72x72\/",
                ext: ".png",
                svgUrl: "https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/svg\/",
                svgExt: ".svg",
                source: {
                    concatemoji:
                        "https:\/staging.dndcleaners.ca\/wp-includes\/js\/wp-emoji-release.min.js?ver=d6eb82c324cab2f36e345863fad95090",
                },
            };
            /*! This file is auto-generated */
            !(function (e, a, t) {
                var n,
                    r,
                    o,
                    i = a.createElement("canvas"),
                    p = i.getContext && i.getContext("2d");
                function s(e, t) {
                    var a = String.fromCharCode,
                        e = (p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, e), 0, 0), i.toDataURL());
                    return (
                        p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, t), 0, 0), e === i.toDataURL()
                    );
                }
                function c(e) {
                    var t = a.createElement("script");
                    (t.src = e),
                        (t.defer = t.type = "text/javascript"),
                        a.getElementsByTagName("head")[0].appendChild(t);
                }
                for (
                    o = Array("flag", "emoji"), t.supports = { everything: !0, everythingExceptFlag: !0 }, r = 0;
                    r < o.length;
                    r++
                )
                    (t.supports[o[r]] = (function (e) {
                        if (p && p.fillText)
                            switch (((p.textBaseline = "top"), (p.font = "600 32px Arial"), e)) {
                                case "flag":
                                    return s([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039])
                                        ? !1
                                        : !s([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) &&
                                              !s(
                                                  [
                                                      55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128,
                                                      56430, 56128, 56423, 56128, 56447,
                                                  ],
                                                  [
                                                      55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128,
                                                      56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447,
                                                  ]
                                              );
                                case "emoji":
                                    return !s(
                                        [129777, 127995, 8205, 129778, 127999],
                                        [129777, 127995, 8203, 129778, 127999]
                                    );
                            }
                        return !1;
                    })(o[r])),
                        (t.supports.everything = t.supports.everything && t.supports[o[r]]),
                        "flag" !== o[r] &&
                            (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[o[r]]);
                (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag),
                    (t.DOMReady = !1),
                    (t.readyCallback = function () {
                        t.DOMReady = !0;
                    }),
                    t.supports.everything ||
                        ((n = function () {
                            t.readyCallback();
                        }),
                        a.addEventListener
                            ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1))
                            : (e.attachEvent("onload", n),
                              a.attachEvent("onreadystatechange", function () {
                                  "complete" === a.readyState && t.readyCallback();
                              })),
                        (e = t.source || {}).concatemoji
                            ? c(e.concatemoji)
                            : e.wpemoji && e.twemoji && (c(e.twemoji), c(e.wpemoji)));
            })(window, document, window._wpemojiSettings);
        </script>
        <style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 0.07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
        </style>
        <link
            rel="stylesheet"
            id="wp-block-library-css"
            href="/wp-includes/css/dist/block-library/style.min.css?ver=d6eb82c324cab2f36e345863fad95090"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="wc-blocks-vendors-style-css"
            href="/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/wc-blocks-vendors-style.css?ver=8.7.5"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="wc-blocks-style-css"
            href="/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/wc-blocks-style.css?ver=8.7.5"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="classic-theme-styles-css"
            href="/wp-includes/css/classic-themes.min.css?ver=1"
            type="text/css"
            media="all"
        />
        <style id="global-styles-inline-css" type="text/css">
            body {
                --wp--preset--color--black: #000000;
                --wp--preset--color--cyan-bluish-gray: #abb8c3;
                --wp--preset--color--white: #ffffff;
                --wp--preset--color--pale-pink: #f78da7;
                --wp--preset--color--vivid-red: #cf2e2e;
                --wp--preset--color--luminous-vivid-orange: #ff6900;
                --wp--preset--color--luminous-vivid-amber: #fcb900;
                --wp--preset--color--light-green-cyan: #7bdcb5;
                --wp--preset--color--vivid-green-cyan: #00d084;
                --wp--preset--color--pale-cyan-blue: #8ed1fc;
                --wp--preset--color--vivid-cyan-blue: #0693e3;
                --wp--preset--color--vivid-purple: #9b51e0;
                --wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(
                    135deg,
                    rgba(6, 147, 227, 1) 0%,
                    rgb(155, 81, 224) 100%
                );
                --wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(
                    135deg,
                    rgb(122, 220, 180) 0%,
                    rgb(0, 208, 130) 100%
                );
                --wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(
                    135deg,
                    rgba(252, 185, 0, 1) 0%,
                    rgba(255, 105, 0, 1) 100%
                );
                --wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(
                    135deg,
                    rgba(255, 105, 0, 1) 0%,
                    rgb(207, 46, 46) 100%
                );
                --wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(
                    135deg,
                    rgb(238, 238, 238) 0%,
                    rgb(169, 184, 195) 100%
                );
                --wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(
                    135deg,
                    rgb(74, 234, 220) 0%,
                    rgb(151, 120, 209) 20%,
                    rgb(207, 42, 186) 40%,
                    rgb(238, 44, 130) 60%,
                    rgb(251, 105, 98) 80%,
                    rgb(254, 248, 76) 100%
                );
                --wp--preset--gradient--blush-light-purple: linear-gradient(
                    135deg,
                    rgb(255, 206, 236) 0%,
                    rgb(152, 150, 240) 100%
                );
                --wp--preset--gradient--blush-bordeaux: linear-gradient(
                    135deg,
                    rgb(254, 205, 165) 0%,
                    rgb(254, 45, 45) 50%,
                    rgb(107, 0, 62) 100%
                );
                --wp--preset--gradient--luminous-dusk: linear-gradient(
                    135deg,
                    rgb(255, 203, 112) 0%,
                    rgb(199, 81, 192) 50%,
                    rgb(65, 88, 208) 100%
                );
                --wp--preset--gradient--pale-ocean: linear-gradient(
                    135deg,
                    rgb(255, 245, 203) 0%,
                    rgb(182, 227, 212) 50%,
                    rgb(51, 167, 181) 100%
                );
                --wp--preset--gradient--electric-grass: linear-gradient(
                    135deg,
                    rgb(202, 248, 128) 0%,
                    rgb(113, 206, 126) 100%
                );
                --wp--preset--gradient--midnight: linear-gradient(135deg, rgb(2, 3, 129) 0%, rgb(40, 116, 252) 100%);
                --wp--preset--duotone--dark-grayscale: url("#wp-duotone-dark-grayscale");
                --wp--preset--duotone--grayscale: url("#wp-duotone-grayscale");
                --wp--preset--duotone--purple-yellow: url("#wp-duotone-purple-yellow");
                --wp--preset--duotone--blue-red: url("#wp-duotone-blue-red");
                --wp--preset--duotone--midnight: url("#wp-duotone-midnight");
                --wp--preset--duotone--magenta-yellow: url("#wp-duotone-magenta-yellow");
                --wp--preset--duotone--purple-green: url("#wp-duotone-purple-green");
                --wp--preset--duotone--blue-orange: url("#wp-duotone-blue-orange");
                --wp--preset--font-size--small: 13px;
                --wp--preset--font-size--medium: 20px;
                --wp--preset--font-size--large: 36px;
                --wp--preset--font-size--x-large: 42px;
                --wp--preset--spacing--20: 0.44rem;
                --wp--preset--spacing--30: 0.67rem;
                --wp--preset--spacing--40: 1rem;
                --wp--preset--spacing--50: 1.5rem;
                --wp--preset--spacing--60: 2.25rem;
                --wp--preset--spacing--70: 3.38rem;
                --wp--preset--spacing--80: 5.06rem;
            }
            :where(.is-layout-flex) {
                gap: 0.5em;
            }
            body .is-layout-flow > .alignleft {
                float: left;
                margin-inline-start: 0;
                margin-inline-end: 2em;
            }
            body .is-layout-flow > .alignright {
                float: right;
                margin-inline-start: 2em;
                margin-inline-end: 0;
            }
            body .is-layout-flow > .aligncenter {
                margin-left: auto !important;
                margin-right: auto !important;
            }
            body .is-layout-constrained > .alignleft {
                float: left;
                margin-inline-start: 0;
                margin-inline-end: 2em;
            }
            body .is-layout-constrained > .alignright {
                float: right;
                margin-inline-start: 2em;
                margin-inline-end: 0;
            }
            body .is-layout-constrained > .aligncenter {
                margin-left: auto !important;
                margin-right: auto !important;
            }
            body .is-layout-constrained > :where(:not(.alignleft):not(.alignright):not(.alignfull)) {
                max-width: var(--wp--style--global--content-size);
                margin-left: auto !important;
                margin-right: auto !important;
            }
            body .is-layout-constrained > .alignwide {
                max-width: var(--wp--style--global--wide-size);
            }
            body .is-layout-flex {
                display: flex;
            }
            body .is-layout-flex {
                flex-wrap: wrap;
                align-items: center;
            }
            body .is-layout-flex > * {
                margin: 0;
            }
            :where(.wp-block-columns.is-layout-flex) {
                gap: 2em;
            }
            .has-black-color {
                color: var(--wp--preset--color--black) !important;
            }
            .has-cyan-bluish-gray-color {
                color: var(--wp--preset--color--cyan-bluish-gray) !important;
            }
            .has-white-color {
                color: var(--wp--preset--color--white) !important;
            }
            .has-pale-pink-color {
                color: var(--wp--preset--color--pale-pink) !important;
            }
            .has-vivid-red-color {
                color: var(--wp--preset--color--vivid-red) !important;
            }
            .has-luminous-vivid-orange-color {
                color: var(--wp--preset--color--luminous-vivid-orange) !important;
            }
            .has-luminous-vivid-amber-color {
                color: var(--wp--preset--color--luminous-vivid-amber) !important;
            }
            .has-light-green-cyan-color {
                color: var(--wp--preset--color--light-green-cyan) !important;
            }
            .has-vivid-green-cyan-color {
                color: var(--wp--preset--color--vivid-green-cyan) !important;
            }
            .has-pale-cyan-blue-color {
                color: var(--wp--preset--color--pale-cyan-blue) !important;
            }
            .has-vivid-cyan-blue-color {
                color: var(--wp--preset--color--vivid-cyan-blue) !important;
            }
            .has-vivid-purple-color {
                color: var(--wp--preset--color--vivid-purple) !important;
            }
            .has-black-background-color {
                background-color: var(--wp--preset--color--black) !important;
            }
            .has-cyan-bluish-gray-background-color {
                background-color: var(--wp--preset--color--cyan-bluish-gray) !important;
            }
            .has-white-background-color {
                background-color: var(--wp--preset--color--white) !important;
            }
            .has-pale-pink-background-color {
                background-color: var(--wp--preset--color--pale-pink) !important;
            }
            .has-vivid-red-background-color {
                background-color: var(--wp--preset--color--vivid-red) !important;
            }
            .has-luminous-vivid-orange-background-color {
                background-color: var(--wp--preset--color--luminous-vivid-orange) !important;
            }
            .has-luminous-vivid-amber-background-color {
                background-color: var(--wp--preset--color--luminous-vivid-amber) !important;
            }
            .has-light-green-cyan-background-color {
                background-color: var(--wp--preset--color--light-green-cyan) !important;
            }
            .has-vivid-green-cyan-background-color {
                background-color: var(--wp--preset--color--vivid-green-cyan) !important;
            }
            .has-pale-cyan-blue-background-color {
                background-color: var(--wp--preset--color--pale-cyan-blue) !important;
            }
            .has-vivid-cyan-blue-background-color {
                background-color: var(--wp--preset--color--vivid-cyan-blue) !important;
            }
            .has-vivid-purple-background-color {
                background-color: var(--wp--preset--color--vivid-purple) !important;
            }
            .has-black-border-color {
                border-color: var(--wp--preset--color--black) !important;
            }
            .has-cyan-bluish-gray-border-color {
                border-color: var(--wp--preset--color--cyan-bluish-gray) !important;
            }
            .has-white-border-color {
                border-color: var(--wp--preset--color--white) !important;
            }
            .has-pale-pink-border-color {
                border-color: var(--wp--preset--color--pale-pink) !important;
            }
            .has-vivid-red-border-color {
                border-color: var(--wp--preset--color--vivid-red) !important;
            }
            .has-luminous-vivid-orange-border-color {
                border-color: var(--wp--preset--color--luminous-vivid-orange) !important;
            }
            .has-luminous-vivid-amber-border-color {
                border-color: var(--wp--preset--color--luminous-vivid-amber) !important;
            }
            .has-light-green-cyan-border-color {
                border-color: var(--wp--preset--color--light-green-cyan) !important;
            }
            .has-vivid-green-cyan-border-color {
                border-color: var(--wp--preset--color--vivid-green-cyan) !important;
            }
            .has-pale-cyan-blue-border-color {
                border-color: var(--wp--preset--color--pale-cyan-blue) !important;
            }
            .has-vivid-cyan-blue-border-color {
                border-color: var(--wp--preset--color--vivid-cyan-blue) !important;
            }
            .has-vivid-purple-border-color {
                border-color: var(--wp--preset--color--vivid-purple) !important;
            }
            .has-vivid-cyan-blue-to-vivid-purple-gradient-background {
                background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;
            }
            .has-light-green-cyan-to-vivid-green-cyan-gradient-background {
                background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;
            }
            .has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background {
                background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;
            }
            .has-luminous-vivid-orange-to-vivid-red-gradient-background {
                background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;
            }
            .has-very-light-gray-to-cyan-bluish-gray-gradient-background {
                background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;
            }
            .has-cool-to-warm-spectrum-gradient-background {
                background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;
            }
            .has-blush-light-purple-gradient-background {
                background: var(--wp--preset--gradient--blush-light-purple) !important;
            }
            .has-blush-bordeaux-gradient-background {
                background: var(--wp--preset--gradient--blush-bordeaux) !important;
            }
            .has-luminous-dusk-gradient-background {
                background: var(--wp--preset--gradient--luminous-dusk) !important;
            }
            .has-pale-ocean-gradient-background {
                background: var(--wp--preset--gradient--pale-ocean) !important;
            }
            .has-electric-grass-gradient-background {
                background: var(--wp--preset--gradient--electric-grass) !important;
            }
            .has-midnight-gradient-background {
                background: var(--wp--preset--gradient--midnight) !important;
            }
            .has-small-font-size {
                font-size: var(--wp--preset--font-size--small) !important;
            }
            .has-medium-font-size {
                font-size: var(--wp--preset--font-size--medium) !important;
            }
            .has-large-font-size {
                font-size: var(--wp--preset--font-size--large) !important;
            }
            .has-x-large-font-size {
                font-size: var(--wp--preset--font-size--x-large) !important;
            }
            .wp-block-navigation a:where(:not(.wp-element-button)) {
                color: inherit;
            }
            :where(.wp-block-columns.is-layout-flex) {
                gap: 2em;
            }
            .wp-block-pullquote {
                font-size: 1.5em;
                line-height: 1.6;
            }
        </style>
        <link
            rel="stylesheet"
            id="envo-extra-css"
            href="/wp-content/plugins/envo-extra/css/style.css?ver=1.4.4"
            type="text/css"
            media="all"
        />


        <link
            rel="stylesheet"
            id="woocommerce-layout-css"
            href="/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=7.1.0"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="woocommerce-smallscreen-css"
            href="/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=7.1.0"
            type="text/css"
            media="only screen and (max-width: 768px)"
        />
        <link
            rel="stylesheet"
            id="woocommerce-general-css"
            href="/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=7.1.0"
            type="text/css"
            media="all"
        />
        <style id="woocommerce-inline-inline-css" type="text/css">
            .woocommerce form .form-row .required {
                visibility: visible;
            }
        </style>
        <link
            rel="stylesheet"
            id="envo-storefront-fonts-css"
            href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed%3A300%2C500%2C700&#038;subset=cyrillic%2Ccyrillic-ext%2Cgreek%2Cgreek-ext%2Clatin-ext%2Cvietnamese"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="bootstrap-css"
            href="/wp-content/themes/envo-storefront/css/bootstrap.css?ver=3.3.7"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="mmenu-light-css"
            href="/wp-content/themes/envo-storefront/css/mmenu-light.min.css?ver=1.0.7"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="envo-storefront-stylesheet-css"
            href="/wp-content/themes/envo-storefront/style.css?ver=1.0.7"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="font-awesome-css"
            href="/wp-content/plugins/elementor/assets/lib/font-awesome/css/font-awesome.min.css?ver=4.7.0"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="font-awesome-official-css"
            href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
            type="text/css"
            media="all"
            integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm"
            crossorigin="anonymous"
        />
        <link
            rel="stylesheet"
            id="font-awesome-official-v4shim-css"
            href="https://use.fontawesome.com/releases/v5.15.4/css/v4-shims.css"
            type="text/css"
            media="all"
            integrity="sha384-Vq76wejb3QJM4nDatBa5rUOve+9gkegsjCebvV/9fvXlGWo4HCMR4cJZjjcF6Viv"
            crossorigin="anonymous"
        />
        <style id="font-awesome-official-v4shim-inline-css" type="text/css">
            @font-face {
                font-family: "FontAwesome";
                font-display: block;
                src: url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-brands-400.eot"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-brands-400.eot?#iefix")
                        format("embedded-opentype"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-brands-400.woff2") format("woff2"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-brands-400.woff") format("woff"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-brands-400.ttf") format("truetype"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-brands-400.svg#fontawesome")
                        format("svg");
            }

            @font-face {
                font-family: "FontAwesome";
                font-display: block;
                src: url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.eot"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.eot?#iefix")
                        format("embedded-opentype"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.woff2") format("woff2"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.woff") format("woff"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.ttf") format("truetype"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-solid-900.svg#fontawesome")
                        format("svg");
            }

            @font-face {
                font-family: "FontAwesome";
                font-display: block;
                src: url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-regular-400.eot"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-regular-400.eot?#iefix")
                        format("embedded-opentype"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-regular-400.woff2") format("woff2"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-regular-400.woff") format("woff"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-regular-400.ttf") format("truetype"),
                    url("https://use.fontawesome.com/releases/v5.15.4/webfonts/fa-regular-400.svg#fontawesome")
                        format("svg");
                unicode-range: U+F004-F005, U+F007, U+F017, U+F022, U+F024, U+F02E, U+F03E, U+F044, U+F057-F059, U+F06E,
                    U+F070, U+F075, U+F07B-F07C, U+F080, U+F086, U+F089, U+F094, U+F09D, U+F0A0, U+F0A4-F0A7, U+F0C5,
                    U+F0C7-F0C8, U+F0E0, U+F0EB, U+F0F3, U+F0F8, U+F0FE, U+F111, U+F118-F11A, U+F11C, U+F133, U+F144,
                    U+F146, U+F14A, U+F14D-F14E, U+F150-F152, U+F15B-F15C, U+F164-F165, U+F185-F186, U+F191-F192, U+F1AD,
                    U+F1C1-F1C9, U+F1CD, U+F1D8, U+F1E3, U+F1EA, U+F1F6, U+F1F9, U+F20A, U+F247-F249, U+F24D,
                    U+F254-F25B, U+F25D, U+F267, U+F271-F274, U+F279, U+F28B, U+F28D, U+F2B5-F2B6, U+F2B9, U+F2BB,
                    U+F2BD, U+F2C1-F2C2, U+F2D0, U+F2D2, U+F2DC, U+F2ED, U+F328, U+F358-F35B, U+F3A5, U+F3D1, U+F410,
                    U+F4AD;
            }
        </style>
        <script
            type="text/javascript"
            src="/wp-content/plugins/jquery-updater/js/jquery-3.6.1.min.js?ver=3.6.1"
            id="jquery-core-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/jquery-updater/js/jquery-migrate-3.4.0.min.js?ver=3.4.0"
            id="jquery-migrate-js"
        ></script>
        <link rel="https://api.w.org/" href="/wp-json/" />
        <link rel="alternate" type="application/json" href="/wp-json/wp/v2/product/22" />
        <link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
        <link
            rel="alternate"
            type="application/json+oembed"
            href="/wp-json/oembed/1.0/embed?url=https%3A%2F%2F%2Fproduct%2Fbikini%2F"
        />
        <link
            rel="alternate"
            type="text/xml+oembed"
            href="/wp-json/oembed/1.0/embed?url=https%3A%2F%2F%2Fproduct%2Fbikini%2F&#038;format=xml"
        />
        <noscript
            ><style>
                .woocommerce-product-gallery {
                    opacity: 1 !important;
                }
            </style></noscript
        >
        <link rel="icon" href="/wp-content/uploads/2022/03/cropped-dndcleaners-site-icon-32x32.png" sizes="32x32" />
        <link rel="icon" href="/wp-content/uploads/2022/03/cropped-dndcleaners-site-icon-192x192.png" sizes="192x192" />
        <link rel="apple-touch-icon" href="/wp-content/uploads/2022/03/cropped-dndcleaners-site-icon-180x180.png" />
        <meta
            name="msapplication-TileImage"
            content="/wp-content/uploads/2022/03/cropped-dndcleaners-site-icon-270x270.png"
        />
        <style id="sccss">
            #sidebar {
                transition: 0.1s linear all;
            }
            .woocommerce div.product .woocommerce-tabs ul.tabs {
                display: flex;
            }
            .woocommerce div.product .woocommerce-tabs ul.tabs li a {
                white-space: nowrap;
            }

            @media (max-width: 1024px) {
            }
            @media (max-width: 768px), (max-width: 930px) and (orientation: landscape) {
                #sidebar {
                    width: 100%;
                }
            }

            @media (max-width: 767px) {
            }
            @media (max-width: 930px) and (orientation: landscape) {
            }
        </style>
        <style id="kirki-inline-styles"></style>