<div class="top-bar-section container-fluid">
                <div class="container">
                    <div class="row">
                        <div id="text-2" class="widget widget_text col-sm-4">
                            <div class="textwidget">
                                <p>
                                    Popular searches: <a href="#">Women </a>// <a href="#">Modern</a> //  <a href="#"
                                        >New</a
                                    >
                                    // <a href="#">Sale</a>
                                </p>
                            </div>
                        </div>
                        <div id="text-3" class="widget widget_text col-sm-4">
                            <div class="textwidget">
                                <p>Limited offer: -20% on all products</p>
                            </div>
                        </div>
                        <div id="text-4" class="widget widget_text col-sm-4">
                            <div class="textwidget">
                                <p>
                                    <i class="fab fa-twitter-square"></i> &nbsp;<i class="fab fa-facebook-square"></i>
                                    &nbsp; <i class="fab fa-youtube-square"></i>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-header container-fluid">
                <div class="container">
                    <div class="heading-row row">
                        <div class="site-heading col-md-4 col-xs-12">
                            <div class="site-branding-logo"></div>
                            <div class="site-branding-text">
                                <p class="site-title"><a href="/" rel="home">Linh Linh Shop</a></p>

                                <p class="site-description">Professional Alternations &amp; Repairs</p>
                            </div>
                            <!-- .site-branding-text -->
                        </div>
                        <div class="search-heading col-md-6 col-xs-12">
                            <div class="header-search-form">
                                <form role="search" method="get" action="/shop">
                                    <input type="hidden" name="post_type" value="product" />
                                    <input
                                        class="header-search-input"
                                        name="s"
                                        type="text"
                                        placeholder="Search products..."
                                    />
                                    <select class="header-search-select" name="product_cat">
                                        <option value="">All Categories</option>
                                        <option value="clothing">Clothing (9)</option>
                                        <option value="mens-clothing">Men&#039;s Clothing (3)</option>
                                        <option value="watches">Watches (3)</option>
                                        <option value="womens-clothing">Women&#039;s Clothing (6)</option>
                                    </select>
                                    <button class="header-search-button" type="submit">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </div>
                            <div class="site-heading-sidebar">
                                <div id="text-5" class="widget widget_text">
                                    <div class="textwidget">
                                        <p>
                                            <i class="fas fa-phone" aria-hidden="true"></i> + 123 654 6548 ||
                                            <i class="far fa-envelope"></i> info@your-mail.com || London Street 569, DH6
                                            SE London &#8211; United Kingdom
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-right col-md-2 hidden-xs">
                            <div class="header-cart">
                                <div class="header-cart-block">
                                    <div class="header-cart-inner">
                                        <a class="cart-contents" href="/cart/" title="View your shopping cart">
                                            <i class="fa fa-shopping-bag"><span class="count">0</span></i>
                                            <div class="amount-cart">&#036;0</div>
                                        </a>
                                        <ul class="site-header-cart menu list-unstyled text-center">
                                            <li>
                                                <div class="widget woocommerce widget_shopping_cart">
                                                    <div class="widget_shopping_cart_content"></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="header-my-account">
                                <div class="header-login">
                                    <a href="/my-account/" title="My Account">
                                        <i class="fa fa-user-circle-o"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>