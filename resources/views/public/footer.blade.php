 <footer id="colophon" class="footer-credits container-fluid">
                <div class="container">
                    <div class="footer-credits-text text-center">
                        Proudly powered by <a href="https://wordpress.org/">WordPress</a>
                        <span class="sep"> | </span> Theme:
                        <a href="https://envothemes.com/free-envo-storefront/">Envo Storefront</a>
                    </div>
                </div>
            </footer>