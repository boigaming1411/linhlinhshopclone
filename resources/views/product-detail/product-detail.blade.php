<!DOCTYPE html>
<html dir="ltr" lang="en-US" prefix="og: https://ogp.me/ns#">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="profile" href="https://gmpg.org/xfn/11" />
        <title>{{$product->name}} - Linh Linh Shop</title>
        <!-- All in One SEO 4.2.7.1 - aioseo.com -->
        <meta
            name="description"
            content="This is a simple product. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Mauris tincidunt sem sed arcu. Nunc auctor. Mauris dictum facilisis augue. Phasellus faucibus molestie nisl."
        />
        <meta name="robots" content="noindex, nofollow, max-image-preview:large" />
        <link rel="canonical" href="/product/bikini/" />
        <meta name="generator" content="All in One SEO (AIOSEO) 4.2.7.1 " />
        <meta property="og:locale" content="en_US" />
        <meta property="og:site_name" content="Linh Linh Shop - Quality - Reputation" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Bikini - Linh Linh Shop" />
        <meta
            property="og:description"
            content="This is a simple product. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Mauris tincidunt sem sed arcu. Nunc auctor. Mauris dictum facilisis augue. Phasellus faucibus molestie nisl."
        />
        <meta property="og:url" content="/product/bikini/" />
        <meta property="article:published_time" content="2019-04-18T11:08:43+00:00" />
        <meta property="article:modified_time" content="2022-11-05T05:50:21+00:00" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Bikini - Linh Linh Shop" />
        <meta
            name="twitter:description"
            content="This is a simple product. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Mauris tincidunt sem sed arcu. Nunc auctor. Mauris dictum facilisis augue. Phasellus faucibus molestie nisl."
        />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "BreadcrumbList",
                        "@id": "httpsstaging.dndcleaners.ca\/product\/bikini\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "Product categories Men Women Jewelry Watch Accessories Baby Sport Summer Sale Shop Now Learn More Men's Fashion Shop Now Women's Fashion Shop Now Featured Best Selling Recent Sale Featured Black pants $89 – $99 Lựa chọn các tùy chọn Giảm giá! Modern $25 $19 Thêm vào giỏ hàng Giảm giá! Single Shirt $30 $22 Thêm",
                                    "url": "httpsstaging.dndcleaners.ca\/"
                                },
                                "nextItem": "httpsstaging.dndcleaners.ca\/product\/bikini\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/product\/bikini\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/product\/bikini\/",
                                    "name": "Bikini",
                                    "description": "This is a simple product. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Mauris tincidunt sem sed arcu. Nunc auctor. Mauris dictum facilisis augue. Phasellus faucibus molestie nisl.",
                                    "url": "httpsstaging.dndcleaners.ca\/product\/bikini\/"
                                },
                                "previousItem": "httpsstaging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "ItemPage",
                        "@id": "httpsstaging.dndcleaners.ca\/product\/bikini\/#itempage",
                        "url": "httpsstaging.dndcleaners.ca\/product\/bikini\/",
                        "name": "Bikini - Linh Linh Shop",
                        "description": "This is a simple product. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Mauris tincidunt sem sed arcu. Nunc auctor. Mauris dictum facilisis augue. Phasellus faucibus molestie nisl.",
                        "inLanguage": "en-US",
                        "isPartOf": { "@id": "httpsstaging.dndcleaners.ca\/#website" },
                        "breadcrumb": { "@id": "httpsstaging.dndcleaners.ca\/product\/bikini\/#breadcrumblist" },
                        "author": "httpsstaging.dndcleaners.ca\/author\/hugo\/#author",
                        "creator": "httpsstaging.dndcleaners.ca\/author\/hugo\/#author",
                        "image": {
                            "@type": "ImageObject",
                            "url": "httpsstaging.dndcleaners.ca\/wp-content\/uploads\/2019\/40\/StockSnap_MO7PZ7AYIC.jpg",
                            "@id": "httpsstaging.dndcleaners.ca\/#mainImage",
                            "width": 1280,
                            "height": 870
                        },
                        "primaryImageOfPage": { "@id": "httpsstaging.dndcleaners.ca\/product\/bikini\/#mainImage" },
                        "datePublished": "2019-04-18T11:08:43+00:00",
                        "dateModified": "2022-11-05T05:50:21+00:00"
                    },
                    {
                        "@type": "Organization",
                        "@id": "httpsstaging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "httpsstaging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "WebSite",
                        "@id": "httpsstaging.dndcleaners.ca\/#website",
                        "url": "httpsstaging.dndcleaners.ca\/",
                        "name": "Linh Linh Shop",
                        "description": "Quality - Reputation",
                        "inLanguage": "en-US",
                        "publisher": { "@id": "httpsstaging.dndcleaners.ca\/#organization" }
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->
        <!-- Same  -->
        <link
            href="/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/cart-frontend.js?ver=6d77f57ad4d2f28e9fae7e9327b354c9"
            as="script"
            rel="prefetch"
        />
        <link href="/wp-includes/js/dist/vendor/lodash.min.js?ver=4.17.19" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/vendor/react.min.js?ver=17.0.1" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=3.15.0" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/vendor/regenerator-runtime.min.js?ver=0.13.9" as="script" rel="prefetch" />
        <link
            href="/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/blocks-checkout.js?ver=1aeb3f9cd2b4952d3b7c658c3ac54767"
            as="script"
            rel="prefetch"
        />
        <link
            href="/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/wc-blocks-registry.js?ver=b6339851784e6ad947a8015afb8847ff"
            as="script"
            rel="prefetch"
        />
        <link href="/wp-includes/js/dist/data.min.js?ver=d8cf5b24f99c64ae47d6" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/compose.min.js?ver=37228270687b2a94e518" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/deprecated.min.js?ver=6c963cb9494ba26b77eb" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/hooks.min.js?ver=4169d3cf8e8d95a3d6d5" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/dom.min.js?ver=133a042fbbef48f38107" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/element.min.js?ver=47162ff4492c7ec4956b" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/vendor/react-dom.min.js?ver=17.0.1" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/escape-html.min.js?ver=03e27a7b6ae14f7afaa6" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/is-shallow-equal.min.js?ver=20c2b06ecf04afb14fee" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/keycodes.min.js?ver=6e0aadc0106bd8aadc89" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/i18n.min.js?ver=9e794f35a71bb98672ae" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/priority-queue.min.js?ver=99e325da95c5a35c7dc2" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/redux-routine.min.js?ver=c9ea6c0df793258797e6" as="script" rel="prefetch" />
        <link
            href="/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/wc-settings.js?ver=7b2188da90828d22e68dae2a8129bd03"
            as="script"
            rel="prefetch"
        />
        <link href="/wp-includes/js/dist/api-fetch.min.js?ver=bc0029ca2c943aec5311" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/url.min.js?ver=bb0ef862199bcae73aa7" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/primitives.min.js?ver=ae0bece54c0487c976b1" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/warning.min.js?ver=4acee5fc2fd9a24cefc2" as="script" rel="prefetch" />
        <link
            href="/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/wc-blocks-data.js?ver=7318f17e99fb56cda4dd7e6fbc16db3c"
            as="script"
            rel="prefetch"
        />
        <link href="/wp-includes/js/dist/data-controls.min.js?ver=e10d473d392daa8501e8" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/html-entities.min.js?ver=36a4a255da7dd2e1bf8e" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/notices.min.js?ver=9c1575b7a31659f45a45" as="script" rel="prefetch" />
        <link
            href="/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/wc-blocks-middleware.js?ver=cfefc68c23f627155c81d4aa8d2dbf3c"
            as="script"
            rel="prefetch"
        />
        <link
            href="/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/wc-blocks-shared-context.js?ver=65e3c38669e82f3ed75ea378b29997f0"
            as="script"
            rel="prefetch"
        />
        <link
            href="/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/wc-blocks-shared-hocs.js?ver=55647c9b478bc30776b5c52dc41b4954"
            as="script"
            rel="prefetch"
        />
        <link
            href="/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/price-format.js?ver=e99bed7a5cf7f43aadbce4396ccbcc5e"
            as="script"
            rel="prefetch"
        />
        <link href="/wp-includes/js/dist/a11y.min.js?ver=ecce20f002eda4c19664" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/dom-ready.min.js?ver=392bdd43726760d1f3ca" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/autop.min.js?ver=43197d709df445ccf849" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/block-editor.min.js?ver=d39738cb7c1202964677" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/blob.min.js?ver=a078f260190acf405764" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/blocks.min.js?ver=69022aed79bfd45b3b1d" as="script" rel="prefetch" />
        <link
            href="/wp-includes/js/dist/block-serialization-default-parser.min.js?ver=eb2cdc8cd7a7975d49d9"
            as="script"
            rel="prefetch"
        />
        <link href="/wp-includes/js/dist/shortcode.min.js?ver=7539044b04e6bca57f2e" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/components.min.js?ver=57b49a5bb19d1f1d566a" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/date.min.js?ver=ce7daf24092d87ff18be" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/vendor/moment.min.js?ver=2.29.4" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/rich-text.min.js?ver=c704284bebe26cf1dd51" as="script" rel="prefetch" />
        <link
            href="/wp-includes/js/dist/keyboard-shortcuts.min.js?ver=b696c16720133edfc065"
            as="script"
            rel="prefetch"
        />
        <link href="/wp-includes/js/dist/style-engine.min.js?ver=10341d6e6decffab850e" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/token-list.min.js?ver=f2cf0bb3ae80de227e43" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/wordcount.min.js?ver=feb9569307aec24292f2" as="script" rel="prefetch" />
        <link href="/wp-includes/js/dist/plugins.min.js?ver=0d1b90278bae7df6ecf9" as="script" rel="prefetch" />
        <link
            href="/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/checkout-frontend.js?ver=a4392447bf571a544c579a23dd39b854"
            as="script"
            rel="prefetch"
        />
        <!-- Same  -->
        
         <!-- Same  -->
        <link
            rel="stylesheet"
            id="photoswipe-css"
            href="/wp-content/plugins/woocommerce/assets/css/photoswipe/photoswipe.min.css?ver=7.1.0"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="photoswipe-default-skin-css"
            href="/wp-content/plugins/woocommerce/assets/css/photoswipe/default-skin/default-skin.min.css?ver=7.1.0"
            type="text/css"
            media="all"
        />
        <!-- Same  -->
        @include('public.header')
    </head>
    <body
        data-rsssl="1"
        id="blog"
        class="product-template-default single single-product postid-22 theme-envo-storefront woocommerce woocommerce-page woocommerce-no-js elementor-default elementor-kit-1847"
    >
    <!-- Same  -->
       @include('public.top-content')
        <!-- Same  -->
        <div class="page-wrap">
            @include('public.top-bar-section')
            
            <div class="main-menu">
                <nav id="site-navigation" class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-header">
                            <span class="navbar-brand brand-absolute visible-xs">Menu</span>
                            <div class="mobile-cart visible-xs">
                                <div class="header-cart">
                                    <div class="header-cart-block">
                                        <div class="header-cart-inner">
                                            <a class="cart-contents" href="/cart/" title="View your shopping cart">
                                                <i class="fa fa-shopping-bag"><span class="count">2</span></i>
                                                <div class="amount-cart">&#036;50</div>
                                            </a>
                                            <ul class="site-header-cart menu list-unstyled text-center">
                                                <li>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mobile-account visible-xs">
                                <div class="header-my-account">
                                    <div class="header-login">
                                        <a href="/my-account/" title="My Account">
                                            <i class="fa fa-user-circle-o"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a href="#" id="main-menu-panel" class="open-panel" data-panel="main-menu-panel">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                        <div id="my-menu" class="menu-container">
                            <ul id="menu-main-menu" class="nav navbar-nav navbar-left">
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2048"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-2048 nav-item"
                                >
                                    <a title="Home" href="/" class="nav-link">Home</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2060"
                                    class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-2060 nav-item"
                                >
                                    <a title="Shop" href="/shop/" class="nav-link">Shop</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2062"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2062 nav-item"
                                >
                                    <a title="Sale" href="/sale/" class="nav-link"
                                        >Sale<span class="menu-description">Hot!</span></a
                                    >
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2046"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2046 nav-item"
                                >
                                    <a title="About Us" href="/about-us/" class="nav-link">About Us</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2061"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2061 nav-item"
                                >
                                    <a title="Blog" href="/blog/" class="nav-link">Blog</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2047"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2047 nav-item"
                                >
                                    <a title="Contact" href="/contact/" class="nav-link">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <div id="site-content" class="container main-container" role="main">
                <div class="page-area">
                    <!-- start content container -->
                    <div class="row">
                        <article class="col-md-9">
                            <div class="woocommerce-notices-wrapper"></div>
                            <div
                                id="product-22"
                                class="product type-product post-22 status-publish first instock product_cat-clothing product_cat-womens-clothing has-post-thumbnail sale shipping-taxable purchasable product-type-simple"
                            >
                                <span class="onsale">Sale!</span>
                                <div
                                    class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images"
                                    data-columns="4"
                                    style="opacity: 0; transition: opacity 0.25s ease-in-out"
                                >
                                    <figure class="woocommerce-product-gallery__wrapper">
                                        <div
                                            data-thumb="https://staging.dndcleaners.ca/wp-content/uploads/2019/40/StockSnap_MO7PZ7AYIC-100x100.jpg"
                                            data-thumb-alt=""
                                            class="woocommerce-product-gallery__image"
                                        >
                                            <a href="/wp-content/uploads/2019/40/StockSnap_MO7PZ7AYIC.jpg"
                                                ><img
                                                    width="600"
                                                    height="408"
                                                    src="<?php echo $product->images[0]->src; ?>"
                                                    class="wp-post-image"
                                                    alt=""
                                                    decoding="async"
                                                    loading="lazy"
                                                    title="StockSnap_MO7PZ7AYIC.jpg"
                                                    data-caption=""
                                                    data-src="<?php echo $product->images[0]->src; ?>"
                                                    data-large_image="<?php echo $product->images[0]->src; ?>"
                                                    data-large_image_width="1280"
                                                    data-large_image_height="870"
                                                    sizes="(max-width: 600px) 100vw, 600px"
                                            /></a>
                                        </div>
                                    </figure>
                                </div>

                                <div class="summary entry-summary">
                                    <h1 class="product_title entry-title"><?php echo $product->name; ?></h1>
                                    <p class="price">
                                        <?php echo $product->price_html; ?>                                        
                                    </p>
                                    <div class="woocommerce-product-details__short-description">
                                        <?php echo $product->short_description; ?>
                                    </div>

                                    <form
                                        class="cart"
                                        action="https://staging.dndcleaners.ca/product/bikini/"
                                        method="post"
                                        enctype="multipart/form-data"
                                    >
                                        <button type="button" class="minus">-</button>
                                        <div class="quantity">
                                            <label class="screen-reader-text" for="quantity_6398242f2f5cd"
                                                >Bikini quantity</label
                                            >
                                            <input
                                                type="number"
                                                id="quantity_6398242f2f5cd"
                                                class="input-text qty text"
                                                step="1"
                                                min="1"
                                                max=""
                                                name="quantity"
                                                value="1"
                                                title="Qty"
                                                size="4"
                                                placeholder=""
                                                inputmode="numeric"
                                                autocomplete="off"
                                            />
                                        </div>
                                        <button type="button" class="plus">+</button>
                                        <button
                                            type="submit"
                                            name="add-to-cart"
                                            value="22"
                                            class="single_add_to_cart_button button alt wp-element-button"
                                        >
                                            Add to cart
                                        </button>
                                    </form>

                                    <div class="product_meta">
                                        <span class="posted_in"
                                            >Categories: 
                                            @foreach ($product->categories as $category)
                                                <a href="/product-category/clothing/ <?php echo $category->slug; ?>" rel="tag"><?php echo $category->name; ?></a>,
                                            @endforeach
                                            </span
                                        >
                                    </div>
                                </div>

                                <div class="woocommerce-tabs wc-tabs-wrapper">
                                    <ul class="tabs wc-tabs" role="tablist">
                                        <li
                                            class="description_tab"
                                            id="tab-title-description"
                                            role="tab"
                                            aria-controls="tab-description"
                                        >
                                            <a href="#tab-description"> Description </a>
                                        </li>
                                    </ul>
                                    <div
                                        class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab"
                                        id="tab-description"
                                        role="tabpanel"
                                        aria-labelledby="tab-title-description"
                                    >
                                        <h2>Description</h2>

                                        <?php echo $product->description; ?>
                                    </div>
                                </div>

                                <section class="related products">
                                    <h2>Related products</h2>

                                    <ul class="products columns-4">
                                        <li
                                            class="product type-product post-2052 status-publish first outofstock product_cat-clothing product_cat-mens-clothing has-post-thumbnail featured shipping-taxable purchasable product-type-variable"
                                        >
                                            <a
                                                href="/product/black-pants/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                ><img
                                                    width="300"
                                                    height="300"
                                                    src="/wp-content/uploads/2019/40/StockSnap_Y6SDOYW0KA-300x300.jpg"
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                    alt=""
                                                    decoding="async"
                                                    loading="lazy"
                                                    srcset="
                                                        /wp-content/uploads/2019/40/StockSnap_Y6SDOYW0KA-300x300.jpg 300w,
                                                        /wp-content/uploads/2019/40/StockSnap_Y6SDOYW0KA-100x100.jpg 100w,
                                                        /wp-content/uploads/2019/40/StockSnap_Y6SDOYW0KA-150x150.jpg 150w
                                                    "
                                                    sizes="(max-width: 300px) 100vw, 300px"
                                                />
                                                <h2 class="woocommerce-loop-product__title">Black pants</h2>
                                                <span class="price"
                                                    ><span class="woocommerce-Price-amount amount"
                                                        ><bdi
                                                            ><span class="woocommerce-Price-currencySymbol">&#36;</span
                                                            >89</bdi
                                                        ></span
                                                    >
                                                    &ndash;
                                                    <span class="woocommerce-Price-amount amount"
                                                        ><bdi
                                                            ><span class="woocommerce-Price-currencySymbol">&#36;</span
                                                            >99</bdi
                                                        ></span
                                                    ></span
                                                > </a
                                            ><a
                                                href="/product/black-pants/"
                                                data-quantity="1"
                                                class="button wp-element-button product_type_variable"
                                                data-product_id="2052"
                                                data-product_sku=""
                                                aria-label="Select options for &ldquo;Black pants&rdquo;"
                                                rel="nofollow"
                                                >Select options</a
                                            >
                                        </li>

                                        <li
                                            class="product type-product post-2054 status-publish instock product_cat-clothing product_cat-womens-clothing has-post-thumbnail sale shipping-taxable purchasable product-type-variable"
                                        >
                                            <a
                                                href="/product/polo-t-shirt/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                            >
                                                <span class="onsale">Sale!</span>
                                                <img
                                                    width="300"
                                                    height="300"
                                                    src="/wp-content/uploads/2019/40/StockSnap_TE9BDY8EYB-300x300.jpg"
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                    alt=""
                                                    decoding="async"
                                                    loading="lazy"
                                                    srcset="
                                                        /wp-content/uploads/2019/40/StockSnap_TE9BDY8EYB-300x300.jpg 300w,
                                                        /wp-content/uploads/2019/40/StockSnap_TE9BDY8EYB-100x100.jpg 100w,
                                                        /wp-content/uploads/2019/40/StockSnap_TE9BDY8EYB-150x150.jpg 150w
                                                    "
                                                    sizes="(max-width: 300px) 100vw, 300px"
                                                />
                                                <h2 class="woocommerce-loop-product__title">Polo T-shirt</h2>
                                                <span class="price"
                                                    ><span class="woocommerce-Price-amount amount"
                                                        ><bdi
                                                            ><span class="woocommerce-Price-currencySymbol">&#36;</span
                                                            >20</bdi
                                                        ></span
                                                    >
                                                    &ndash;
                                                    <span class="woocommerce-Price-amount amount"
                                                        ><bdi
                                                            ><span class="woocommerce-Price-currencySymbol">&#36;</span
                                                            >25</bdi
                                                        ></span
                                                    ></span
                                                > </a
                                            ><a
                                                href="/product/polo-t-shirt/"
                                                data-quantity="1"
                                                class="button wp-element-button product_type_variable add_to_cart_button"
                                                data-product_id="2054"
                                                data-product_sku=""
                                                aria-label="Select options for &ldquo;Polo T-shirt&rdquo;"
                                                rel="nofollow"
                                                >Select options</a
                                            >
                                        </li>

                                        <li
                                            class="product type-product post-2050 status-publish instock product_cat-clothing product_cat-mens-clothing has-post-thumbnail sale shipping-taxable purchasable product-type-simple"
                                        >
                                            <a
                                                href="/product/black-trousers/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                            >
                                                <span class="onsale">Sale!</span>
                                                <img
                                                    width="300"
                                                    height="300"
                                                    src="/wp-content/uploads/2019/40/StockSnap_RMOCSAQNUG-300x300.jpg"
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                    alt=""
                                                    decoding="async"
                                                    loading="lazy"
                                                    srcset="
                                                        /wp-content/uploads/2019/40/StockSnap_RMOCSAQNUG-300x300.jpg 300w,
                                                        /wp-content/uploads/2019/40/StockSnap_RMOCSAQNUG-100x100.jpg 100w,
                                                        /wp-content/uploads/2019/40/StockSnap_RMOCSAQNUG-150x150.jpg 150w
                                                    "
                                                    sizes="(max-width: 300px) 100vw, 300px"
                                                />
                                                <h2 class="woocommerce-loop-product__title">Black trousers</h2>
                                                <span class="price"
                                                    ><del aria-hidden="true"
                                                        ><span class="woocommerce-Price-amount amount"
                                                            ><bdi
                                                                ><span class="woocommerce-Price-currencySymbol"
                                                                    >&#36;</span
                                                                >25</bdi
                                                            ></span
                                                        ></del
                                                    >
                                                    <ins
                                                        ><span class="woocommerce-Price-amount amount"
                                                            ><bdi
                                                                ><span class="woocommerce-Price-currencySymbol"
                                                                    >&#36;</span
                                                                >16</bdi
                                                            ></span
                                                        ></ins
                                                    ></span
                                                > </a
                                            ><a
                                                href="/product/bikini/?add-to-cart=2050"
                                                data-quantity="1"
                                                class="button wp-element-button product_type_simple add_to_cart_button ajax_add_to_cart"
                                                data-product_id="2050"
                                                data-product_sku="woo-cap"
                                                aria-label="Add &ldquo;Black trousers&rdquo; to your cart"
                                                rel="nofollow"
                                                >Add to cart</a
                                            >
                                        </li>

                                        <li
                                            class="product type-product post-2053 status-publish last instock product_cat-clothing product_cat-womens-clothing has-post-thumbnail shipping-taxable purchasable product-type-simple"
                                        >
                                            <a
                                                href="/product/hard-top/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                ><img
                                                    width="300"
                                                    height="300"
                                                    src="/wp-content/uploads/2019/40/StockSnap_AQ62CU8K64-300x300.jpg"
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                    alt=""
                                                    decoding="async"
                                                    loading="lazy"
                                                    srcset="
                                                        /wp-content/uploads/2019/40/StockSnap_AQ62CU8K64-300x300.jpg 300w,
                                                        /wp-content/uploads/2019/40/StockSnap_AQ62CU8K64-100x100.jpg 100w,
                                                        /wp-content/uploads/2019/40/StockSnap_AQ62CU8K64-150x150.jpg 150w
                                                    "
                                                    sizes="(max-width: 300px) 100vw, 300px"
                                                />
                                                <h2 class="woocommerce-loop-product__title">Hard top</h2>
                                                <span class="price"
                                                    ><span class="woocommerce-Price-amount amount"
                                                        ><bdi
                                                            ><span class="woocommerce-Price-currencySymbol">&#36;</span
                                                            >30</bdi
                                                        ></span
                                                    ></span
                                                > </a
                                            ><a
                                                href="/product/bikini/?add-to-cart=2053"
                                                data-quantity="1"
                                                class="button wp-element-button product_type_simple add_to_cart_button ajax_add_to_cart"
                                                data-product_id="2053"
                                                data-product_sku=""
                                                aria-label="Add &ldquo;Hard top&rdquo; to your cart"
                                                rel="nofollow"
                                                >Add to cart</a
                                            >
                                        </li>
                                    </ul>
                                </section>
                            </div>
                        </article>
                        <aside id="sidebar" class="col-md-3">
                            <div id="woocommerce_widget_cart-1" class="widget woocommerce widget_shopping_cart">
                                <div class="widget-title"><h3>Cart</h3></div>
                                <div class="widget_shopping_cart_content"></div>
                            </div>
                            <div id="block-10" class="widget widget_block">
                                <div class="widget-title"><h3>Search Products</h3></div>
                                <div class="wp-widget-group__inner-blocks">
                                    <form
                                        role="search"
                                        method="get"
                                        action="https://staging.dndcleaners.ca/"
                                        class="wp-block-search__button-outside wp-block-search__text-button wp-block-search"
                                    >
                                        <label
                                            for="wp-block-search__input-2"
                                            class="wp-block-search__label screen-reader-text"
                                            >Search</label
                                        >
                                        <div class="wp-block-search__inside-wrapper">
                                            <input
                                                type="search"
                                                id="wp-block-search__input-2"
                                                class="wp-block-search__input wp-block-search__input"
                                                name="s"
                                                value=""
                                                placeholder="Search products..."
                                                required
                                            /><input type="hidden" name="post_type" value="product" /><button
                                                type="submit"
                                                class="wp-block-search__button wp-element-button"
                                            >
                                                Search
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="woocommerce_products-1" class="widget woocommerce widget_products">
                                <div class="widget-title"><h3>Products</h3></div>
                                <div id="side_bar"></div>
                            </div>
                        </aside>
                    </div>
                    <!-- end content container -->
                </div>
                <!-- end main-container -->
            </div>
            <!-- end page-area -->
              <!-- Same  -->                      
            @include('public.footer')
        </div>

        <!-- end page-wrap -->
        <!-- Same  -->
        <script type="application/ld+json">
            {
                "@context": "https:\/\/schema.org\/",
                "@type": "Product",
                "@id": "httpsstaging.dndcleaners.ca\/product\/bikini\/#product",
                "name": "Bikini",
                "url": "httpsstaging.dndcleaners.ca\/product\/bikini\/",
                "description": "This is a simple product.\r\n\r\nEtiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Mauris tincidunt sem sed arcu. Nunc auctor. Mauris dictum facilisis augue. Phasellus faucibus molestie nisl.",
                "image": "httpsstaging.dndcleaners.ca\/wp-content\/uploads\/2019\/40\/StockSnap_MO7PZ7AYIC.jpg",
                "sku": 22,
                "offers": [
                    {
                        "@type": "Offer",
                        "price": "20",
                        "priceValidUntil": "2023-12-31",
                        "priceSpecification": {
                            "price": "20",
                            "priceCurrency": "CAD",
                            "valueAddedTaxIncluded": "false"
                        },
                        "priceCurrency": "CAD",
                        "availability": "http:\/\/schema.org\/InStock",
                        "url": "httpsstaging.dndcleaners.ca\/product\/bikini\/",
                        "seller": {
                            "@type": "Organization",
                            "name": "Linh Linh Shop",
                            "url": "httpsstaging.dndcleaners.ca"
                        }
                    }
                ]
            }
        </script>
        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="pswp__bg"></div>
            <div class="pswp__scroll-wrap">
                <div class="pswp__container">
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                </div>
                <div class="pswp__ui pswp__ui--hidden">
                    <div class="pswp__top-bar">
                        <div class="pswp__counter"></div>
                        <button class="pswp__button pswp__button--close" aria-label="Close (Esc)"></button>
                        <button class="pswp__button pswp__button--share" aria-label="Share"></button>
                        <button class="pswp__button pswp__button--fs" aria-label="Toggle fullscreen"></button>
                        <button class="pswp__button pswp__button--zoom" aria-label="Zoom in/out"></button>
                        <div class="pswp__preloader">
                            <div class="pswp__preloader__icn">
                                <div class="pswp__preloader__cut">
                                    <div class="pswp__preloader__donut"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                        <div class="pswp__share-tooltip"></div>
                    </div>
                    <button class="pswp__button pswp__button--arrow--left" aria-label="Previous (arrow left)"></button>
                    <button class="pswp__button pswp__button--arrow--right" aria-label="Next (arrow right)"></button>
                    <div class="pswp__caption">
                        <div class="pswp__caption__center"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Same  -->
        
        <!-- Same  -->
         <script
            type="text/javascript"
            src="/js/vuejs/vue.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/vuejs/moment.min.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/product-detail.js"
        ></script>
        <script
            type="text/javascript"
            src="/js/sidebar.js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/zoom/jquery.zoom.min.js?ver=1.7.21-wc.7.1.0"
            id="zoom-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/flexslider/jquery.flexslider.min.js?ver=2.7.2-wc.7.1.0"
            id="flexslider-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/photoswipe/photoswipe.min.js?ver=4.1.1-wc.7.1.0"
            id="photoswipe-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/photoswipe/photoswipe-ui-default.min.js?ver=4.1.1-wc.7.1.0"
            id="photoswipe-ui-default-js"
        ></script>
        <script type="text/javascript" id="wc-single-product-js-extra">
            /* <![CDATA[ */
            var wc_single_product_params = {
                i18n_required_rating_text: "Please select a rating",
                review_rating_required: "yes",
                flexslider: {
                    rtl: false,
                    animation: "slide",
                    smoothHeight: true,
                    directionNav: false,
                    controlNav: "thumbnails",
                    slideshow: false,
                    animationSpeed: 500,
                    animationLoop: false,
                    allowOneSlide: false,
                },
                zoom_enabled: "1",
                zoom_options: [],
                photoswipe_enabled: "1",
                photoswipe_options: {
                    shareEl: false,
                    closeOnScroll: false,
                    history: false,
                    hideAnimationDuration: 0,
                    showAnimationDuration: 0,
                },
                flexslider_enabled: "1",
            };
            /* ]]> */
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/frontend/single-product.min.js?ver=7.1.0"
            id="wc-single-product-js"
        ></script>
        <!-- Same  -->
        @include('public.script')
        <!-- Same  -->
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-12-13 07:05:19 by W3 Total Cache
--></html>
