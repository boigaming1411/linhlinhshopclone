<!DOCTYPE html>
<html dir="ltr" lang="en-US" prefix="og: https://ogp.me/ns#">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="profile" href="https://gmpg.org/xfn/11" />
        <title>About Us - Linh Linh Shop</title>

        <!-- All in One SEO 4.2.7.1 - aioseo.com -->
        <meta
            name="description"
            content="About Us If you want to learn more about us, you&#039;re in the right place. Read to learn how we managed to grow our business so fast. Facebook Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcor. Twitter Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec"
        />
        <meta name="robots" content="noindex, nofollow, max-image-preview:large" />
        <link rel="canonical" href="/about-us/" />
        <meta name="generator" content="All in One SEO (AIOSEO) 4.2.7.1 " />
        <meta property="og:locale" content="en_US" />
        <meta property="og:site_name" content="Linh Linh Shop - Professional Alternations &amp; Repairs" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="About Us - Linh Linh Shop" />
        <meta
            property="og:description"
            content="About Us If you want to learn more about us, you&#039;re in the right place. Read to learn how we managed to grow our business so fast. Facebook Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcor. Twitter Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec"
        />
        <meta property="og:url" content="/about-us/" />
        <meta property="article:published_time" content="2019-04-18T11:16:34+00:00" />
        <meta property="article:modified_time" content="2019-04-18T11:16:34+00:00" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="About Us - Linh Linh Shop" />
        <meta
            name="twitter:description"
            content="About Us If you want to learn more about us, you&#039;re in the right place. Read to learn how we managed to grow our business so fast. Facebook Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcor. Twitter Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec"
        />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "BreadcrumbList",
                        "@id": "https:\/staging.dndcleaners.ca\/about-us\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "https:\/staging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "https:\/staging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "Product categories Men Women Jewelry Watch Accessories Baby Sport Summer Sale Shop Now Learn More Men's Fashion Shop Now Women's Fashion Shop Now Featured Best Selling Recent Sale Featured Black pants $89 – $99 Lựa chọn các tùy chọn Giảm giá! Modern $25 $19 Thêm vào giỏ hàng Giảm giá! Single Shirt $30 $22 Thêm",
                                    "url": "https:\/staging.dndcleaners.ca\/"
                                },
                                "nextItem": "https:\/staging.dndcleaners.ca\/about-us\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "https:\/staging.dndcleaners.ca\/about-us\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "https:\/staging.dndcleaners.ca\/about-us\/",
                                    "name": "About Us",
                                    "description": "About Us If you want to learn more about us, you're in the right place. Read to learn how we managed to grow our business so fast. Facebook Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcor. Twitter Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec",
                                    "url": "https:\/staging.dndcleaners.ca\/about-us\/"
                                },
                                "previousItem": "https:\/staging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "Organization",
                        "@id": "https:\/staging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "https:\/staging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "WebPage",
                        "@id": "https:\/staging.dndcleaners.ca\/about-us\/#webpage",
                        "url": "https:\/staging.dndcleaners.ca\/about-us\/",
                        "name": "About Us - Linh Linh Shop",
                        "description": "About Us If you want to learn more about us, you're in the right place. Read to learn how we managed to grow our business so fast. Facebook Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcor. Twitter Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec",
                        "inLanguage": "en-US",
                        "isPartOf": { "@id": "https:\/staging.dndcleaners.ca\/#website" },
                        "breadcrumb": { "@id": "https:\/staging.dndcleaners.ca\/about-us\/#breadcrumblist" },
                        "datePublished": "2019-04-18T11:16:34+00:00",
                        "dateModified": "2019-04-18T11:16:34+00:00"
                    },
                    {
                        "@type": "WebSite",
                        "@id": "https:\/staging.dndcleaners.ca\/#website",
                        "url": "https:\/staging.dndcleaners.ca\/",
                        "name": "Linh Linh Shop",
                        "description": "Professional Alternations & Repairs",
                        "inLanguage": "en-US",
                        "publisher": { "@id": "https:\/staging.dndcleaners.ca\/#organization" }
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->
            @include('public.header')
        
        <link
            rel="stylesheet"
            id="elementor-icons-css"
            href="/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.16.0"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-frontend-css"
            href="/wp-content/plugins/elementor/assets/css/frontend-lite.min.css?ver=3.8.1"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-post-1847-css"
            href="/wp-content/uploads/elementor/css/post-1847.css?ver=1668492633"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="etww-frontend-css"
            href="/wp-content/plugins/envo-elementor-for-woocommerce/assets/css/etww-frontend.min.css?ver=d6eb82c324cab2f36e345863fad95090"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-pro-css"
            href="/wp-content/plugins/elementor-pro/assets/css/frontend-lite.min.css?ver=3.8.0"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-global-css"
            href="/wp-content/uploads/elementor/css/global.css?ver=1668492633"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-post-53-css"
            href="/wp-content/uploads/elementor/css/post-53.css?ver=1668492634"
            type="text/css"
            media="all"
        />
        
        <link
            rel="stylesheet"
            id="google-fonts-1-css"
            href="https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPT+Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=swap&#038;ver=6.1"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-icons-shared-0-css"
            href="/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3"
            type="text/css"
            media="all"
        />
        <link
            rel="stylesheet"
            id="elementor-icons-fa-brands-css"
            href="/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.15.3"
            type="text/css"
            media="all"
        />
       
    </head>
    <body
        data-rsssl="1"
        id="blog"
        class="page-template page-template-template-parts page-template-template-page-builders page-template-template-partstemplate-page-builders-php page page-id-53 theme-envo-storefront woocommerce-no-js elementor-default elementor-kit-1847 elementor-page elementor-page-53"
    >
        @include('public.top-content')
        <div class="page-wrap">
           @include('public.top-bar-section')

            <div class="main-menu">
                <nav id="site-navigation" class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-header">
                            <span class="navbar-brand brand-absolute visible-xs">Menu</span>
                            <div class="mobile-cart visible-xs">
                                <div class="header-cart">
                                    <div class="header-cart-block">
                                        <div class="header-cart-inner">
                                            <a class="cart-contents" href="/cart/" title="View your shopping cart">
                                                <i class="fa fa-shopping-bag"><span class="count">0</span></i>
                                                <div class="amount-cart">&#036;0</div>
                                            </a>
                                            <ul class="site-header-cart menu list-unstyled text-center">
                                                <li>
                                                    <div class="widget woocommerce widget_shopping_cart">
                                                        <div class="widget_shopping_cart_content"></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mobile-account visible-xs">
                                <div class="header-my-account">
                                    <div class="header-login">
                                        <a href="/my-account/" title="My Account">
                                            <i class="fa fa-user-circle-o"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a href="#" id="main-menu-panel" class="open-panel" data-panel="main-menu-panel">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                        <div id="my-menu" class="menu-container">
                            <ul id="menu-main-menu" class="nav navbar-nav navbar-left">
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2048"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-2048 nav-item"
                                >
                                    <a title="Home" href="/" class="nav-link">Home</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2060"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2060 nav-item"
                                >
                                    <a title="Shop" href="/shop/" class="nav-link" >Shop</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2062"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2062 nav-item"
                                >
                                    <a title="Sale" href="/sale/" class="nav-link"
                                        >Sale<span class="menu-description">Hot!</span></a
                                    >
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2046"
                                    class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item current_page_item active menu-item-2046 nav-item"
                                >
                                    <a title="About Us" href="/about-us/" aria-current="page" class="nav-link">About Us</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2061"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2061 nav-item"
                                >
                                    <a title="Blog" href="/blog/" class="nav-link">Blog</a>
                                </li>
                                <li
                                    itemscope="itemscope"
                                    itemtype="https://www.schema.org/SiteNavigationElement"
                                    id="menu-item-2047"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2047 nav-item"
                                >
                                    <a title="Contact" href="/contact/" class="nav-link">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

            <div id="site-content" class="page-builders" role="main">
                <div class="page-builders-content-area">
                    <!-- start content container -->
                    <div class="post-53 page type-page status-publish hentry">
                        <div data-elementor-type="wp-post" data-elementor-id="53" class="elementor elementor-53">
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-11beeb82 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="11beeb82"
                                data-element_type="section"
                                data-settings='{"background_background":"classic","shape_divider_bottom":"mountains"}'
                            >
                                <div class="elementor-background-overlay"></div>
                                <div class="elementor-shape elementor-shape-bottom" data-negative="false">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewbox="0 0 1000 100"
                                        preserveaspectratio="none"
                                    >
                                        <path
                                            class="elementor-shape-fill"
                                            opacity="0.33"
                                            d="M473,67.3c-203.9,88.3-263.1-34-320.3,0C66,119.1,0,59.7,0,59.7V0h1000v59.7 c0,0-62.1,26.1-94.9,29.3c-32.8,3.3-62.8-12.3-75.8-22.1C806,49.6,745.3,8.7,694.9,4.7S492.4,59,473,67.3z"
                                        ></path>
                                        <path
                                            class="elementor-shape-fill"
                                            opacity="0.66"
                                            d="M734,67.3c-45.5,0-77.2-23.2-129.1-39.1c-28.6-8.7-150.3-10.1-254,39.1 s-91.7-34.4-149.2,0C115.7,118.3,0,39.8,0,39.8V0h1000v36.5c0,0-28.2-18.5-92.1-18.5C810.2,18.1,775.7,67.3,734,67.3z"
                                        ></path>
                                        <path
                                            class="elementor-shape-fill"
                                            d="M766.1,28.9c-200-57.5-266,65.5-395.1,19.5C242,1.8,242,5.4,184.8,20.6C128,35.8,132.3,44.9,89.9,52.5C28.6,63.7,0,0,0,0 h1000c0,0-9.9,40.9-83.6,48.1S829.6,47,766.1,28.9z"
                                        ></path>
                                    </svg>
                                </div>
                                <div class="elementor-container elementor-column-gap-default">
                                    <div
                                        class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-66f995e4"
                                        data-id="66f995e4"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-2199df57 elementor-widget elementor-widget-heading"
                                                data-id="2199df57"
                                                data-element_type="widget"
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-heading-title {
                                                            padding: 0;
                                                            margin: 0;
                                                            line-height: 1;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title[class*="elementor-size-"]
                                                            > a {
                                                            color: inherit;
                                                            font-size: inherit;
                                                            line-height: inherit;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-small {
                                                            font-size: 15px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-medium {
                                                            font-size: 19px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-large {
                                                            font-size: 29px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-xl {
                                                            font-size: 39px;
                                                        }
                                                        .elementor-widget-heading
                                                            .elementor-heading-title.elementor-size-xxl {
                                                            font-size: 59px;
                                                        }
                                                    </style>
                                                    <h2 class="elementor-heading-title elementor-size-default">
                                                        About Us
                                                    </h2>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-5e0ffb6 elementor-widget-divider--view-line elementor-widget elementor-widget-divider"
                                                data-id="5e0ffb6"
                                                data-element_type="widget"
                                                data-widget_type="divider.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-widget-divider {
                                                            --divider-border-style: none;
                                                            --divider-border-width: 1px;
                                                            --divider-color: #2c2c2c;
                                                            --divider-icon-size: 20px;
                                                            --divider-element-spacing: 10px;
                                                            --divider-pattern-height: 24px;
                                                            --divider-pattern-size: 20px;
                                                            --divider-pattern-url: none;
                                                            --divider-pattern-repeat: repeat-x;
                                                        }
                                                        .elementor-widget-divider .elementor-divider {
                                                            display: -webkit-box;
                                                            display: -ms-flexbox;
                                                            display: flex;
                                                        }
                                                        .elementor-widget-divider .elementor-divider__text {
                                                            font-size: 15px;
                                                            line-height: 1;
                                                            max-width: 95%;
                                                        }
                                                        .elementor-widget-divider .elementor-divider__element {
                                                            margin: 0 var(--divider-element-spacing);
                                                            -ms-flex-negative: 0;
                                                            flex-shrink: 0;
                                                        }
                                                        .elementor-widget-divider .elementor-icon {
                                                            font-size: var(--divider-icon-size);
                                                        }
                                                        .elementor-widget-divider .elementor-divider-separator {
                                                            display: -webkit-box;
                                                            display: -ms-flexbox;
                                                            display: flex;
                                                            margin: 0;
                                                            direction: ltr;
                                                        }
                                                        .elementor-widget-divider--view-line_icon
                                                            .elementor-divider-separator,
                                                        .elementor-widget-divider--view-line_text
                                                            .elementor-divider-separator {
                                                            -webkit-box-align: center;
                                                            -ms-flex-align: center;
                                                            align-items: center;
                                                        }
                                                        .elementor-widget-divider--view-line_icon
                                                            .elementor-divider-separator:after,
                                                        .elementor-widget-divider--view-line_icon
                                                            .elementor-divider-separator:before,
                                                        .elementor-widget-divider--view-line_text
                                                            .elementor-divider-separator:after,
                                                        .elementor-widget-divider--view-line_text
                                                            .elementor-divider-separator:before {
                                                            display: block;
                                                            content: "";
                                                            border-bottom: 0;
                                                            -webkit-box-flex: 1;
                                                            -ms-flex-positive: 1;
                                                            flex-grow: 1;
                                                            border-top: var(--divider-border-width)
                                                                var(--divider-border-style) var(--divider-color);
                                                        }
                                                        .elementor-widget-divider--element-align-left
                                                            .elementor-divider
                                                            .elementor-divider-separator
                                                            > .elementor-divider__svg:first-of-type {
                                                            -webkit-box-flex: 0;
                                                            -ms-flex-positive: 0;
                                                            flex-grow: 0;
                                                            -ms-flex-negative: 100;
                                                            flex-shrink: 100;
                                                        }
                                                        .elementor-widget-divider--element-align-left
                                                            .elementor-divider-separator:before {
                                                            content: none;
                                                        }
                                                        .elementor-widget-divider--element-align-left
                                                            .elementor-divider__element {
                                                            margin-left: 0;
                                                        }
                                                        .elementor-widget-divider--element-align-right
                                                            .elementor-divider
                                                            .elementor-divider-separator
                                                            > .elementor-divider__svg:last-of-type {
                                                            -webkit-box-flex: 0;
                                                            -ms-flex-positive: 0;
                                                            flex-grow: 0;
                                                            -ms-flex-negative: 100;
                                                            flex-shrink: 100;
                                                        }
                                                        .elementor-widget-divider--element-align-right
                                                            .elementor-divider-separator:after {
                                                            content: none;
                                                        }
                                                        .elementor-widget-divider--element-align-right
                                                            .elementor-divider__element {
                                                            margin-right: 0;
                                                        }
                                                        .elementor-widget-divider:not(
                                                                .elementor-widget-divider--view-line_text
                                                            ):not(.elementor-widget-divider--view-line_icon)
                                                            .elementor-divider-separator {
                                                            border-top: var(--divider-border-width)
                                                                var(--divider-border-style) var(--divider-color);
                                                        }
                                                        .elementor-widget-divider--separator-type-pattern {
                                                            --divider-border-style: none;
                                                        }
                                                        .elementor-widget-divider--separator-type-pattern.elementor-widget-divider--view-line
                                                            .elementor-divider-separator,
                                                        .elementor-widget-divider--separator-type-pattern:not(
                                                                .elementor-widget-divider--view-line
                                                            )
                                                            .elementor-divider-separator:after,
                                                        .elementor-widget-divider--separator-type-pattern:not(
                                                                .elementor-widget-divider--view-line
                                                            )
                                                            .elementor-divider-separator:before,
                                                        .elementor-widget-divider--separator-type-pattern:not(
                                                                [class*="elementor-widget-divider--view"]
                                                            )
                                                            .elementor-divider-separator {
                                                            width: 100%;
                                                            min-height: var(--divider-pattern-height);
                                                            -webkit-mask-size: var(--divider-pattern-size) 100%;
                                                            mask-size: var(--divider-pattern-size) 100%;
                                                            -webkit-mask-repeat: var(--divider-pattern-repeat);
                                                            mask-repeat: var(--divider-pattern-repeat);
                                                            background-color: var(--divider-color);
                                                            -webkit-mask-image: var(--divider-pattern-url);
                                                            mask-image: var(--divider-pattern-url);
                                                        }
                                                        .elementor-widget-divider--no-spacing {
                                                            --divider-pattern-size: auto;
                                                        }
                                                        .elementor-widget-divider--bg-round {
                                                            --divider-pattern-repeat: round;
                                                        }
                                                        .rtl .elementor-widget-divider .elementor-divider__text {
                                                            direction: rtl;
                                                        }
                                                        .e-con-inner > .elementor-widget-divider,
                                                        .e-con > .elementor-widget-divider {
                                                            width: var(--container-widget-width, 100%);
                                                        }
                                                    </style>
                                                    <div class="elementor-divider">
                                                        <span class="elementor-divider-separator"> </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="elementor-element elementor-element-5b713658 elementor-widget elementor-widget-text-editor"
                                                data-id="5b713658"
                                                data-element_type="widget"
                                                data-widget_type="text-editor.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-widget-text-editor.elementor-drop-cap-view-stacked
                                                            .elementor-drop-cap {
                                                            background-color: #818a91;
                                                            color: #fff;
                                                        }
                                                        .elementor-widget-text-editor.elementor-drop-cap-view-framed
                                                            .elementor-drop-cap {
                                                            color: #818a91;
                                                            border: 3px solid;
                                                            background-color: transparent;
                                                        }
                                                        .elementor-widget-text-editor:not(
                                                                .elementor-drop-cap-view-default
                                                            )
                                                            .elementor-drop-cap {
                                                            margin-top: 8px;
                                                        }
                                                        .elementor-widget-text-editor:not(
                                                                .elementor-drop-cap-view-default
                                                            )
                                                            .elementor-drop-cap-letter {
                                                            width: 1em;
                                                            height: 1em;
                                                        }
                                                        .elementor-widget-text-editor .elementor-drop-cap {
                                                            float: left;
                                                            text-align: center;
                                                            line-height: 1;
                                                            font-size: 50px;
                                                        }
                                                        .elementor-widget-text-editor .elementor-drop-cap-letter {
                                                            display: inline-block;
                                                        }
                                                    </style>
                                                    <p>
                                                        If you want to learn more about us, you&#8217;re in the right
                                                        place. Read to learn how we managed to grow our business so
                                                        fast.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-28bccb4d elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="28bccb4d"
                                data-element_type="section"
                            >
                                <div class="elementor-container elementor-column-gap-wide">
                                    <div
                                        class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-485de39"
                                        data-id="485de39"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-524c8acd elementor-view-default elementor-mobile-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                data-id="524c8acd"
                                                data-element_type="widget"
                                                data-widget_type="icon-box.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <link
                                                        rel="stylesheet"
                                                        href="/wp-content/plugins/elementor/assets/css/widget-icon-box.min.css"
                                                    />
                                                    <div class="elementor-icon-box-wrapper">
                                                        <div class="elementor-icon-box-icon">
                                                            <span class="elementor-icon elementor-animation-">
                                                                <i aria-hidden="true" class="fab fa-facebook-f"></i>
                                                            </span>
                                                        </div>
                                                        <div class="elementor-icon-box-content">
                                                            <h3 class="elementor-icon-box-title">
                                                                <span> Facebook </span>
                                                            </h3>
                                                            <p class="elementor-icon-box-description">
                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                                Ut elit tellus, luctus nec ullamcor.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-401d53e4"
                                        data-id="401d53e4"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-6e8af269 elementor-view-default elementor-mobile-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                data-id="6e8af269"
                                                data-element_type="widget"
                                                data-widget_type="icon-box.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-icon-box-wrapper">
                                                        <div class="elementor-icon-box-icon">
                                                            <span class="elementor-icon elementor-animation-">
                                                                <i aria-hidden="true" class="fab fa-twitter"></i>
                                                            </span>
                                                        </div>
                                                        <div class="elementor-icon-box-content">
                                                            <h3 class="elementor-icon-box-title">
                                                                <span> Twitter </span>
                                                            </h3>
                                                            <p class="elementor-icon-box-description">
                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                                Ut elit tellus, luctus nec ullamcor.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-17801781"
                                        data-id="17801781"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-5b03abf7 elementor-view-default elementor-mobile-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                data-id="5b03abf7"
                                                data-element_type="widget"
                                                data-widget_type="icon-box.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-icon-box-wrapper">
                                                        <div class="elementor-icon-box-icon">
                                                            <span class="elementor-icon elementor-animation-">
                                                                <i aria-hidden="true" class="fab fa-youtube"></i>
                                                            </span>
                                                        </div>
                                                        <div class="elementor-icon-box-content">
                                                            <h3 class="elementor-icon-box-title">
                                                                <span> YouTube </span>
                                                            </h3>
                                                            <p class="elementor-icon-box-description">
                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                                Ut elit tellus, luctus nec ullamcor.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-1c807e5 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="1c807e5"
                                data-element_type="section"
                                data-settings='{"background_background":"classic"}'
                            >
                                <div class="elementor-container elementor-column-gap-wide">
                                    <div
                                        class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-73210d42"
                                        data-id="73210d42"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-e44e0e9 elementor-widget elementor-widget-heading"
                                                data-id="e44e0e9"
                                                data-element_type="widget"
                                                data-widget_type="heading.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <h3 class="elementor-heading-title elementor-size-default">
                                                        Our story
                                                    </h3>
                                                </div>
                                            </div>
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-6caca5e6 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="6caca5e6"
                                                data-element_type="section"
                                            >
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div
                                                        class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-2202aea4"
                                                        data-id="2202aea4"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-78cdfcde elementor-widget elementor-widget-text-editor"
                                                                data-id="78cdfcde"
                                                                data-element_type="widget"
                                                                data-widget_type="text-editor.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <p>
                                                                        Lorem ipsum dolor sit amet, consectetur
                                                                        adipiscing elit. Donec venenatis velit metus.
                                                                        Morbi non augue non sapien ullamcorper volutpat
                                                                        id a libero. Curabitur maximus vel est ac
                                                                        suscipit. Pellentesque imperdiet ultricies
                                                                        interdum. 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-12138f37"
                                                        data-id="12138f37"
                                                        data-element_type="column"
                                                    >
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div
                                                                class="elementor-element elementor-element-7139c32d elementor-widget elementor-widget-text-editor"
                                                                data-id="7139c32d"
                                                                data-element_type="widget"
                                                                data-widget_type="text-editor.default"
                                                            >
                                                                <div class="elementor-widget-container">
                                                                    <p>
                                                                        Nunc elementum porta tempus. In gravida nisi at
                                                                        dui eleifend lobortis. Vestibulum lobortis, leo
                                                                        at laoreet ornare, ante lectus commodo nisl,
                                                                        varius pellentesque velit justo eget purus.
                                                                         Aliquam tortor massa, elementum at tellus. 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section
                                class="elementor-section elementor-top-section elementor-element elementor-element-2517b949 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                data-id="2517b949"
                                data-element_type="section"
                                data-settings='{"background_background":"classic","shape_divider_top":"mountains"}'
                            >
                                <div class="elementor-background-overlay"></div>
                                <div class="elementor-shape elementor-shape-top" data-negative="false">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewbox="0 0 1000 100"
                                        preserveaspectratio="none"
                                    >
                                        <path
                                            class="elementor-shape-fill"
                                            opacity="0.33"
                                            d="M473,67.3c-203.9,88.3-263.1-34-320.3,0C66,119.1,0,59.7,0,59.7V0h1000v59.7 c0,0-62.1,26.1-94.9,29.3c-32.8,3.3-62.8-12.3-75.8-22.1C806,49.6,745.3,8.7,694.9,4.7S492.4,59,473,67.3z"
                                        ></path>
                                        <path
                                            class="elementor-shape-fill"
                                            opacity="0.66"
                                            d="M734,67.3c-45.5,0-77.2-23.2-129.1-39.1c-28.6-8.7-150.3-10.1-254,39.1 s-91.7-34.4-149.2,0C115.7,118.3,0,39.8,0,39.8V0h1000v36.5c0,0-28.2-18.5-92.1-18.5C810.2,18.1,775.7,67.3,734,67.3z"
                                        ></path>
                                        <path
                                            class="elementor-shape-fill"
                                            d="M766.1,28.9c-200-57.5-266,65.5-395.1,19.5C242,1.8,242,5.4,184.8,20.6C128,35.8,132.3,44.9,89.9,52.5C28.6,63.7,0,0,0,0 h1000c0,0-9.9,40.9-83.6,48.1S829.6,47,766.1,28.9z"
                                        ></path>
                                    </svg>
                                </div>
                                <div class="elementor-container elementor-column-gap-default">
                                    <div
                                        class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-5e486587"
                                        data-id="5e486587"
                                        data-element_type="column"
                                    >
                                        <div class="elementor-widget-wrap elementor-element-populated">
                                            <div
                                                class="elementor-element elementor-element-1a3771fe elementor-aspect-ratio-169 elementor-widget elementor-widget-video"
                                                data-id="1a3771fe"
                                                data-element_type="widget"
                                                data-settings='{"youtube_url":"https:\/\/www.youtube.com\/watch?v=XHOmBV4js_E","video_type":"youtube","controls":"yes","aspect_ratio":"169"}'
                                                data-widget_type="video.default"
                                            >
                                                <div class="elementor-widget-container">
                                                    <style>
                                                        /*! elementor - v3.8.1 - 13-11-2022 */
                                                        .elementor-widget-video .elementor-widget-container {
                                                            overflow: hidden;
                                                            -webkit-transform: translateZ(0);
                                                            transform: translateZ(0);
                                                        }
                                                        .elementor-widget-video
                                                            .elementor-open-inline
                                                            .elementor-custom-embed-image-overlay {
                                                            position: absolute;
                                                            top: 0;
                                                            left: 0;
                                                            width: 100%;
                                                            height: 100%;
                                                            background-size: cover;
                                                            background-position: 50%;
                                                        }
                                                        .elementor-widget-video .elementor-custom-embed-image-overlay {
                                                            cursor: pointer;
                                                            text-align: center;
                                                        }
                                                        .elementor-widget-video
                                                            .elementor-custom-embed-image-overlay:hover
                                                            .elementor-custom-embed-play
                                                            i {
                                                            opacity: 1;
                                                        }
                                                        .elementor-widget-video
                                                            .elementor-custom-embed-image-overlay
                                                            img {
                                                            display: block;
                                                            width: 100%;
                                                        }
                                                        .elementor-widget-video .e-hosted-video .elementor-video {
                                                            -o-object-fit: cover;
                                                            object-fit: cover;
                                                        }
                                                        .e-con-inner > .elementor-widget-video,
                                                        .e-con > .elementor-widget-video {
                                                            width: var(--container-widget-width, 100%);
                                                        }
                                                    </style>
                                                    <div
                                                        class="elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline"
                                                    >
                                                        <div class="elementor-video"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>

                    <!-- end content container -->
                </div>
                <!-- end main-container -->
            </div>
            <!-- end page-area -->

            @include('public.footer')
        </div>
        <!-- end page-wrap -->
       @include('public.script')
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.8.0"
            id="elementor-pro-webpack-runtime-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.8.1"
            id="elementor-webpack-runtime-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.8.1"
            id="elementor-frontend-modules-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/vendor/regenerator-runtime.min.js?ver=0.13.9"
            id="regenerator-runtime-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=3.15.0"
            id="wp-polyfill-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/hooks.min.js?ver=4169d3cf8e8d95a3d6d5"
            id="wp-hooks-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/i18n.min.js?ver=9e794f35a71bb98672ae"
            id="wp-i18n-js"
        ></script>
        <script type="text/javascript" id="wp-i18n-js-after">
            wp.i18n.setLocaleData({ "text directionltr": ["ltr"] });
        </script>
        <script type="text/javascript" id="elementor-pro-frontend-js-before">
            var ElementorProFrontendConfig = {
                ajaxurl: "https:\/staging.dndcleaners.ca\/wp-admin\/admin-ajax.php",
                nonce: "ea03ce6765",
                urls: {
                    assets: "https:\/staging.dndcleaners.ca\/wp-content\/plugins\/elementor-pro\/assets\/",
                    rest: "https:\/staging.dndcleaners.ca\/wp-json\/",
                },
                shareButtonsNetworks: {
                    facebook: { title: "Facebook", has_counter: true },
                    twitter: { title: "Twitter" },
                    linkedin: { title: "LinkedIn", has_counter: true },
                    pinterest: { title: "Pinterest", has_counter: true },
                    reddit: { title: "Reddit", has_counter: true },
                    vk: { title: "VK", has_counter: true },
                    odnoklassniki: { title: "OK", has_counter: true },
                    tumblr: { title: "Tumblr" },
                    digg: { title: "Digg" },
                    skype: { title: "Skype" },
                    stumbleupon: { title: "StumbleUpon", has_counter: true },
                    mix: { title: "Mix" },
                    telegram: { title: "Telegram" },
                    pocket: { title: "Pocket", has_counter: true },
                    xing: { title: "XING", has_counter: true },
                    whatsapp: { title: "WhatsApp" },
                    email: { title: "Email" },
                    print: { title: "Print" },
                },
                woocommerce: {
                    menu_cart: {
                        cart_page_url: "https:\/staging.dndcleaners.ca\/cart\/",
                        checkout_page_url: "https:\/staging.dndcleaners.ca\/checkout\/",
                        fragments_nonce: "214bd84b63",
                    },
                },
                facebook_sdk: { lang: "en_US", app_id: "" },
                lottie: {
                    defaultAnimationUrl:
                        "https:\/staging.dndcleaners.ca\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json",
                },
            };
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.8.0"
            id="elementor-pro-frontend-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2"
            id="elementor-waypoints-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/jquery/ui/core.min.js?ver=1.13.2"
            id="jquery-ui-core-js"
        ></script>
        <script type="text/javascript" id="elementor-frontend-js-before">
            var elementorFrontendConfig = {
                environmentMode: { edit: false, wpPreview: false, isScriptDebug: false },
                i18n: {
                    shareOnFacebook: "Share on Facebook",
                    shareOnTwitter: "Share on Twitter",
                    pinIt: "Pin it",
                    download: "Download",
                    downloadImage: "Download image",
                    fullscreen: "Fullscreen",
                    zoom: "Zoom",
                    share: "Share",
                    playVideo: "Play Video",
                    previous: "Previous",
                    next: "Next",
                    close: "Close",
                },
                is_rtl: false,
                breakpoints: { xs: 0, sm: 480, md: 768, lg: 1025, xl: 1440, xxl: 1600 },
                responsive: {
                    breakpoints: {
                        mobile: { label: "Mobile", value: 767, default_value: 767, direction: "max", is_enabled: true },
                        mobile_extra: {
                            label: "Mobile Extra",
                            value: 880,
                            default_value: 880,
                            direction: "max",
                            is_enabled: false,
                        },
                        tablet: {
                            label: "Tablet",
                            value: 1024,
                            default_value: 1024,
                            direction: "max",
                            is_enabled: true,
                        },
                        tablet_extra: {
                            label: "Tablet Extra",
                            value: 1200,
                            default_value: 1200,
                            direction: "max",
                            is_enabled: false,
                        },
                        laptop: {
                            label: "Laptop",
                            value: 1366,
                            default_value: 1366,
                            direction: "max",
                            is_enabled: false,
                        },
                        widescreen: {
                            label: "Widescreen",
                            value: 2400,
                            default_value: 2400,
                            direction: "min",
                            is_enabled: false,
                        },
                    },
                },
                version: "3.8.1",
                is_static: false,
                experimentalFeatures: {
                    e_dom_optimization: true,
                    e_optimized_assets_loading: true,
                    e_optimized_css_loading: true,
                    a11y_improvements: true,
                    additional_custom_breakpoints: true,
                    e_import_export: true,
                    e_hidden_wordpress_widgets: true,
                    theme_builder_v2: true,
                    "landing-pages": true,
                    "elements-color-picker": true,
                    "favorite-widgets": true,
                    "admin-top-bar": true,
                    "page-transitions": true,
                    notes: true,
                    loop: true,
                    "form-submissions": true,
                    e_scroll_snap: true,
                },
                urls: { assets: "https:\/staging.dndcleaners.ca\/wp-content\/plugins\/elementor\/assets\/" },
                settings: { page: [], editorPreferences: [] },
                kit: {
                    active_breakpoints: ["viewport_mobile", "viewport_tablet"],
                    global_image_lightbox: "yes",
                    lightbox_enable_counter: "yes",
                    lightbox_enable_fullscreen: "yes",
                    lightbox_enable_zoom: "yes",
                    lightbox_enable_share: "yes",
                    lightbox_title_src: "title",
                    lightbox_description_src: "description",
                    woocommerce_notices_elements: [],
                },
                post: { id: 53, title: "About%20Us%20-%20Linh%20Linh%20Shop", excerpt: "", featuredImage: false },
            };
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.8.1"
            id="elementor-frontend-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/elementor-pro/assets/js/elements-handlers.min.js?ver=3.8.0"
            id="pro-elements-handlers-js"
        ></script>
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-11-15 14:06:59 by W3 Total Cache
--></html>
